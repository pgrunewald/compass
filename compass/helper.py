from datetime import datetime
import pytz


def utcnow():
    t = datetime.utcnow()
    return t.replace(tzinfo=pytz.utc)


def default_strftime(dt):
    if not dt:
        return u"null"
    return dt.isoformat()


