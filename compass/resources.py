# coding=utf-8
from collections import defaultdict
from pyramid.security import Everyone, Authenticated
from pyramid.security import Allow, Deny, DENY_ALL, ALL_PERMISSIONS
from pyramid.traversal import quote_path_segment
from sqlalchemy import and_, or_, desc, asc
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from pyramid.httpexceptions import HTTPCreated, HTTPConflict, HTTPNotFound, HTTPOk
from zope.interface import implementer
from zope.interface import Interface

from compass.models import (
    DBSession,
    Problem,
    Tag,
    Comment,
    User,
    Solution,
    ProblemSolutionProposal,
    FeedbackType,
    SolutionFeedback,
    SolutionUsefulness,
    problem_tags,
    Topic,
    topic_tags,
)

from compass.cbr.cycle import (
    CBRCycle,
    ProblemQuery
)

admin_access = (Allow, 'admin', ALL_PERMISSIONS)

default_acl = [admin_access,
               (Allow, Everyone, 'read'),
               ]


confidential_acl = [admin_access,
                    (Allow, Authenticated, 'read'),
                    ]


class Resource(object):

    def __init__(self):
        self._parent = None
        self.__parent__ = None

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value
        if getattr(self, 'acl_lineage', None) in (None, True):
            self.__parent__ = value

    def __resource_url__(self, request, info):
        parent_resource_url = request.resource_url(self.parent)
        if not request.resource_url(self.parent).endswith(u'/'):
            parent_resource_url += u'/'
        return parent_resource_url + quote_path_segment(self.__name__)

    def find_parent(self, klass):
        if self.parent is None:
            return None
        if isinstance(self.parent, klass):
            return self.parent
        else:
            return self.parent.find_parent(klass)


class ISearchableResource(Interface):
    """An interface which ought to be used to search.
    """

    def search(request):
        """Perform a search by parameters given in the request."""

@implementer(ISearchableResource)
class SearchableResource(Resource):
    """An abstract class which implements the ISearchableResource which offers some default behaviour."""

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def paginate_query(self, query, request):
        if not request.json_body.get('ALL'):
            page = request.json_body.get('PAGE', 0)
            per_page = request.json_body.get('PER_PAGE', 25)
            offset = page * per_page
            return query.limit(per_page).offset(offset)
        else:
            return query

    def more_results_possible(self, total_entries, request):
        page = request.json_body.get('PAGE', 0)
        per_page = request.json_body.get('PER_PAGE', 25)
        max_pages = total_entries / per_page
        if total_entries % per_page:
            max_pages += 1
        return page < max_pages - 1

    def build_query(self, request):
        raise NotImplementedError('please implement the build_query function of class {}!'.format(self.__name__))

    def search(self, request):
        import time
        timepoint1 = time.time()
        base_query = self.build_query(request)
        total_entries = base_query.count()
        paginated_query = self.paginate_query(base_query, request)
        results = paginated_query.all()
        timepoint2 = time.time()
        # Build response dict
        response = dict()
        response['query'] = request.json_body
        response['query_time'] = (timepoint2 - timepoint1)
        response['results'] = results
        response['count'] = len(results)
        response['total_entries'] = total_entries
        response['more_results'] = self.more_results_possible(total_entries, request)
        return response


class DatabaseResource(Resource):
    id_field = 'id'
    acl_lineage = False
    __acl__ = default_acl

    def __init__(self, obj, parent):
        self.obj = obj
        self.parent = parent
        self.__name__ = getattr(self.obj, self.id_field)
        if hasattr(self.obj, '__acl__'):
            self.__acl__ = getattr(self, '__acl__', []) + self.obj.__acl__

    def delete(self):
        DBSession.delete(self.obj)
        DBSession.flush()


class NonExistentResource(DatabaseResource):
    acl_lineage = True

    def __init__(self, name, parent):
        self.obj = {}
        self.__name__ = name
        self.parent = parent


########################################################################################################################
# Problem-related
########################################################################################################################

###########################################################
# Problems/tags
###########################################################


class ProblemTagResource(DatabaseResource):
    """Refers to rest/problems/{problem_id}/tags/{tag_name}
    """
    id_field = 'name'
    acl_lineage = True

    def unbind(self, tag_name):
        self.parent.unbind(tag_name)


class NotYetBoundProblemTagResource(NonExistentResource):
    """Refers to rest/problems/{problem_id}/tags/{tag_name}
    """
    def bind(self, tag_name):
        self.parent.bind(tag_name)


class ProblemTagCollection(Resource):
    """Refers to rest/problems/{problem_id}/tags
    """

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.problem = self.parent.obj

    def __getitem__(self, tag_name):
        try:
            tag = self.problem.tags.filter(Tag.name == tag_name).one()
        except NoResultFound:
            return NotYetBoundProblemTagResource(tag_name, self)
        return ProblemTagResource(tag, self)

    def all(self):
        return self.problem.tags.all()

    def bind(self, tag_name):
        if self.problem.tags.filter(Tag.name == tag_name).first():
            raise ValueError(u"Tag '{}' is already bound.".format(tag_name))
        try:
            tag = DBSession.query(Tag).filter(Tag.name == tag_name).one()
        except NoResultFound:
            raise ValueError(u"Did not find tag '{}'.".format(tag_name))
        self.problem.tags.append(tag)

    def unbind(self, tag_name):
        try:
            tag = self.problem.tags.filter(Tag.name == tag_name).one()
        except NoResultFound:
            raise ValueError(u"Cannot remove tag '{}', because it is not bound to problem.".format(tag_name))
        self.problem.tags.remove(tag)

    def delete(self):
        for tag in self.problem.tags.all():
            DBSession.delete(tag)


###########################################################
# Problems/comments
###########################################################

class ProblemCommentRepliesCollection(Resource):
    """Refers to rest/problems/{problem_id}/comments/{comment_id}/replies
    """
    acl_lineage = False
    __acl__ = default_acl + [(Allow, Authenticated, 'create')]  # Every member can create a reply

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.parent_comment = self.parent.obj
        self.problem = self.parent.parent.problem

    def create(self, param):
        reply_comment = Comment(**param)
        reply_comment.parent_id = self.parent_comment.id
        self.problem.comments.append(reply_comment)
        DBSession.flush()
        return ProblemCommentResource(reply_comment, self.parent.parent)

    def all(self):
        return self.parent_comment.children


class ProblemCommentResource(DatabaseResource):
    """Refers to rest/problems/{problem_id}/comments/{comment_id}
    """
    children = {
        'replies': ProblemCommentRepliesCollection,
    }

    def __getitem__(self, key):
        return self.children[key](key, self)

    def update(self, request):
        comment = self.obj
        for key, value in request.json_body.items():
            if key not in ('text', ):
                continue
            setattr(comment, key, value)
        DBSession.flush()


class ProblemCommentCollection(Resource):
    """Refers to rest/problems/{problem_id}/comments
    """
    acl_lineage = False
    __acl__ = default_acl + [(Allow, Authenticated, 'create')]  # Every member can create a comment

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.problem = self.parent.obj

    def __getitem__(self, comment_id):
        try:
            comment = self.problem.comments.filter(Comment.id == comment_id).one()
        except NoResultFound:
            raise KeyError
        return ProblemCommentResource(comment, self)

    def create(self, param):
        comment = Comment(**param)
        self.problem.comments.append(comment)
        DBSession.flush()
        return ProblemCommentResource(comment, self)

    def all(self):
        return self.problem.comments.all()


###########################################################
# Problems/solutions
###########################################################

class UserSolutionFeedbackResource(Resource):
    """Refers to rest/problems/{problem_id}/solutions/{solution_id}/feedback/{username}
    """
    acl_lineage = False

    def __init__(self, name, user, parent):
        self.__name__ = name
        self.parent = parent
        self.user = user
        self.__acl__ = [(Allow, user.username, 'create'),
                        (Allow, user.username, 'read'),
                        (Allow, user.username, 'update'),
                        (Allow, user.username, 'delete')]

    def get_user_feedback(self):
        usefulness_feedback = DBSession.query(FeedbackType.serialization_name, SolutionUsefulness.response).\
            outerjoin(SolutionUsefulness, and_(FeedbackType.id == SolutionUsefulness.feedback_type_id,
                                               SolutionUsefulness.solution_id == self.parent.solution_proposal.solution_id,
                                               SolutionUsefulness.problem_id == self.parent.solution_proposal.problem_id,
                                               SolutionUsefulness.user_id == self.user.id)).\
            filter(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_AND_PROBLEM).all()

        solution_feedback = DBSession.query(FeedbackType.serialization_name, SolutionFeedback.response).\
            outerjoin(SolutionFeedback, and_(FeedbackType.id == SolutionFeedback.feedback_type_id,
                                             SolutionFeedback.solution_id == self.parent.solution_proposal.solution_id,
                                             SolutionFeedback.user_id == self.user.id)
                      ).filter(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_ONLY).all()

        base_dict = dict()
        for feedback_serialize_name, feedback_response in solution_feedback + usefulness_feedback:
            base_dict[feedback_serialize_name] = feedback_response
        return base_dict

    def delete_user_feedback(self):
        for feedback in self.parent.solution_proposal.solution.feedback.filter(SolutionFeedback.user_id ==
                                                                               self.user.id).all() +\
                self.parent.solution_proposal.usefulness.filter(SolutionUsefulness.user_id == self.user.id).all():
            DBSession.delete(feedback)
        DBSession.flush()


class SolutionFeedbackCollection(Resource):
    """Refers to rest/problems/{problem_id}/solutions/{solution_id}/feedback
    """
    acl_lineage = False
    __acl__ = default_acl + [(Allow, Authenticated, 'create')]

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.solution_proposal = self.parent.obj

    def __getitem__(self, username):
        try:
            user = DBSession.query(User).filter(User.username == username).one()
        except NoResultFound:
            raise KeyError
        return UserSolutionFeedbackResource(user.username, user, self)

    def criteria(self):
        """Displays all criteria information for the feedback.
        """
        all_criteria = DBSession.query(FeedbackType).filter(FeedbackType.scope.in_(
            [FeedbackType.SCOPE_SOLUTION_ONLY, FeedbackType.SCOPE_SOLUTION_AND_PROBLEM])).all()

        c = dict()
        for feedback_type in all_criteria:
            c[feedback_type.serialization_name] = {
                'name': feedback_type.name,
                'description': feedback_type.description,
                'allowed_values': range(feedback_type.min_value, feedback_type.max_value + 1),
            }
        return c

    def vote(self, data, user):
        solution_only_types = DBSession.query(FeedbackType).filter(FeedbackType.scope ==
                                                                   FeedbackType.SCOPE_SOLUTION_ONLY).all()
        for solution_only_type in solution_only_types:
            if solution_only_type.serialization_name in data:
                response = data[solution_only_type.serialization_name]
                # Vote found, but new one or overwriting existing?
                solution_feedback = self.solution_proposal.solution.feedback.join(User).filter(
                    and_(User.username == user.username,
                         SolutionFeedback.feedback_type_id == solution_only_type.id)).first()
                if solution_feedback:
                    # Found, just update
                    if response is None:
                        # None value forces to delete that particular feedback
                        DBSession.delete(solution_feedback)
                    solution_feedback.response = response
                else:
                    # New vote, insert
                    solution_feedback = SolutionFeedback(feedback_type=solution_only_type, response=response, user=user)
                    self.solution_proposal.solution.feedback.append(solution_feedback)

        solution_and_problem_types = DBSession.query(FeedbackType).filter(FeedbackType.scope ==
                                                                          FeedbackType.SCOPE_SOLUTION_AND_PROBLEM).all()
        for solution_and_problem_type in solution_and_problem_types:
            if solution_and_problem_type.serialization_name in data:
                response = data[solution_and_problem_type.serialization_name]
                # Vote found, but new one or overwriting existing?
                usefulness_feedback = self.solution_proposal.usefulness.join(User).filter(
                    and_(User.username == user.username,
                         SolutionUsefulness.feedback_type_id ==
                         solution_and_problem_type.id)).first()

                if usefulness_feedback:
                    if response is None:
                        # None value forces to delete that particular feedback
                        DBSession.delete(usefulness_feedback)
                    # Found, just update
                    usefulness_feedback.response = response
                else:
                    # New vote, insert
                    usefulness_feedback = SolutionUsefulness(feedback_type=solution_and_problem_type, response=response,
                                                             user=user)
                    self.solution_proposal.usefulness.append(usefulness_feedback)
        DBSession.flush()
        return UserSolutionFeedbackResource(user.username, user, self)

    def show_stats(self):
        base_dict = self.criteria()

        # Insert average scores
        union_feedback = self.solution_proposal.avg_usefulness + self.solution_proposal.solution.feedback_avg_criteria
        for _feedback_type_id, feedback_serialize_name, feedback_avg_value in union_feedback:
            base_dict[feedback_serialize_name]['average'] = feedback_avg_value

        # prepare individual votes
        for feedback_serialize_name in base_dict:
            base_dict[feedback_serialize_name]['votes'] = dict()
            for value in base_dict[feedback_serialize_name]['allowed_values']:
                base_dict[feedback_serialize_name]['votes'][value] = 0
            base_dict[feedback_serialize_name]['sum'] = 0

        # Insert individual votes
        each_responses = self.solution_proposal.each_usefulness_responses +\
                         self.solution_proposal.solution.each_feedback_responses
        for _id, feedback_serialize_name, feedback_response, feedback_response_sum in each_responses:
            if not feedback_response:
                continue
            base_dict[feedback_serialize_name]['votes'][feedback_response] = feedback_response_sum
            if feedback_response_sum:
                base_dict[feedback_serialize_name]['sum'] += feedback_response
        return base_dict


class ProblemSolutionProposalResource(DatabaseResource):
    """Refers to rest/problems/{problem_id}/solutions/{solution_id}
    """
    id_field = 'solution_id'

    children = {
        'feedback': SolutionFeedbackCollection
    }

    def __getitem__(self, key):
        return self.children[key](key, self)

    def revoke(self):
        DBSession.delete(self.obj)


class ProblemSolutionNotProposedResource(NonExistentResource):

    def propose(self, solution_id, user):
        self.parent.propose(solution_id, user)
    #       TODO: /ratings (GET, POST) + /ratings/<<username>> (GET, POST, PUT, DELETE)


class ProblemSolutionCollection(Resource):
    """Refers to rest/problems/{problem_id}/solutions
    """
    acl_lineage = False
    __acl__ = default_acl + [(Allow, Authenticated, 'create')]

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.problem = self.parent.obj

    def __getitem__(self, solution_id):
        try:
            solution_proposal = DBSession.query(ProblemSolutionProposal)\
                .filter(and_(ProblemSolutionProposal.solution_id == solution_id,
                             ProblemSolutionProposal.problem_id == self.problem.id)).one()
        except NoResultFound:
            if DBSession.query(Solution).filter(Solution.id == solution_id).first():
                return ProblemSolutionNotProposedResource(solution_id, self)
            raise KeyError
        return ProblemSolutionProposalResource(solution_proposal, self)

    def propose(self, solution_id, user):
        solution = DBSession.query(Solution).filter(Solution.id == solution_id).one()
        self.problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution, user=user))

    def create_and_propose(self, param):
        solution = Solution(**param)
        solution_proposal = ProblemSolutionProposal(solution=solution, user=param['user'])
        self.problem.problem_solutions_association.append(solution_proposal)
        DBSession.flush()
        return ProblemSolutionProposalResource(solution_proposal, self)

    def all(self):
        return self.problem.solutions_sorted()


class ProblemSuggestionsCollection(DatabaseResource):

    acl_lineage = False
    __acl__ = default_acl

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.problem = self.parent.obj

    def retrieve_similar_problems(self, request):
        """Retrieve most similar problems to this problem by using the retrieval component of the CBR Cycle class.
        :param request:
        :return:
        """
        import time
        explain = request.json_body.get('explain', False)
        language = request.json_body.get('language', 'de')  # default german explanation
        count = request.json_body.get('count', 5)

        only_solved = True
        if 'only_solved' in request.json_body:
            only_solved = request.json_body.get('only_solved')
        with_solution_proposals = False
        if 'with_solution_proposals' in request.json_body:
            with_solution_proposals = request.json_body.get('with_solution_proposals')

        # Formulate query
        problem_query = ProblemQuery(self.problem, explain=explain)

        timepoint1 = time.time()
        # Instantiate a cycle
        cycle = CBRCycle()
        cycle.set_input(problem_query)
        cycle.set_constraints({'only_solved': only_solved, 'with_solution_proposals': with_solution_proposals})
        problem_similarity_pairs = cycle.retrieve_cases(n=count)
        timepoint2 = time.time()

        # Build response dict
        response = dict()
        response['query'] = request.json_body
        response['query_time'] = (timepoint2 - timepoint1)
        response['results'] = [{'problem': problem, 'similarity': similarity} for problem, similarity in
                               problem_similarity_pairs]
        response['total_entries'] = len(problem_similarity_pairs)

        if explain:
            response['explanation'] = cycle.get_explanation(language=language)
        return response


###########################################################
# Problems
###########################################################


class ProblemResource(DatabaseResource):
    """Refers to rest/problems/{problem_id}
    """

    children = {
        'tags': ProblemTagCollection,
        'comments': ProblemCommentCollection,
        'solutions': ProblemSolutionCollection,
        'suggestions': ProblemSuggestionsCollection,
    }

    def __getitem__(self, key):
        return self.children[key](key, self)

    def update(self, request):
        problem = self.obj

        for key, value in request.json_body.items():
            # Pass through
            if key in ('title', 'description'):
                setattr(problem, key, value)

            # Special tag handling
            if key == 'tags' and isinstance(value, list):
                wanted_tags = set(value)
                if wanted_tags:
                    tags_to_append = DBSession.query(Tag).filter(Tag.name.in_(wanted_tags)).all()
                    found_tag_names = set([tag.name for tag in tags_to_append])

                    if found_tag_names != set(value):
                        raise ValueError(u"Could not find all tags for this problem: %s" %
                                         (u', '.join(wanted_tags - found_tag_names)),)
                    if not tags_to_append:
                        raise ValueError(u"No tags found or submitted.")
                    problem.tags = tags_to_append
                else:
                    problem.tags = []

            # (De-)Select solution
            if key == 'chosen_solution_id':
                if value is None:
                    problem.chosen_solution = None
                else:
                    solution = DBSession.query(Solution).filter(Solution.id == value).one()
                    problem.chosen_solution = solution
            # Alternative way via boolean
            if key == 'solved_for_problem_creator':
                if value is not False:
                    raise ValueError("Invalid value for 'solved_for_problem_creator'. Allowed values: False")
                else:
                    setattr(problem, key, value)

        DBSession.flush()


class ProblemCollection(Resource):
    """Refers to rest/problems/
    """
    __acl__ = [(Allow, Authenticated, 'create')]

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def __getitem__(self, problem_id):
        try:
            problem = DBSession.query(Problem).filter(Problem.id == problem_id).one()
        except NoResultFound:
            raise KeyError
        return ProblemResource(problem, self)

    def create(self, param):
        if param.get('tags') and isinstance(param.get('tags'), list):
            wanted_tags = set(param.get('tags'))
            if wanted_tags:
                tags_to_append = DBSession.query(Tag).filter(Tag.name.in_(wanted_tags)).all()

                found_tag_names = set([tag.name for tag in tags_to_append])

                if found_tag_names != wanted_tags:
                    raise ValueError(u"Could not find all tags for this problem: %s" %
                                     (u', '.join(wanted_tags - found_tag_names)),)
                if not tags_to_append:
                    raise ValueError(u"No tags found or submitted.")
                param['tags'] = tags_to_append
            else:
                param['tags'] = []
        else:
            del param['tags']
        problem = Problem(**param)
        DBSession.add(problem)
        DBSession.flush()
        return ProblemResource(problem, self)

    def all(self):
        return DBSession.query(Problem).all()


########################################################################################################################
# Tag-related
########################################################################################################################

class TagChildrenCollection(Resource):
    """Refers to rest/tags/{tag_name}/children
    """

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.parent_tag = self.parent.obj

    def create(self, name, description=""):
        if DBSession.query(Tag).filter(Tag.name == name).first():
            raise HTTPConflict(u"The tag '%s' could not be created, because it does already exist. Please use a "
                               u"different one." % name)
        else:
            tag = Tag(name=name, description=description)
            DBSession.add(tag)
            self.parent_tag.children.append(tag)
            DBSession.flush()
            return TagResource(tag, self.find_parent(TagCollection))

    def all(self):
        return self.parent_tag.children


class TagResource(DatabaseResource):
    """Refers to rest/tags/{tag_name}
    """
    id_field = 'name'

    children = {
        'children': TagChildrenCollection
    }

    def __getitem__(self, key):
        return self.children[key](key, self)

    def update(self, name=None, description=None):
        if name is not None:
            self.obj.name = name
        if description is not None:
            self.obj.description = description
        DBSession.flush()


class TagNotFoundResource(NonExistentResource):
    pass


class TagCollection(Resource):
    """Refers to rest/tags/
    """

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def __getitem__(self, name):
        try:
            tag = DBSession.query(Tag).filter(Tag.name == name).one()
        except NoResultFound:
            return TagNotFoundResource(name, self)
        return TagResource(tag, self)

    def create(self, name, description):
        if DBSession.query(Tag).filter(Tag.name == name).first():
            raise HTTPConflict(u"The tag '%s' could not be created, because it does already exist. Please use a "
                               u"different one." % name)
        else:
            tag = Tag(name=name, description=description)
            DBSession.add(tag)
            DBSession.flush()
            return TagResource(tag, self)

    def all(self, request=None):
        if request and request.GET.get('with_hierarchy') in (u'True', True, 1,):
            # Return all tags with optimized method
            return Tag.get_all_with_children()
        elif request and request.GET.get('tree_representation') in (u'True', True, 1,):
            return Tag.get_trees()
        else:
            return DBSession.query(Tag).all()

########################################################################################################################
# Solution
########################################################################################################################


class SolutionResource(DatabaseResource):

    def update(self, request):
        solution = self.obj
        data = request.json_body
        solution.text = data['text']
        DBSession.flush()

    def delete(self):
        DBSession.delete(self.obj)
        DBSession.flush()


class SolutionCollection(Resource):

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def __getitem__(self, solution_id):
        try:
            solution = DBSession.query(Solution).filter(Solution.id == solution_id).one()
        except NoResultFound:
            raise KeyError
        return SolutionResource(solution, self)

    def all(self):
        return DBSession.query(Solution).all()


########################################################################################################################
# Search
########################################################################################################################


def as_list(json_body, key):
    item = json_body.get(key, [])
    if isinstance(item, list):
        return item
    else:
        return [item]


class SearchTags(SearchableResource):

    def build_query(self, request):
        or_name = []

        base_query = DBSession.query(Tag).\
            outerjoin(problem_tags, problem_tags.c.tag_id == Tag.id).group_by(Tag.id).\
            order_by(func.count(problem_tags.c.tag_id).label('problem_count').desc())

        # by name
        if 'name' in request.json_body:
            for search_token in as_list(request.json_body, 'name'):
                or_name.append(Tag.name.ilike('%' + search_token + '%'))
            base_query = base_query.filter(or_(*or_name))
        if request.json_body.get('top_level'):
            base_query = base_query.filter(Tag.parent_id == None)
        return base_query


class SearchProblems(SearchableResource):

    def _get_children_of_tags(self, tags, filter_tags, pass_through=False):
        """Recursively grab certain tags and their children
        """
        ret = []
        for tag in tags:
            if pass_through or tag in filter_tags:
                ret.extend([tag] + self._get_children_of_tags(tag.children, filter_tags, pass_through=True))
            else:
                ret.extend(self._get_children_of_tags(tag.children, filter_tags))
        return ret

    def build_query(self, request):
        base_query = DBSession.query(Problem)
        if 'username' in request.json_body:
            base_query = base_query.join(User).filter(User.username == request.json_body['username'])
        if 'title' in request.json_body:
            # interprete title as string or list of strings
            title = as_list(request.json_body, 'title')
            if request.json_body.get('title_operator', 'or') == 'or':
                base_query = base_query.filter(or_(*[Problem.title.ilike('%' + candidate + '%')
                                                     for candidate in title]))
            else:
                base_query = base_query.filter(and_(*[Problem.title.ilike('%' + candidate + '%')
                                                      for candidate in title]))

        if 'description' in request.json_body:
            # interprete title as string or list of strings
            description = as_list(request.json_body, 'description')
            if request.json_body.get('description_operator', 'or') == 'or':
                base_query = base_query.filter(or_(*[Problem.description.ilike('%' + candidate + '%')
                                                     for candidate in description]))
            else:
                base_query = base_query.filter(and_(*[Problem.description.ilike('%' + candidate + '%')
                                                      for candidate in description]))

        if 'solved_for_problem_creator' in request.json_body:
            base_query = base_query.filter(Problem.solved_for_problem_creator ==
                                           request.json_body['solved_for_problem_creator'])
        tags = as_list(request.json_body, 'tags')
        with_sub_tags = request.json_body.get('with_sub_tags', False)
        if tags:
            if with_sub_tags:
                # Get first all tags by name, their sub tags and convert those to list tag name strings
                tags_by_name = DBSession.query(Tag).filter(Tag.name.in_([tag_name for tag_name in tags])).all()
                all_sub_tags = self._get_children_of_tags(Tag.get_trees(), tags_by_name)
                tags = [tag.name for tag in all_sub_tags]

            if request.json_body.get('tags_operator', 'or') == 'or':
                base_query = base_query.filter(Problem.tags.any(or_(*[Tag.name == tag for tag in tags])))
            else:
                # TODO: Needs to be optimized later (this induce a sub-query for each tag)
                tags_conjunction = []
                for tag in tags:
                    tags_conjunction.append(Problem.tags.any(Tag.name == tag))
                base_query = base_query.filter(and_(*tags_conjunction))

        if request.json_body.get('topic'):
            topic = DBSession.query(Topic).filter(Topic.id == int(request.json_body.get('topic'))).one()
            all_sub_tags = self._get_children_of_tags(Tag.get_trees(), topic.tags)
            base_query = base_query.filter(Problem.tags.any(or_(*[Tag.id == tag.id for tag in all_sub_tags])))
        if request.json_body.get('latest'):
            base_query = base_query.order_by(desc(Problem.created))
        if request.json_body.get('oldest'):
            base_query = base_query.order_by(asc(Problem.created))
        return base_query


class SearchUsers(SearchableResource):

    def build_query(self, request):
        base_query = DBSession.query(User.id.label('id'), User.username.label('username'))
        if 'id' in request.json_body:
            base_query = base_query.filter(User.id == request.json_body['id'])
        if 'username' in request.json_body:
            base_query = base_query.filter(User.username == request.json_body['username'])
        return base_query


class SearchSolutions(SearchableResource):

    def build_query(self, request):
        base_query = DBSession.query(Solution)
        if 'username' in request.json_body:
            base_query = base_query.join(User).filter(User.username == request.json_body['username'])
        if 'text' in request.json_body:
            base_query = base_query.filter(Solution.text.ilike('%' + request.json_body['text'] + '%'))
        return base_query


class SearchResource(Resource):
    __acl__ = default_acl

    children = {
        'tags': SearchTags,
        'problems': SearchProblems,
        'solutions': SearchSolutions,
        'users': SearchUsers,
    }

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def __getitem__(self, key):
        return self.children[key](key, self)


########################################################################################################################
# Users
########################################################################################################################

class SolutionsFromUserResource(Resource):

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def all(self):
        return DBSession.query(Solution).join(User, Solution.user_id == self.parent.obj.id).\
            order_by(Solution.created.desc()).all()


class ProblemsFromUserResource(Resource):

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def all(self):
        return DBSession.query(Problem).join(User, Problem.user_id == self.parent.obj.id).\
            order_by(Problem.created.desc()).all()


class UserStatusResource(Resource):

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.user = self.parent.obj

    def get_status(self, request):
        """Returns the status (including updates) since a certain date.
        :param request: Has to contain either one of two variables to determine the time span for updates:
                        * since - url-encoded, ISO-formatted date or
                        * days - positive integer
        :return: status
        """
        from datetime import timedelta
        from compass.helper import utcnow
        from dateutil import parser
        import pytz

        # Default value when missing date
        since = utcnow() - timedelta(days=3)

        if request.GET.get('since') and request.GET.get('days'):
            raise ValueError("Cannot determine the date presenting updates.")
        if request.GET.get('since'):
            since = parser.parse(request.GET.get('since'))
            if not since.tzinfo:
                # No time-zone given? Assume UTC then
                since = since.replace(tzinfo=pytz.utc)
            else:
                # Time-zone given? Make sure to convert it to utc.
                since = since.astimezone(tz=pytz.utc)
            if since > utcnow():
                raise ValueError("Date is in future.")

        if request.GET.get('days'):
            since = utcnow() - timedelta(int(request.GET.get('days')))

        # Collect problem ids that have changed related content
        problem_ids = set()

        all_new_comments = DBSession.query(Comment).\
            join(Problem, and_(Comment.problem_id == Problem.id,
                               Problem.user_id == int(self.user.id))).\
            filter(Comment.created > since).\
            order_by(Comment.created.desc()).\
            all()

        new_comments = defaultdict(list)
        for comment in all_new_comments:
            problem_ids.add(comment.problem_id)
            new_comments[comment.problem_id].append(comment)

        all_new_solutions = DBSession.query(Solution, ProblemSolutionProposal.problem_id).\
            join(ProblemSolutionProposal, ProblemSolutionProposal.solution_id == Solution.id).\
            join(Problem, and_(ProblemSolutionProposal.problem_id == Problem.id,
                               Problem.user_id == int(self.user.id))).\
            filter(Solution.created > since).all()

        new_solutions = defaultdict(list)
        for solution, problem_id in all_new_solutions:
            problem_ids.add(problem_id)
            new_solutions[problem_id].append(solution)

        problems_updated = []
        if problem_ids:
            problems_updated = DBSession.query(Problem).\
                filter(and_(Problem.user_id == int(self.user.id), Problem.id.in_(problem_ids))).\
                order_by(Problem.created.desc()).all()

        timespan = utcnow() - since
        status = dict()
        status['since'] = since.isoformat()
        status['since_in_days_and_hours'] = '%dd, %dh' % (timespan.days, timespan.seconds / 3600,)
        status['problems'] = dict()

        for problem in problems_updated:
            info = dict()
            info['count_new_comments'] = len(new_comments[problem.id])
            info['count_new_solutions'] = len(new_solutions[problem.id])
            info['new_comments'] = new_comments[problem.id]
            info['new_solutions'] = new_solutions[problem.id]
            status['problems'][problem.id] = info

        return status


class UserResource(DatabaseResource):
    """Refers to rest/users/{username}
    """
    id_field = 'username'
    acl_lineage = False
    __acl__ = [admin_access]

    children = {
        'solutions': SolutionsFromUserResource,
        'problems': ProblemsFromUserResource,
        'status': UserStatusResource,
    }

    def __getitem__(self, key):
        return self.children[key](key, self)

    def update(self, request):
        from compass.security import create_password_hash_with_random_salt

        data = request.json_body
        user = self.obj

        email = data.get('email')
        email_repeated = data.get('email_repeated')
        password = data.get('password')
        password_repeated = data.get('password_repeated')

        if email or email_repeated:
            if email != email_repeated:
                raise ValueError(u"Entered e-mail Addresses do not match!")
            user.email = email
        if password or password_repeated:
            if password != password_repeated:
                raise ValueError(u"Entered passwords do not match!")
            if len(password) < 6:
                raise ValueError(u"Password needs to be at least 6 characters long!")
            if password == user.username:
                raise ValueError(u"Username and password should not be identical!")
            user.password_hash, user.password_salt = create_password_hash_with_random_salt(password)
        DBSession.flush()


class UserCollection(Resource):
    """Refers to rest/users/
    """
    __acl__ = [admin_access, (Deny, Authenticated, 'create'),  # Don't let users create another account.
                             (Allow, Everyone, 'create')]
    acl_lineage = False

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def __getitem__(self, username):
        try:
            user = DBSession.query(User).filter(User.username == username).one()
        except NoResultFound:
            raise KeyError
        return UserResource(user, self)

    def create(self, username, email, email_repeated, password, password_repeated):
        from compass.security import create_password_hash_with_random_salt
        # Be forgiving.. considerably.
        username = username.strip()
        email = email.strip()
        email_repeated = email_repeated.strip()

        if email != email_repeated:
            raise ValueError(u"Entered e-mail Addresses do not match!")
        if password != password_repeated:
            raise ValueError(u"Entered passwords do not match!")
        if len(password) <= 6:
            raise ValueError(u"Password needs to be at least 6 characters long!")
        if password == username:
            raise ValueError(u"Username and password should not be identical!")
        password_hash, password_salt = create_password_hash_with_random_salt(password)
        user = User(username=username, email=email, password_hash=password_hash, password_salt=password_salt)
        DBSession.add(user)
        DBSession.flush()
        return UserResource(user, self)

    def all(self):
        return DBSession.query(User).all()


class TopicTagResource(DatabaseResource):
    """Refers to rest/topics/{topic_id}/tags/{tag_name}
    """
    id_field = 'name'
    acl_lineage = True

    def unbind(self, tag_name):
        self.parent.unbind(tag_name)


class NotYetBoundTopicTagResource(NonExistentResource):
    """Refers to rest/topics/{topic_id}/tags/{tag_name}
    """
    def bind(self, tag_name):
        self.parent.bind(tag_name)


class TopicTagCollection(Resource):
    """Refers to rest/topics/{topic_id}/tags
    """

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent
        self.topic = self.parent.obj

    def __getitem__(self, tag_name):
        try:
            tag = self.topic.tags.filter(Tag.name == tag_name).one()
        except NoResultFound:
            return NotYetBoundTopicTagResource(tag_name, self)
        return TopicTagResource(tag, self)

    def all(self):
        return self.topic.tags.all()

    def bind(self, tag_name):
        if self.topic.tags.filter(Tag.name == tag_name).first():
            raise ValueError(u"Tag '{}' is already bound.".format(tag_name))
        try:
            tag = DBSession.query(Tag).filter(Tag.name == tag_name).one()
        except NoResultFound:
            raise ValueError(u"Did not find tag '{}'.".format(tag_name))
        self.topic.tags.append(tag)

    def unbind(self, tag_name):
        try:
            tag = self.topic.tags.filter(Tag.name == tag_name).one()
        except NoResultFound:
            raise ValueError(u"Cannot remove tag '{}', because it is not bound to this topic.".format(tag_name))
        self.topic.tags.remove(tag)

    def delete(self):
        for tag in self.topic.tags.all():
            DBSession.delete(tag)


class TopicResource(DatabaseResource):
    """Refers to rest/topics/{topic_id}
    """

    children = {
        'tags': TopicTagCollection,
    }

    def __getitem__(self, key):
        return self.children[key](key, self)

    def update(self, request):

        topic = self.obj

        for key, value in request.json_body.items():
            # Pass through
            if key in ('name', 'description'):
                setattr(topic, key, value)

            # Special tag handling
            if key == 'tags' and isinstance(value, list):
                wanted_tags = set(value)
                if wanted_tags:
                    tags_to_append = DBSession.query(Tag).filter(Tag.name.in_(wanted_tags)).all()
                    found_tag_names = set([tag.name for tag in tags_to_append])

                    if found_tag_names != set(value):
                        raise ValueError(u"Could not find all tags for this topic: %s" %
                                         (u', '.join(wanted_tags - found_tag_names)),)
                    topic.tags = tags_to_append
                else:
                    topic.tags = []
        DBSession.flush()


class TopicCollection(Resource):
    """Refers to rest/topics/
    """

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def __getitem__(self, topic_id):
        try:
            topic = DBSession.query(Topic).filter(Topic.id == topic_id).one()
        except NoResultFound:
            raise KeyError
        return TopicResource(topic, self)

    def create(self, param):
        if param.get('tags') and isinstance(param.get('tags'), list):
            wanted_tags = set(param.get('tags'))
            if wanted_tags:
                tags_to_append = DBSession.query(Tag).filter(Tag.name.in_(wanted_tags)).all()

                found_tag_names = set([tag.name for tag in tags_to_append])
                if found_tag_names != set(param.get('tags')):
                    raise ValueError(u"Could not find all tags for this topic: %s" %
                                     (u', '.join(wanted_tags - found_tag_names)),)
                param['tags'] = tags_to_append
            else:
                param['tags'] = []
        else:
            del param['tags']
        topic = Topic(**param)
        DBSession.add(topic)
        DBSession.flush()
        return TopicResource(topic, self)

    def all(self):
        return DBSession.query(Topic).all()


class RestAPI(Resource):
    __acl__ = default_acl

    children = {
        'tags': TagCollection,
        'problems': ProblemCollection,
        'solutions': SolutionCollection,
        'search': SearchResource,
        'users': UserCollection,
        'topics': TopicCollection,
    }

    def __init__(self, name, parent):
        self.__name__ = name
        self.parent = parent

    def __getitem__(self, key):
        return self.children[key](key, self)


# Default access directive
class RootFactory(object):
    __acl__ = []
    __parent__ = None
    __name__ = ''

    children = {
        'rest': RestAPI,
    }

    def __init__(self, request):
        self.request = request

    def __getitem__(self, key):
        return self.children[key](key, self)