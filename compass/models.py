# coding=utf-8
from collections import defaultdict
from sqlalchemy import (
    func,
    Table,
    Column,
    ForeignKey,
    Index,
    Integer,
    Text,
    String,
    DateTime,
    Unicode,
    UnicodeText,
    event,
    Boolean,
    and_,
    UniqueConstraint,
    ForeignKeyConstraint,
    Enum
)

from sqlalchemy.ext.declarative import (
    declarative_base,
    declared_attr
)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm.attributes import set_committed_value
from sqlalchemy.orm import (
    object_session,
    relationship,
    scoped_session,
    sessionmaker,
    backref,
    aliased
)

from zope.sqlalchemy import ZopeTransactionExtension

import helper
from sqlalchemy_decorator import UTCDateTime
from pyramid.security import Allow
from pyramid.security import Everyone, Authenticated


DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class UserReferenceMixin(object):
    @declared_attr
    def user_id(cls):
        return Column(Integer, ForeignKey('user.id'), nullable=False)

    @declared_attr
    def user(cls):
        return relationship('User')

    @property
    def __acl__(cls):
        """Owner is allowed to do all stuff"""
        acl = [(Allow, cls.user.username, 'create'),
               (Allow, cls.user.username, 'read'),
               (Allow, cls.user.username, 'update'),
               (Allow, cls.user.username, 'delete'),
               ]
        return acl


class CreatedTimestampMixin(object):
    created = Column(UTCDateTime(timezone=True), nullable=False, default=helper.utcnow)


class ModifiedTimestampMixin(object):
    modified = Column(UTCDateTime(timezone=True), nullable=True, onupdate=helper.utcnow)


def serialize_datetime(dt):
    return helper.default_strftime(dt)

# Model description


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(Unicode, unique=True)
    password_hash = Column(String(128))
    password_salt = Column(String(256))
    email = Column(Unicode, unique=True)
    secondary_title = Column(Unicode)
    registered = Column(UTCDateTime(timezone=True), default=helper.utcnow())
    last_login = Column(UTCDateTime(timezone=True), default=helper.utcnow())

    def __repr__(self):
        return u'<User(id={0},username={1},email={2},last_login={3})>'.format(self.id, self.username, self.email,
                                                                              serialize_datetime(self.last_login)
                                                                              ).encode('utf8')

    @property
    def __acl__(self):
        """Owner is allowed to do all stuff"""
        acl = [(Allow, self.username, 'create'),
               (Allow, self.username, 'read'),
               (Allow, self.username, 'update'),
               (Allow, self.username, 'delete'),
               ]
        return acl

    def __json__(self, request):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'registered': serialize_datetime(self.registered),
            'last_login': serialize_datetime(self.last_login),
        }

    def __json__restricted__(self, request=None):
        return {
            'id': self.id,
            'username': self.username,
        }


@event.listens_for(User, 'before_insert')
@event.listens_for(User, 'before_update')
def valid_user_name(mapper, connection, target):
    import re
    assert re.match(u"^[a-zA-Z0-9_.-]+$", target.username) is not None,\
        'Username invalid. Only use alphanumerical characters or other like underscore, dot or minus.'
    assert re.match(u"^[^@ ]+@+[^@ ]+\.[^@ ]+$", target.email) is not None,\
        'E-mail invalid.'

# Association table for tags and problems
problem_tags = Table('problem_tags', Base.metadata,
                     Column('problem_id', Integer, ForeignKey('problem.id')),
                     Column('tag_id', Integer, ForeignKey('tag.id'))
                     )


# Association table for solutions
class ProblemSolutionProposal(UserReferenceMixin, CreatedTimestampMixin, Base):
    __tablename__ = 'problem_solution_proposals'
    problem_id = Column(Integer, ForeignKey('problem.id'), primary_key=True)
    solution_id = Column(Integer, ForeignKey('solution.id'), primary_key=True)
    problem = relationship('Problem', backref=backref('problem_solutions_association', lazy='dynamic',
                                                      cascade='all, delete-orphan'))  # Delete this row if any
                                                                                      # associated problem or solution
    solution = relationship('Solution', backref=backref('as_solution_proposals',      # was deleted
                                                        cascade='all, delete-orphan',
                                                        lazy='dynamic'))
    # Usefulness feedback
    usefulness = relationship('SolutionUsefulness', backref='problem_solution_proposal',
                              cascade='all, delete-orphan', lazy='dynamic')

    def __init__(self, solution=None, user=None):
        self.solution = solution
        self.user = user

    def __repr__(self):
        return u'<ProblemSolutionProposal([problem_id={0},solution_id={1}],usefulness_responses={2})>'.\
            format(self.problem_id, self.solution_id, self.usefulness.count()).encode('utf8')

    @property
    def avg_usefulness(self):
        return object_session(self).query(FeedbackType.id, FeedbackType.serialization_name,
                                          func.avg(SolutionUsefulness.response)).\
            outerjoin(SolutionUsefulness, and_(FeedbackType.id == SolutionUsefulness.feedback_type_id,
                                               SolutionUsefulness.solution_id == self.solution_id,
                                               SolutionUsefulness.problem_id == self.problem_id)).\
            filter(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_AND_PROBLEM).\
            group_by(FeedbackType.id).all()

    @property
    def each_usefulness_responses(self):
        return object_session(self).query(FeedbackType.id, FeedbackType.serialization_name,
                                          SolutionUsefulness.response,
                                          func.count(SolutionUsefulness.response)).\
            outerjoin(SolutionUsefulness, and_(FeedbackType.id == SolutionUsefulness.feedback_type_id,
                                               SolutionUsefulness.solution_id == self.solution_id,
                                               SolutionUsefulness.problem_id == self.problem_id)).\
            filter(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_AND_PROBLEM).\
            group_by(FeedbackType.id, SolutionUsefulness.response).all()


class Problem(UserReferenceMixin, CreatedTimestampMixin, ModifiedTimestampMixin, Base):
    __tablename__ = 'problem'
    id = Column(Integer, primary_key=True)
    title = Column(Unicode, nullable=False)
    description = Column(UnicodeText, nullable=False)
    # TODO: Context description
    # many-to-many relationship
    tags = relationship('Tag', secondary=problem_tags, backref=backref('problems', lazy='dynamic'), lazy='dynamic')
    # one-to-many relationship
    comments = relationship('Comment', passive_updates=False, order_by='Comment.id', backref='problem',
                            cascade='all, delete-orphan', lazy='dynamic')
    # many-to-many relationship (using Association object with proxy)
    solution_proposals = association_proxy('problem_solutions_association', 'solution')

    chosen_solution_id = Column(Integer, ForeignKey('solution.id'))
    chosen_solution = relationship('Solution')

    @hybrid_property
    def solved_for_problem_creator(self):
        return self.chosen_solution_id is not None

    @solved_for_problem_creator.expression
    def solved_for_problem_creator(cls):
        return cls.chosen_solution_id != None

    @solved_for_problem_creator.setter
    def solved_for_problem_creator(self, value):  # Alternative way to to set to state "unsolved"
        if value is False:
            self.chosen_solution = None
        else:
            raise AttributeError("Setter of attribute 'solved_for_problem_creator' accepts only the following"
                                 "values: False")

    @hybrid_method
    def solutions_sorted(self):
        """Return all solutions sorted by: IsSolved?, sum of usefulness votes, sum of feedback votes, date proposed
        """
        session = object_session(self)
        alias_solution = aliased(Solution)
        alias_problem = aliased(Problem)
        # Sub-Query for Ranking of Solution x Problem
        usefulness_sub_query = session.query(SolutionUsefulness.solution_id.label('rank_usefulness_solution_id'),
                                             SolutionUsefulness.problem_id.label('rank_usefulness_problem_id'),
                                             func.sum(SolutionUsefulness.response).label('usefulness')).\
            join(FeedbackType, FeedbackType.id == SolutionUsefulness.feedback_type_id).\
            filter(and_(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_AND_PROBLEM,
                        SolutionUsefulness.solution_id == alias_solution.id,
                        SolutionUsefulness.problem_id == alias_problem.id)).\
            group_by(SolutionUsefulness.solution_id, SolutionUsefulness.problem_id).subquery()

        # Sub-Query for Ranking of Solution
        feedback_sub_query = session.query(SolutionFeedback.solution_id.label('rank_feedback_solution_id'),
                                           func.sum(SolutionFeedback.response).label('feedback')).\
            join(FeedbackType, FeedbackType.id == SolutionFeedback.feedback_type_id).\
            filter(and_(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_ONLY,
                        SolutionFeedback.solution_id == alias_solution.id)).\
            group_by(SolutionFeedback.solution_id).subquery()

        query = session.query(alias_solution).join(ProblemSolutionProposal,
                                                   and_(alias_solution.id == ProblemSolutionProposal.solution_id,
                                                        ProblemSolutionProposal.problem_id == int(self.id))).\
            join(alias_problem, alias_problem.id == ProblemSolutionProposal.problem_id).\
            outerjoin(usefulness_sub_query,
                      and_(usefulness_sub_query.c.rank_usefulness_solution_id == ProblemSolutionProposal.solution_id,
                           usefulness_sub_query.c.rank_usefulness_problem_id == ProblemSolutionProposal.problem_id)).\
            outerjoin(feedback_sub_query,
                      feedback_sub_query.c.rank_feedback_solution_id == ProblemSolutionProposal.solution_id).\
            order_by(alias_problem.chosen_solution_id.is_(alias_solution.id).desc(),
                     usefulness_sub_query.c.usefulness.desc(),
                     feedback_sub_query.c.feedback.desc(),
                     ProblemSolutionProposal.created.desc(),
        )
        return query.all()

    def __repr__(self):
        return u'<Problem(id={0},title={1},description={2},created={3})>'.format(self.id, self.title[:50],
                                                                                 self.description[:50],
                                                                                 serialize_datetime(self.created)
                                                                                 ).encode('utf8')

    def __json__(self, request):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'created': serialize_datetime(self.created),
            'modified': serialize_datetime(self.modified),
            'tags': [tag.__json__(request) for tag in self.tags],
            'solutions': [solution.__json__(request) for solution in self.solutions_sorted()],
            'comments': [comment.__json__(request) for comment in self.comments],
            'solved_for_problem_creator': self.solved_for_problem_creator,
            'chosen_solution_id': self.chosen_solution_id,
            'user': self.user.__json__restricted__(request),
        }


@event.listens_for(Problem, 'before_insert')
@event.listens_for(Problem, 'before_update')
def valid_problem(mapper, connection, target):
    assert len(target.title) != 0, 'Problem title is empty.'
    assert len(target.description) != 0, 'Problem description is empty.'


class Tag(Base):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText(255), nullable=False, unique=True)
    description = Column(UnicodeText, default=u"")
    parent_id = Column(Integer, ForeignKey('tag.id'))
    children = relationship('Tag', backref=backref('parent', remote_side=[id]), order_by='Tag.id',
                            cascade='all, delete-orphan')
    # Number of occurrences of instances of this tag
    occurrences = 0
    # (Logarithmic) probability of witnessing an (sub-)instance of this tag
    log_probability_of_instance = None

    def __init__(self, name, description=u""):
        self.name = name
        self.description = description

    def __repr__(self):
        return u'<Tag(name=%s)>' % self.name

    def __json__(self, request):
        ret_dict = {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'problem_count': self.problems.count(),
        }
        # optional: Append all children if needed
        def any_true(request, variables):
            trueish = lambda value: value is True or value == u'True' or value == u'true' or value == 1
            return any([trueish(request.GET.get(variable)) or trueish(request.POST.get(variable))
                        for variable in variables])

        if request and any_true(request, ['with_hierarchy', 'tree_representation']):
            ret_dict['children'] = [child.__json__(request) for child in self.children]
        else:
            ret_dict['children'] = [{'id': child.id, 'name': child.name, 'description': child.description} for child
                                    in self.children]
        return ret_dict

    @classmethod
    def from_param(cls, **params):
        return cls(name=params.get('name'))

    @classmethod
    def get_all_with_children(cls):
        """Return all elements and their children in a rather efficient way.
        """
        nodes = DBSession.query(Tag).all()

        # Collect parent-child relations
        children = defaultdict(list)

        for node in nodes:
            if node.parent:
                children[node.parent.id].append(node)

        # Set collected values
        for node in nodes:
            set_committed_value(node, 'children', children[node.id])

        return nodes


    @classmethod
    def get_trees(cls):
        """Return all top-level elements of the existing trees. Access to children attribute will be performed in
        a rather efficient way.
        :rtype : list of Tag
        """
        nodes = DBSession.query(Tag).all()

        # Collect parent-child relations
        children = defaultdict(list)
        # Collect root objects
        root_nodes = []

        for node in nodes:
            if node.parent:
                children[node.parent.id].append(node)
            else:
                root_nodes.append(node)

        # Set collected values
        for node in nodes:
            set_committed_value(node, 'children', children[node.id])

        return root_nodes

    def get_all_sub_children(self):
        """Returns all (sub)-children of this node."""
        ret_children = []
        for child in self.children:
            ret_children.extend(child.get_all_sub_children())
            ret_children.append(child)
        return ret_children

    def get_root(self):
        """Returns the root object"""
        current = self
        while not current.parent is None:
            current = current.parent
        return current

    def get_ancestors(self, include_self=True):
        """Returns all ordered ancestors (from closest to farthest)."""
        ancestors = []
        current = self
        if include_self:
            ancestors.append(current)
        while not current.parent is None:
            ancestors.append(current)
            current = current.parent
        return ancestors

    @classmethod
    def get_virtual_root(cls):
        return cls(name=u'virtual_root')

    def is_virtual_root(self):
        return self.name == u'virtual_root'


@event.listens_for(Tag, 'before_insert')
@event.listens_for(Tag, 'before_update')
def valid_tag(mapper, connection, target):
    assert len(target.name) != 0, "Tag name is empty."


class Comment(UserReferenceMixin, CreatedTimestampMixin, ModifiedTimestampMixin, Base):
    __tablename__ = 'comment'
    id = Column(Integer, primary_key=True)
    problem_id = Column(Integer, ForeignKey('problem.id', onupdate='cascade'))
    parent_id = Column(Integer, ForeignKey('comment.id'))
    text = Column(UnicodeText, nullable=False)
    children = relationship('Comment', backref=backref('parent', remote_side=[id]), order_by='Comment.id',
                            cascade='all, delete-orphan')

    def __repr__(self):
        return u'<Comment(id={0},parent={1},text={2})>'.format(self.id, str(self.parent_id), self.text[:50]).encode('utf8')

    def __json__(self, request):
        return {
            'id': self.id,
            'text': self.text,
            'created': serialize_datetime(self.created),
            'modified': serialize_datetime(self.modified),
            'user': self.user.__json__restricted__(request),
        }


@event.listens_for(Comment, 'before_insert')
@event.listens_for(Comment, 'before_update')
def valid_comment(mapper, connection, target):
    assert len(target.text) != 0, "Comment text is empty."


class FeedbackType(Base):
    __tablename__ = 'feedback_type'
    id = Column(Integer, primary_key=True)
    serialization_name = Column(Unicode(255))
    name = Column(Unicode(255))
    description = Column(Unicode)
    min_value = Column(Integer)
    max_value = Column(Integer)
    # Scopes which define for which feedback this type is applicable
    SCOPE_SOLUTION_ONLY = 'SolutionOnly'               # Feature for ratings of: Solution
    SCOPE_SOLUTION_AND_PROBLEM = 'SolutionAndProblem'  # Feature for ratings of: Solution x Problem
    scope = Column(Enum(SCOPE_SOLUTION_ONLY, SCOPE_SOLUTION_AND_PROBLEM))

    def __repr__(self):
        return u'<FeedbackType(id={0},serial_name={1},name={2},min_value={3},max_value={4},desc={5})>'.format(
            self.id,
            self.serialization_name,
            self.name,
            self.min_value,
            self.max_value,
            self.description[:50]).encode('utf8')

    def __init__(self, serialization_name, name, description, min_value, max_value, scope):
        self.serialization_name = serialization_name
        self.name = name
        self.description = description
        self.min_value = min_value
        self.max_value = max_value
        self.scope = scope


class SolutionFeedback(UserReferenceMixin, CreatedTimestampMixin, Base):
    __tablename__ = 'solution_feedback'
    id = Column(Integer, primary_key=True)
    solution_id = Column(Integer, ForeignKey('solution.id'))
    feedback_type_id = Column(Integer, ForeignKey('feedback_type.id'), nullable=False)
    response = Column(Integer, nullable=False)
    feedback_type = relationship('FeedbackType')
    # One vote per user for each Solution-FeedbackType-combination
    __table_args__ = (UniqueConstraint('solution_id', 'feedback_type_id', 'user_id'),)

    def __repr__(self):
        return u'<SolutionFeedback(id={0},solution_id={1}, feedback_type={2},response={3} [Scale: {4}-{5}>'.\
            format(self.id, self.solution_id, self.feedback_type.name, self.response, self.feedback_type.min_value,
                   self.feedback_type.max_value).encode('utf8')


@event.listens_for(SolutionFeedback, 'before_insert')
@event.listens_for(SolutionFeedback, 'before_update')
def response_values_in_valid_range(mapper, connection, target):
    assert target.feedback_type.scope == FeedbackType.SCOPE_SOLUTION_ONLY, 'used wrong feedback type for this table.' \
                                                                           'Expected: {}, got: {}'.format(
        FeedbackType.SCOPE_SOLUTION_ONLY, target.feedback_type.scope)
    assert target.feedback_type.min_value <= target.response <= target.feedback_type.max_value,\
        'value of solution feedback is not in valid range of [%i-%i]' % (target.feedback_type.min_value,
                                                                         target.feedback_type.max_value)


class Solution(UserReferenceMixin, CreatedTimestampMixin, ModifiedTimestampMixin, Base):
    __tablename__ = 'solution'
    id = Column(Integer, primary_key=True)
    text = Column(UnicodeText, nullable=False)
    feedback = relationship('SolutionFeedback', backref='solution', cascade='all, delete-orphan', lazy='dynamic')

    @property
    def feedback_avg_criteria(self):
        return object_session(self).query(FeedbackType.id, FeedbackType.serialization_name,
                                          func.avg(SolutionFeedback.response)).\
            outerjoin(SolutionFeedback, and_(FeedbackType.id == SolutionFeedback.feedback_type_id,
                                             SolutionFeedback.solution_id == self.id)).\
            filter(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_ONLY).\
            group_by(FeedbackType.id).all()

    @property
    def each_feedback_responses(self):
        return object_session(self).query(FeedbackType.id, FeedbackType.serialization_name,
                                          SolutionFeedback.response,
                                          func.count(SolutionFeedback.response)).\
            outerjoin(SolutionFeedback, and_(FeedbackType.id == SolutionFeedback.feedback_type_id,
                                             SolutionFeedback.solution_id == self.id)).\
            filter(FeedbackType.scope == FeedbackType.SCOPE_SOLUTION_ONLY).\
            group_by(FeedbackType.id, SolutionFeedback.response).all()

    def __repr__(self):
        return u'<Solution(id={0},text={1},feedback_responses={2})>'.format(self.id, self.text[:50],
                                                                            self.feedback.count()).encode('utf8')

    def __json__(self, request):
        return {
            'id': self.id,
            'text': self.text,
            'created': serialize_datetime(self.created),
            'modified': serialize_datetime(self.modified),
            'user': self.user.__json__restricted__(request),
        }


@event.listens_for(Solution, 'before_insert')
@event.listens_for(Solution, 'before_update')
def valid_solution(mapper, connection, target):
    assert len(target.text) != 0, "Solution text is empty."


class SolutionUsefulness(UserReferenceMixin, CreatedTimestampMixin, Base):
    """Feedback for the tuple Problem x Solution -> R x ... x R in terms of usefulness
    """
    __tablename__ = 'solution_usefulness'
    id = Column(Integer, primary_key=True)
    # composite foreign keys
    problem_id = Column(Integer)
    solution_id = Column(Integer)
    feedback_type_id = Column(Integer, ForeignKey('feedback_type.id'), nullable=False)
    response = Column(Integer, nullable=False)
    feedback_type = relationship('FeedbackType')

    __table_args__ = (ForeignKeyConstraint([solution_id, problem_id],
                                           [ProblemSolutionProposal.solution_id, ProblemSolutionProposal.problem_id]),
                      # One feedback for Problem x Solution x Feedback Type per user
                      UniqueConstraint('problem_id', 'solution_id', 'user_id', 'feedback_type_id'),)

    def __repr__(self):
        return u'<SolutionUsefulness(id={},problem_id={},solution_id=[},user_id={}, user_id={}, feedback_type={},' \
               u'response={}, [Scale: {}-{}]>'.\
            format(self.id, self.problem_id, self.solution_id, self.user_id, self.feedback_type.name, self.response,
                   self.feedback_type.min_value, self.feedback_type.max_value).encode('utf8')


@event.listens_for(SolutionUsefulness, 'before_insert')
@event.listens_for(SolutionUsefulness, 'before_update')
def usefulness_response_values_in_valid_range(mapper, connection, target):
    assert target.feedback_type.scope == FeedbackType.SCOPE_SOLUTION_AND_PROBLEM, 'used wrong feedback type for this '\
                                                                                  'table. Expected: {}, got: {}'.format(
        FeedbackType.SCOPE_SOLUTION_ONLY, target.feedback_type.scope)
    assert target.feedback_type.min_value <= target.response <= target.feedback_type.max_value,\
        'value of solution feedback is not in valid range of [%i-%i]' % (target.feedback_type.min_value,
                                                                         target.feedback_type.max_value)


# Association table for tags and topics
topic_tags = Table('topic_tags', Base.metadata,
                   Column('topic_id', Integer, ForeignKey('topic.id')),
                   Column('tag_id', Integer, ForeignKey('tag.id'))
                   )


class Topic(Base):
    """Topics represents a collection of tags and offers a more broad view.
    """
    __tablename__ = 'topic'
    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText(255), nullable=False, unique=True)
    description = Column(UnicodeText, default=u"")
    # many-to-many relationship
    tags = relationship('Tag', secondary=topic_tags, backref='topics', lazy='dynamic')

    def __repr__(self):
        return u'<Topic(name=%s)>' % self.name

    def __json__(self, request):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'tags': [tag.__json__(request) for tag in self.tags],
        }

@event.listens_for(Topic, 'before_insert')
@event.listens_for(Topic, 'before_update')
def valid_topic(mapper, connection, target):
    assert len(target.name) != 0, "Topic name is empty."
