from pyramid.config import Configurator
from pyramid.httpexceptions import HTTPNotFound
from sqlalchemy import engine_from_config
from .resources import RootFactory
from .security import (
    compass_authentication,
    compass_authorization,
)
from .serializer import json_renderer

from .models import (
    DBSession,
    Base,
)


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings,
                          root_factory=RootFactory,
                          authentication_policy=compass_authentication,
                          authorization_policy=compass_authorization)

    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static')
    config.add_renderer('json', json_renderer)

    # Allow redirect when having 404s to trailing urls
    config.add_notfound_view(lambda request: HTTPNotFound(), append_slash=True)
    config.scan()

    return config.make_wsgi_app()