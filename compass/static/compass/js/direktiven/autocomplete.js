var myApp = angular.module('app', []).controller('autoCompleteController', function($scope, $http, $log, $timeout) {

    $scope.results = [];
    $scope.tags = [];
    $scope.selectedTag =[];
        

    $scope.loggedIn = function()
    {
        $scope.loadStyle();
        console.log("loggedIn() wird ausgeführt.");
            if (localStorage.getItem("loggedInUserBase64") != null)
            {
                $scope.userLoggedIn = localStorage.getItem("loggedInUserName");
                return true;
            }
            else
            { 
                return false; 
            }  

    }


    $scope.search = function(title)
    {
    	console.log("search() arbeitet.");
        $http({
        url: '/rest/search/problems/',
        method: "POST",
        data: $scope.createJSON_title(title),
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
             }).success(function (data, status, headers, config) {
                      $scope.results = data;
                      console.log($scope.results);
                       }).error(function (data, status, headers, config) {
            console.log("Suche nicht erfolgreich!");
                               });


    }

    $scope.getTags = function(){
    $http({
    url: "/rest/tags/", 
    headers:{'Authorization':localStorage.getItem("loggedInUserBase64")},
    method: "GET",
    }).success(function(data, status, headers, response) {
        console.log("getTags() arbeitet");
    $scope.tags = data;
    }).error(function(data, status) {
    if (status == 403)
        $scope.valid = "Benutzerdaten sind falsch";
    if (status == 404)
        $scope.valid = "Benutzerdaten sind falsch";
     else
    console.log("fehlgeschlagen");
    })


}



}).directive('autoCompleteProblem', function factory($timeout) 
{
    var directiveDefinitionObject = 
    {
        restrict: 'A',
        template: '<div><form class="ym-searchform">'+
        '<a id="problemsuche"></a><label class="invisibleLabel" for="problemsSearch">Nach einer Frage suchen und mit den Pfeiltasten in die Auto-Vervollständigung springen.</label><input role="combobox"  aria-autocomplete="list" aria-expanded="false"  aria-owns="problemsSelect" results=5 list="problemsField" ng-focus="select()" autocomplete="off" id="problemsSearch" ng-keydown="checkIfKeyDown($event)" class="ym-searchfield" ng-change="startHideSearch(searchpara)" type="search" ng-model="searchpara" placeholder="Nach einem Problem suchen" ng-blur="delayHide()"/>' + 
        '<button class="ym-searchbutton" ng-focus="checkIfSelectFocus(searchpara)" ng-click="searchForPara(searchpara)">Suchen</button>' + 
        '<p>' + 
        '<label class="invisibleLabel" for="problemsSelect">Hier werden Sucherergebnisse während der Eingabe angezeigt</label><select aria-live="polite" ng-mousedown="setFocus()" ng-hide="see1" id="problemsSelect" ng-keyup="moveByKey($event, selectedTitle.id)" role="listbox" ng-keydown="checkKeys($event, selectedTitle.id, selectedTitle)" ng-model="selectedTitle" size="3" ng-click="setFocus()" ng-options="r.title for r in results.results| filter: searchpara" ng-dblclick="loadProblem(selectedTitle.id)" ng-blur="delayHide()">' + 
        '<option value="">-- Gefundene Probleme --</option>'+'</select>' + 
        '</p>' + 
        '</form>'+
        '</div>',
        replace: 'true',

        scope: true,
        link: function (scope, element, attrs)
        {       
           scope.selectIsFocused = false; 
           scope.see1 = true;

            scope.hideSelectField1 = function()
            {
                scope.see1 = false;
                document.getElementById('problemsSearch').setAttribute('aria-expanded', 'true');
                        if (scope.results == "")
                        {
                            scope.see1 = true;
                            document.getElementById('problemsSearch').setAttribute('aria-expanded', 'false');
                        }
            }

            scope.select = function()
            {
               document.getElementById("problemsSearch").select();
            }
               
            scope.setFocus = function()
            {
                scope.selectIsFocused = true;
            }

            scope.hide = function()
            { 
            console.log(scope.selectIsFocused+" Select");
                if (scope.selectIsFocused == false)
                {
                scope.see1=true;
                document.getElementById('problemsSearch').setAttribute('aria-expanded', 'false');
              }
            }
                  

            scope.delayHide = function()
            {
                 scope.selectIsFocused = false;
                 $timeout(function()
                 {
                    if (scope.selectIsFocused == true)
                      scope.see1 = false;
                    else
                      {
                        scope.see1 = true;
                        document.getElementById('problemsSearch').setAttribute('aria-expanded', 'false');
                      }
                 },110);

            }

            scope.checkIfSelectFocus = function(searchpara){
              scope.selectIsFocused = true;
            }

            scope.startHideSearch = function(searchpara)
            {
                    scope.hideSelectField1();
                    console.log("results = "+scope.results);
                    if (searchpara.length >=2)
                    {
                      scope.search(searchpara);
                                        if (document.getElementById('problemsSearch'))
                                        {
                                          {
                                            console.log("aria-expanded = true");
                                            document.getElementById('problemsSearch').setAttribute('aria-expanded', 'true');
                                          }
                                        }
                                        if (searchpara == "")
                                            {
                                              scope.see1 = true;
                                            }
                                      
                    
                    }
            }

            scope.checkIfKeyDown = function(ev)
            {
                console.log(ev);
                if (ev.which==40)
                 {    
                     $timeout(function()
                     {   
                     document.getElementById('problemsSelect').options[0].selected = true;
                     document.getElementById('problemsSelect').focus();
                     scope.selectIsFocused = true;
                     ev.preventDefault();
                     });
                 }
            }


            scope.checkIfKeyUp = function($event, selectedTitle)
            {
                console.log(selectedTitle);
                 if ($event.which==38)
                 {   
                    if(document.getElementById('problemsSelect').options[0].selected) 
                     {                    
                      $timeout(function()
                                          { 
                                            document.getElementById("problemsSearch").focus();
                                            document.getElementById("problemsSearch").select();
                                            scope.selectIsFocused = true;
                                            $event.preventDefault();
                                          });
                     }
                  }
            }

            scope.loadProblem = function(id)
            {
               localStorage.setItem("problemId",id);
               open("problem.html","_self");  
            }

            scope.checkIfEnter = function($event, id)
            {
              if($event.which==13)
              {
                try{
                  console.log(scope.selectedTitle.title);                  
                if((scope.selectedTitle.title != "-- Gefundene Probleme --"))
                  {
                   localStorage.setItem("problemId",id);
                   open("problem.html","_self");
                   $event.preventDefault();
                  }
                }
                catch(err){
                  return; 
                }
              }
            }

            scope.checkKeys = function($event,id, selectedTitle)
            {
                scope.checkIfEnter($event,id);
                scope.checkIfKeyUp($event, selectedTitle);
                
            }



            scope.moveByKey = function(ev, id) 
            {
              if((ev.which==38) || (ev.which==40))
                {
                    for(var i=0; i<=ev.target.length; i++)
                    {
                           if(ev.target.options[ev.target.selectedIndex]!= undefined)
                           {
                               ev.target.options[ev.target.selectedIndex].setAttribute("id","prob"+id);
                               document.getElementById('problemsSearch').setAttribute("aria-activedescendant", "prob"+id);
                           }  
                    }
                }
            }
            
            scope.searchForPara= function(searchpara)
            {
              localStorage.setItem("searchpara", searchpara);
              localStorage.setItem("searchFromSearchfield", "true");
              if(scope.results.toString()=="")
              {
                localStorage.setItem("searchFromSearchfield", "false");
                open("createproblems.html","_self"); 
              }
               else
                {
                  open("search.html","_self");
                }
            }
        
        }
    }

    return directiveDefinitionObject;
});



/*Diese Direktive wird genutzt um ein Input-Feld zu erzeugen, in dem nach vorgegebenen Tags gesucht werden kann.
Diese Tags können dann mittels einem Doppelklick oder durch Enter aus gewählt werden.
*/



myApp.directive('autoCompleteTag', function factory($timeout) {
    var directiveDefinitionObject = {
        restrict: 'A',
        template: '<div>'+
                  '<label class="invisibleLabel" for="tagsearch">Nach Tags suchen</label>'+
                  '<a id="tagsuche"></a><input type="text"  ng-keydown="checkIfKeyDown($event)" id="tagsearch" aria-autocomplete="list" aria-owns="tagsField" role="combobox" aria-expanded="false" placeholder="Hier können Sie nach verfügbaren Tags suchen." ng-change="startGetTagsHide(problem.tagsearch)" ng-model="problem.tagsearch" ng-blur="delayHide()" required/>'+
                  '<label class="invisibleLabel" for="tagsField">Hier werden während der Eingabe mögliche Tags angezeigt</label><select aria-live="polite" role="listbox" ng-hide="see2" ng-blur="delayHide()" ng-focus="setFocus()" ng-click="setFocus()" id="tagsField" ng-keyup="moveByKey($event, selectedTag.id)" ng-keydown="checkKeys($event)" size="5" ng-model="selectedTag" ng-options="t.name for t in tags | filter: problem.tagsearch" ng-dblclick="addChoosenTag(selectedTag.name)" >' +
                  '<option value="">-- Auswählbare Tags --</option>'+'</select>' + '</p>'+
                  '</div>',
                   
        replace: 'true',

        scope: true,
        link: function (scope, element, attrs)
        { 
            scope.selectIsFocused = false; 
            scope.see2 = true;
            scope.startGetTagsHide = function(tagsearch)
            {
                console.log("startGetTagsHide() ")
                scope.selectIsFocused = true;
                scope.getTags();
                scope.hideSelectField(tagsearch);
            }
           
            scope.hide = function()
            {
              if (scope.selectIsFocused = false)
              {
                scope.see2 = true;
                document.getElementById('tagsearch').setAttribute('aria-expanded', 'false');
              }
            }

            scope.delayHide = function()
            {
                 scope.selectIsFocused = false;
                 $timeout(function()
                 {
                    if (scope.selectIsFocused == true)
                      scope.see2 = false;
                    else
                      {
                        scope.see2 = true;
                        console.log("hier...");
                        document.getElementById('tagsearch').setAttribute('aria-expanded', 'false');
                      }
                 },110);

            }

            
             scope.setFocus = function()
             {
                scope.selectIsFocused = true;
             }

             scope.getValue = function(ev)
             {
               if (ev.which==13 && (scope.selectedTag != null))
                  {
                      ev.preventDefault();
                      console.log(scope.selectedTag.name);
                      scope.addChoosenTag(scope.selectedTag.name);
                  }
             }  

        

            scope.checkIfKeyDown = function($event)
            {
                 if ($event.which==40)
                 {     
                    $timeout(function()
                    {   document.getElementById('tagsField').options[0].selected = true;
                        document.getElementById('tagsField').focus();
                        scope.selectIsFocused = true;
                        $event.preventDefault();
                    });
                 }
            }


            scope.checkIfKeyUp = function($event)
            {
                 if ($event.which==38)
                 {    
                    if(document.getElementById('tagsField').options[0].selected) 
                     {
                                         $timeout(function()
                                          {
                                            document.getElementById("tagsearch").focus();
                                            document.getElementById("tagsearch").select();
                                            scope.selectIsFocused = true;
                                            $event.preventDefault();
                                          });
                     }
                  }
            }


            scope.checkKeys = function($event)
            {
                scope.getValue($event);
                scope.checkIfKeyUp($event);
            }


    


             scope.hideSelectField = function(tagsearch)
             {
                    console.log("hideSelectField() arbeitet");
                    if (scope.selectIsFocused==true)
                    {
                      scope.see2 = false;
                      document.getElementById('tagsearch').setAttribute('aria-expanded', 'true');
                    }
                    else
                    {
                      scope.see2 = true;
                      document.getElementById('tagsearch').setAttribute('aria-expanded', 'false');
                    }
                    if (tagsearch == "")
                      {
                        scope.see2 = true;
                        scope.selectIsFocused = false;
                        document.getElementById('tagsearch').setAttribute('aria-expanded', 'false');
                      }
             }


/*moveByTag() ermittelt den aktuell selektieren Tag und weisst diesem eine id zu, über diese id, wird die aria-activedescendant Eigenschaft zugewiesen und somit der aktuelle tag an die assestive Technologie übermittelt*/

            scope.moveByKey = function(ev, id) 
            {
                if((ev.which==38) || (ev.which==40))
                {
                       for(var i=0; i<=ev.target.length; i++)
                       {
                           if(ev.target.options[ev.target.selectedIndex]!= undefined)
                           {
                               ev.target.options[ev.target.selectedIndex].setAttribute("id","tag"+id);
                               document.getElementById('tagsearch').setAttribute("aria-activedescendant", "tag"+id);
                           }  
                       }
                }
            }  
        }

    }
    return directiveDefinitionObject;
});



myApp.controller('logController', function($scope, $http, $log) 
{
 
   $scope.loggedIn = function()
    {
        $scope.loadStyle();
        console.log("loggedIn() wird ausgeführt.");
            if (localStorage.getItem("loggedInUserBase64") != null)
            {
                $scope.userLoggedIn = localStorage.getItem("loggedInUserName");
                return true;
            }
            else
            { 
                return false; 
            }   

    }

    $scope.logout = function()
    {
        console.log("ausgeloggt");
        localStorage.removeItem("loggedInUser");
        localStorage.removeItem("loggedInUserName");
        localStorage.removeItem("loggedInUserBase64");
        alert("Sie sind nun abgemeldet");
        window.open("index.html","_self"); 
    }

}).directive('login', function factory() 
{
    var directiveDefinitionObject = 
    {
        restrict: 'A',
        template: '<li>'+
                  '<a href="login.html" ng-show="!loggedIn()">Benutzer Anmelden</a>'+
                  '<a href="" ng-hide="!loggedIn()" ng-click="logout()">{{userLoggedIn}} abmelden</a>'+
                  '</li>',
        replace: 'true',
 
        scope: true,
        link: function (scope, element, attrs)
        {    

        }
    }
    return directiveDefinitionObject;
});


myApp.controller('suggestController', function($scope, $http, $log) {
      $scope.search = function(title)
    {
      console.log("search() arbeitet.");
        $http({
        url: '/rest/search/problems/',
        method: "POST",
        data: $scope.createJSON_title(title),
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
             }).success(function (data, status, headers, config) {
                      document.getElementById('problemsSelectField').options[0].text="-- Ähnliche gefundene Probleme --";
                      $scope.results = data;
                      console.log($scope.results);
                       }).error(function (data, status, headers, config) {
            console.log("Suche nicht erfolgreich!");
                               });


    }

}).directive('suggestProblem', function factory($timeout) 
{
    var directiveDefinitionObject = 
    {
        restrict: 'A',
        template:  '<div>'+
                   '<label class="invisibleLabel" for="problemname">Name des Problems</label>'+
                   '<input role="combobox"  aria-autocomplete="list" aria-expanded="false"  aria-owns="problemsSelectField" type="text" list="problemsSelectField" ng-keyup="setProblemName(problem.name)" ng-blur="delayHide()" ng-keydown="checkIfKeyDown($event)"  id="probname" ng-model="problem.name" ng-change="startHideSearch(problem.name)" type="search" placeholder="Bitte geben Sie einen Titel für die Frage ein" required/>'+
                   '<label class="invisibleLabel" for="problemsField">Name der Frage</label><select aria-live="polite" role="listbox"  ng-hide="see1" ng-focus="setFocus()" ng-mousedown="setFocus()" ng-blur="delayHide()" id="problemsSelectField" ng-keyup="moveByKey($event, selectedTitle.id)" ng-keypress="checkKeys($event, selectedTitle.id, selectedTitle.title)" size="5" ng-model="selectedTitle" ng-options="r.title for r in results.results| filter: problem.name" ng-click="loadProblem(selectedTitle.id, selectedTitle.title)">'+
                   '<option value="">-- Ähnliche gefundene Fragen --</option></select></p>'+
                   '</div>',
                   
        replace: 'true',

        scope: true,
        link: function (scope, element, attrs){
          scope.see1 = true;
          scope.selectIsFocused = false;
         

         scope.hideSelectField1 = function()
                 {  
                        scope.see1 = false;
                        document.getElementById('probname').setAttribute('aria-expanded', 'true');
                        if (scope.results == "")
                        {
                            scope.see1 = true;
                            document.getElementById('probname').setAttribute('aria-expanded', 'false');
                        }
               }  
              
               scope.hide = function(){
                scope.see1 = true;
               }

            scope.delayHide = function()
            {
                 scope.selectIsFocused = false;
                 $timeout(function()
                 {
                    if (scope.selectIsFocused == true)
                      scope.see1 = false;
                    else
                      {
                        scope.see1 = true;
                        document.getElementById('tagsearch').setAttribute('aria-expanded', 'false');
                      }
                 },110);

            }

            
                scope.setFocus = function(){
                scope.selectIsFocused = true;
               }



                scope.startHideSearch = function(searchpara)
                {
                    //localStorage.setItem("problem.name", searchpara);
                    scope.hideSelectField1();
                    if (searchpara.length >=2)
                    {
                      scope.search(searchpara);
                                        if (document.getElementById('probname'))
                                        {
                                          {
                                            console.log("aria-expanded = true");
                                            document.getElementById('probname').setAttribute('aria-expanded', 'true');
                                          }
                                        }
                                        if (searchpara == "")
                                            {
                                              scope.see1 = true;
                                            }
                                      
                    
                    }
                }

                scope.checkIfKeyDown = function(ev){
                 if (ev.which==40)
                 {     
                     /*getElementById('tagsField').focus(); */  
                     $timeout(function ()
                     {
                     document.getElementById('problemsSelectField').options[0].selected = true;
                     document.getElementById('problemsSelectField').focus();
                     scope.selectIsFocused = true;
                     ev.preventDefault();
                     });
                  }
                }


            scope.checkIfKeyUp = function($event)
            {
                console.log($event);
                 if ($event.which==38)
                 {   
                    if(document.getElementById('problemsSelectField').options[0].selected) 
                     {
                       $timeout(function (){
                       document.getElementById("probname").focus();
                       document.getElementById("probname").select();
                       scope.selectIsFocused = true;
                       //$event.preventDefault();
                     });
                     }
                  }
            }

            scope.loadProblem = function (id, title) 
            {   
              if(title != undefined) 
                {
                console.log("ProblemId = "+id);
                localStorage.setItem("problemId",id);
                open("problem.html","_self");  
                } 
            }


            scope.checkIfEnter = function($event, id, title)
            {
              console.log("arbeitet");
              if($event.which==13)
              {
                console.log(title);
                if (title != undefined)
                {
                  scope.loadProblem(id, title);
                }
            }
          }

            scope.checkKeys = function($event,id, title)
            {
                console.log("arbeitet");
                scope.checkIfEnter($event, id, title);
                scope.checkIfKeyUp($event);
                scope.title = scope.problem.name;
            }

            scope.moveByKey = function(ev, id) 
            {
                if((ev.which==38) || (ev.which==40))
                {
                       for(var i=0; i<=ev.target.length; i++)
                       {
                           if(ev.target.options[ev.target.selectedIndex]!= undefined)
                           {
                               ev.target.options[ev.target.selectedIndex].setAttribute("id","tag"+id);
                               document.getElementById('probname').setAttribute("aria-activedescendant", "tag"+id);
                           }  
                       }
                }
            }  

        }
        }
       return directiveDefinitionObject;
});

myApp.controller('commentController', function($scope, $http, $log) {})
.directive('commentQuestion', function factory() 
{
    var directiveDefinitionObject = 
    {
        restrict: 'A',
        template:  '<form class="ym-form">'+
                    '<div class="ym-fbox">'+
                    '<label class="invisibleLabel" for="comment">Kommentar:</label>'+
                    '<textarea id="comment" ng-model="comment" rows="5" placeholder="Bitte tragen Sie hier Ihr Kommentar ein."></textarea>'+
                    '<button id="commentButton" ng-click="commentProblem(comment, problem.id)" class="ym-button ym-primary">Kommentieren</button>'+
                    '</div>'+
                    '</form>',
                   
        replace: 'true',

        scope: true,
        link: function (scope, element, attrs){
        }

        }
       return directiveDefinitionObject;
});

myApp.controller('solutionController', function($scope, $http, $log) {

}).directive('answerQuestion', function factory() 
{
    var directiveDefinitionObject = 
    {
        restrict: 'A',
        template:   '<form class="ym-form">'+
                                    '<div class="ym-fbox">'+
                                        '<label class="invisibleLabel" for="solution">Lösung:</label>'+
                                        '<textarea id="solution" ng-model="solution" rows="5" placeholder="Bitte tragen Sie hier Ihre Lösung ein."></textarea>'+
                                        '<button name="solutionButton" ng-click="solutionProblem(solution)" class="ym-button ym-primary">Lösung anbieten</button>'+
                                    '</div>'+
                                '</form>',
                   
        replace: 'true',

        scope: true,
        link: function (scope, element, attrs){
        }
        }
       return directiveDefinitionObject;
});


myApp.controller('indexSearchController', function($scope, $http, $log) {
      $scope.search = function(title)
    {
      console.log("search() arbeitet.");
        $http({
        url: '/rest/search/problems/',
        method: "POST",
        data: $scope.createJSON_title(title),
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
             }).success(function (data, status, headers, config) {
                      if(data.total_entries == 0){
                      data.results.title = "Neue Frage erstellen";
                      document.getElementById('problemsField1').options[0].text="Keine Suchergebnisse. Neue Frage erstellen?";   
                      }
                      else
                      document.getElementById('problemsField1').options[0].text="-- Ähnliche gefundene Fragen --";
                      $scope.results = data;
                      console.log($scope.results);
                       }).error(function (data, status, headers, config) {
            console.log("Suche nicht erfolgreich!");
                               });


    }

}).directive('indexSearchSuggestProblem', function factory($timeout) 
{
    var directiveDefinitionObject = 
    {
        restrict: 'A',
        template:  '<div>'+
                   '<form class="ym-form">'+
                   '<div class="ym-fbox"><label class="invisibleLabel" for="problemname1">Nach einer Frage suchen</label>'+
                   '<input role="combobox"  aria-autocomplete="list" aria-expanded="false"  aria-owns="problemsSelectField" type="text" list="problemsField1" ng-keyup="setProblemName(problem.name)" ng-keydown="checkIfKeyDown($event)"  id="problemname1" ng-model="problem.name" ng-blur="delayHide()" ng-change="startHideSearch(problem.name)" type="search" placeholder="Geben Sie den Namen der Frage ein"/></div>'+
                   '<label class="invisibleLabel" for="problemsField1">Name der Frage</label><select aria-live="polite" role="listbox" ng-hide="see1" id="problemsField1" ng-focus="setFocus()" ng-onmousedown="setFocus()" ng-blur="delayHide()" ng-keydown="checkKeys($event, selectedTitle.id, selectedTitle.title)" size="10" ng-keyup="moveByKey($event, selectedTitle.id)" ng-model="selectedTitle" ng-options="r.title for r in results.results| filter: problem.name" ng-click="loadProblem(selectedTitle.id, selectedTitle.title, $event)">'+
                   '<option value="">-- Ähnliche gefundene Fragen --</option></select></p><div class="ym-fbox"><button id="searchbutton1" class="ym-button ym-primary" ng-click="searchForPara(problem.name)">Suchen</button></div></div>'+
                   '</form></div>',
                   
        replace: 'true',

        scope: true,
        link: function (scope, element, attrs){
          scope.see1 = true;
          scope.selectIsFocused = false;
                scope.hideSelectField1 = function()
                 {  
                        scope.see1 = false;
                        if (scope.results == "")
                            {
                              document.getElementById('problemname1').setAttribute('aria-expanded', 'false');
                              scope.see1 = true;
                              scope.selectIsFocused = false;
                            }
                }

                scope.hide = function(){
                  scope.see1 = true;
                }  

                scope.delayHide = function(){
                  scope.selectIsFocused = false;
                  $timeout(function()
                  {
                    if (scope.selectIsFocused == false){
                      scope.see1 = true;
                      document.getElementById('problemname1').setAttribute('aria-expanded', 'false');
                    }
                      
                  },110);
                  console.log(scope.selectIsFocused);
                }

            
                scope.setFocus = function(){
                scope.selectIsFocused = true;
               }



                scope.startHideSearch = function(searchpara)
                {
                    //localStorage.setItem("problem.name", searchpara);
                    scope.hideSelectField1();
                    scope.search(searchpara);
                                        if (document.getElementById('problemname1'))
                                        {
                                          {
                                            console.log("aria-expanded = true");
                                            document.getElementById('problemname1').setAttribute('aria-expanded', 'true');
                                          }
                                        }
                                        if (searchpara == "")
                                            {
                                              scope.see1 = true;
                                              document.getElementById('problemname1').setAttribute('aria-expanded', 'false');
                                            }
                }

                scope.checkIfKeyDown = function(ev){
                    console.log("Taste nach unten gedrückt."); 
                 if (ev.which==40)
                 {     
                  $timeout(function(){document.getElementById('problemsField1').options[0].selected = true;
                     document.getElementById('problemsField1').focus();
                     scope.selectIsFocused = true;
                     ev.preventDefault();
                   });
                  }
                }


            scope.checkIfKeyUp = function($event)
            {
                console.log($event);
                 if ($event.which==38)
                 {   
                    if(document.getElementById('problemsField1').options[0].selected) 
                     {
                       $timeout(function()
                       {
                        document.getElementById("problemname1").focus();
                        document.getElementById("problemname1").select();
                        scope.selectIsFocused = true;
                       $event.preventDefault();
                      });
                     }
              }
            }

            scope.loadProblem = function (id, title, ev)
            {
              console.log(ev.target.label);
              if ("Keine Suchergebnisse. Neue Frage erstellen?" == ev.target.label)
                {
                    open("createproblems.html","_self");

                }
                
                    
                if (title != undefined)
                {
                                  console.log("ProblemId = "+id);
                                  localStorage.setItem("problemId",id);
                                  open("problem.html","_self"); 
                }
            }


            scope.checkIfEnter = function($event, id, title)
            {
              if($event.which==13)
              {
                if ("Keine Suchergebnisse. Neue Frage erstellen?" == $event.target.options[$event.target.selectedIndex].text)
                {
                    open("createproblems.html","_self");

                }
                console.log("Keine Suchergebnisse. Neue Frage erstellen?" == $event.target.options[$event.target.selectedIndex].text);
                    
                if (title != undefined)
                {
                                  console.log("ProblemId = "+id);
                                  localStorage.setItem("problemId",id);
                                  open("problem.html","_self"); 
                }
                  
             }

            }

            scope.checkKeys = function($event,id, title)
            {
                scope.checkIfKeyUp($event);
                scope.checkIfEnter($event,id, title);
                scope.title = scope.problem.name;
            }

            scope.moveByKey = function(ev, id) 
            {
                if((ev.which==38) || (ev.which==40))
                {
                       for(var i=0; i<=ev.target.length; i++)
                       {
                           if(ev.target.options[ev.target.selectedIndex]!= undefined)
                           {
                               ev.target.options[ev.target.selectedIndex].setAttribute("id","tag"+id);
                               document.getElementById('problemname1').setAttribute("aria-activedescendant", "tag"+id);
                           }  
                       }
                }
            } 

      

      scope.searchForPara= function(searchpara){
              localStorage.setItem("searchpara", searchpara);
              localStorage.setItem("searchFromSearchfield", "true");
              open("search.html","_self");
            }
    }
    }    
       return directiveDefinitionObject; 

});

