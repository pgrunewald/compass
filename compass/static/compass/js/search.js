var json = '{"glossary":{"title":"example glossary","GlossDiv":{"title":"S","GlossList":{"GlossEntry":{"ID":"SGML","SortAs":"SGML","GlossTerm":"Standard Generalized Markup Language","Acronym":"SGML","Abbrev":"ISO 8879:1986","GlossDef":{"para":"A meta-markup language, used to create markup languages such as DocBook.","ID":"44","str":"SGML","GlossSeeAlso":["GML","XML"]},"GlossSee":"markup"}}}}}';
 
var tags =[
    {
        "Assistive Technologien": [
            {
                "id": "Screenreader",
                "tags": [
                    {
                        "tagname": "Screenreader"
                    },
                    {
                        "tagname": "JAWS"
                    },
                    {
                        "tagname": "JAWS 9"
                    },
                    {
                        "tagname": "JAWS 10"
                    },
                    {
                        "tagname": "JAWS 11"
                    },
                    {
                        "tagname": "COBRA"
                    },
                    {
                        "tagname": "NVDA"
                    }
                ]
            },
            {
                "id": "Braille",
                "name": [
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6"
                ]
            }
        ]
    }
]


var js = JSON.parse(json);



function getObjects(obj, key, val) {

    var objects = [];
    for (var i in obj) {
                console.log(i);
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));    
        } else 
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
            objects.push(obj);
        } else if (obj[i] == val && key == ''){
            //only add if the object is not already in the array
            if (objects.lastIndexOf(obj) == -1){
                objects.push(obj);
            }
        }
    }
    return objects;
}
 
//return an array of values that match on a certain key
function getValues(obj, key) {

    var objects = [];
    for (var i in obj) {
        
        if (!obj.hasOwnProperty(i)) continue;
            console.log(i);
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getValues(obj[i], key));
        } else if (i == key) {
            console.log("gefunden");
            objects.push(obj[i]);
        }
    }
    return objects;
}

 
//return an array of keys that match on a certain value
function getKeys(obj, val) {

    var objects = [];
    for (var i in obj) {
       /* if (!obj.hasOwnProperty(i)) continue;*/
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getKeys(obj[i], val));
        } else if (obj[i] == val) {
            console.log('Object: '+obj[i]+' Value: '+val);
            objects.push(i);
        }
    }
    console.log(objects);
    return objects;
}


 function search() 
{/*
    console.log(getObjects(js,'','XML'));*/
    console.log(getObjects(tags,'id','Screenreader'));
    console.log(getObjects(tags,'id','Braille'));
  }
 
