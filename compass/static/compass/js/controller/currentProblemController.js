function currentProblemController($scope, $http) {
 $scope.finalSolution = null;
 $scope.tagsPerPage = 20;
 $scope.pageCount = 0;
 $scope.showResults = [];
 $scope.allTags = [];
 $scope.show = false;
 $scope.topics = [];

    $scope.getFinalSolution = function(problemid){
        console.log("getCurrentProblem() arbeitet");
        $http({
        url: '/rest/problems/'+problemid,
        method: "POST",
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
             }).success(function (data, status, headers, config) {
                      $scope.ratings = data;
                      console.log($scope.ratings);
                       }).error(function (data, status, headers, config) {
            console.log("Suche nicht erfolgreich!");
        });
        }

    $scope.getAllTags = function (){
      $scope.JSONTag = new Object();
      $scope.JSONTag.with_count = true;
      $scope.JSONTag.PER_PAGE = $scope.tagsPerPage;
      $scope.JSONTag.PAGE = $scope.pageCount;
      //$scope.JSONTag.ALL = true;
         $http({
            url:'/rest/search/tags',
            method: "POST",
            headers:{'Authorization': localStorage.getItem("loggedInUserBase64")},
            data: $scope.JSONTag,
         }).success(function (data, status, headers, config) {
          console.log(data);
          if(data.length<= $scope.tagsPerPage)
            $scope.show = true;
            $scope.allTags[$scope.pageCount] = data;

             if (data.more_results == true)
             {
                $scope.pageCount++;
                $scope.getAllTags();
                $scope.show = false;
             }
             console.log("showResults");
             console.log($scope.showResults);
             $scope.showResults = $scope.allTags[0];

         }).error(function (data, status, headers, config) {
         });
     }

     $scope.loadSearchPageCount = function(pageCount){
        $scope.showResults = $scope.allTags[pageCount];
     }

     $scope.getTopics = function(){
       $http({
            url:'/rest/topics',
            method: "GET",
            headers:{'Authorization': localStorage.getItem("loggedInUserBase64")},
         }).success(function (data, status, headers, config) {
          $scope.topics = data;
          console.log($scope.topics);
         }).error(function (data, status, headers, config) {
         }); 
     }


$scope.searchTags = function(tag){
  $scope.JSON = new Object();
  $scope.JSON.name = tag;
       $http({
            url:'/rest/search/tags',
            method: "POST",
            headers:{'Authorization': localStorage.getItem("loggedInUserBase64")},
            data: $scope.JSON,
         }).success(function (data, status, headers, config) {
          $scope.showResults = data;
          console.log(data.results);
          $scope.show = true;
         }).error(function (data, status, headers, config) {
         });
}

 $scope.loadByTopic = function(topic){
    localStorage.setItem("searchTopic", topic);
    localStorage.setItem("searchedByTopic","true");
    open("search.html","_self");
 }
   
}