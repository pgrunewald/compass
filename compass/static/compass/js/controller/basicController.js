/*Dieser Controller bildet die Basis-Funktionen ab, die immer gebraucht werden.*/




function basicController($scope, $http){  // der BasicController muss vor allen anderen geladen werden
 $scope.probs = [];
 $scope.tagProbleme = [];
 $scope.selectedTag = [];
 $scope.allTags = [];
 $scope.userLoggedIn = false;
 $scope.noAsk = [];
 $scope.isOwner = false;

/*    Diese Funktion stellt die Base64-Kodierung bereit, die für die Anmeldung beim Server
 benötigt wird. */

	$scope.base64 = function() {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
        'QRSTUVWXYZabcdef' +
        'ghijklmnopqrstuv' +
        'wxyz0123456789+/' +
        '=';
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
 
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
 
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
 
                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
            return output;
        },
 
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
 
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
 
                output = output + String.fromCharCode(chr1);
 
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
 
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
 
            } while (i < input.length);

            return output;

        }
    };
}



$scope.Styles = "Standard";
$scope.switchStyle = function(s){
  if (!document.getElementsByTagName) return;
  var el = document.getElementsByTagName("link");
  for (var i = 0; i < el.length; i++ ) {
    if (el[i].getAttribute("rel").indexOf("style") != -1 && el[i].getAttribute("title")) {
      el[i].disabled = true;
      if (el[i].getAttribute("title") == s) el[i].disabled = false;
    }
  }
}

$scope.loadStyle = function() {
  if (localStorage.getItem("style") != null){
  var c = localStorage.getItem("style");
  if (c && c != $scope.Styles) {
    $scope.switchStyle(c);
    $scope.Styles = c;
  }
}
}

$scope.setStyle = function(s){
  if (s != $scope.Styles) {
    $scope.switchStyle(s);
    $scope.Styles = s;
    localStorage.setItem("style",s);
  }
}


$scope.createJSON = function(title, tags){
        $scope.JSON = new Object();
        $scope.JSON.PER_PAGE = 5;
        $scope.JSON.PAGE = 0;
        $scope.JSON.ALL = false;
        $scope.JSON.title = title;
        $scope.JSON.tags = tags;
        return $scope.JSON;
        }


$scope.createJSON_title = function(title){
        $scope.JSON = new Object();
        $scope.JSON.title = title;
        return $scope.JSON;
        }

$scope.createJSON_title_description = function(title, description, choosenTags)
    {
        $scope.JSON = new Object();
        $scope.JSON.title = title;
        $scope.JSON.description = description;
        $scope.JSON.tags = choosenTags;
        return $scope.JSON;

    }

 $scope.createJSON_title_tags = function(title){
        $scope.JSON = new Object();
        $scope.JSON.title = title;
        $scope.JSON.tags = $scope.choosenTags;
        return $scope.JSON;
}   

 $scope.createJSON_newUser = function(newuser, pw, pw_repeat, email, email_repeat){
        $scope.JSON = new Object();
        $scope.JSON.username = newuser;
        $scope.JSON.password = pw;
        $scope.JSON.password_repeated = pw_repeat;
        $scope.JSON.email = email;
        $scope.JSON.email_repeated = email_repeat;  
        return $scope.JSON;
} 
 $scope.loadProblem = function(id){
                localStorage.setItem("problemId",id);
                open("problem.html","_self");
                } 

$scope.loadProblemByTag = function(tag){
    localStorage.setItem("searchTag", tag);
    localStorage.setItem("searchedByTag","true");
    open("search.html","_self");
}

 $scope.getCurrentProblems = function(count){
        $scope.JSONCount = new Object();
        $scope.JSONCount.PER_PAGE = count;
        $scope.JSONCount.latest = true;
        $http({
        url: '/rest/search/problems/',
        method: "POST",
        data: $scope.JSONCount,
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
             }).success(function (data, status, headers, config) {
                      $scope.probs = data;
                      console.log($scope.probs);
                       }).error(function (data, status, headers, config) {
        });


}

 $scope.getNoAskQuestion = function(){
        $scope.JSONAsk = new Object();
        $scope.JSONAsk.PER_PAGE = 5;
        $scope.JSONAsk.latest = true;
        $scope.JSONAsk.solved_for_problem_creator = false;
        $http({
        url: '/rest/search/problems/',
        method: "POST",
        data: $scope.JSONAsk,
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
             }).success(function (data, status, headers, config) {
                $scope.noAsk = data;
                       }).error(function (data, status, headers, config) {
        });


}

 $scope.searchByTag = function(){
        $scope.tags = [];
        title = "";
        $scope.tags.push(localStorage.getItem("searchTag"));
        $http({
        url: '/rest/search/problems/',
        method: "POST",
        data: $scope.createJSON(title, $scope.tags),
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
             }).success(function (data, status, headers, config) {
                      $scope.searchquestions = data;
                      console.log($scope.tagProbleme);
                       }).error(function (data, status, headers, config) {
                            });
}

$scope.saveSearchTag = function (tag){
        localStorage.setItem("searchTag",tag);
} 

$scope.checkUser = function(){
    if(localStorage.getItem("loggedInUserName") != null)
        {
            $scope.userLoggedIn = true;
        }
    else
        {
            $scope.userLoggedIn = false;
        }

    console.log($scope.userLoggedIn);
}

$scope.startFunctions = function(){
    $scope.checkUser();
    $scope.getCurrentProblems(8);
    $scope.getNoAskQuestion();
}

$scope.formatDate = function(date){
    if(date == undefined)
        return null;
    $scope.newDate  = new Date(date);
    return $scope.newDate.toLocaleString();
}

function setFocus(){
  setTimeout(setFocus, 1000);
  document.getElementById("problemname").focus();
}


}











