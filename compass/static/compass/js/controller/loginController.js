

function createLoginController($scope, $http, $route) {

$scope.hide=true;
$scope.loginSuccess = false;
$scope.createLoginSuccess = false;
$scope.result = "";

function setFocus(){
  document.getElementById("new_username").focus();
}
   $scope.show = function(){
    if ($scope.hide==true){
       
    console.log( $('#new_username'));
    $scope.hide= false;  
    setTimeout(setFocus, 100);
    }
       else
    {
    $scope.hide= true;
    }
  }

   
$scope.login = function(username, password){
    $http.get("/rest/users/"+username, {headers:{Authorization:'Basic '+$scope.base64().encode(username+':'+password)}})
    .success(function(data, status, headers, response) {
    $scope.user = data;
    $scope.valid = "Angemeldet als: "+username;
    $scope.result = "Sie sind nun angemeldet als "+username;
    alert("Sie sind nun als "+username+" angemeldet.");
    $scope.loginSuccess = true;
    localStorage.setItem("loggedInUserBase64", 'Basic '+$scope.base64().encode(username+':'+password));
    localStorage.setItem("loggedInUserName", username);
    window.open("myproblems.html","_self");
    console.log(localStorage.getItem("loggedInUserBase64"));
    })
    .error(function(data, status) {
    if (status == 403){
        $scope.valid = "Benutzerdaten sind falsch";
        $scope.result = "Die Benutzerdaten wurden falsch eingegeben, bitte versuchen Sie es erneut.";
        $scope.loginSuccess = false;
}
    if (status == 404){
        $scope.valid = "Nicht gefunden";
        $scope.result = "Die Benutzerdaten wurden falsch eingegeben, bitte versuchen Sie es erneut.";
        $scope.loginSuccess = false;
    }
     else
    console.log("fehlgeschlagen");
});
}




    $scope.createUser = function(newuser,pw, pw_repeat,email,email_repeat){
        console.log("createUser() arbeitet.");
      $http({
        url: '/rest/users',
        method: "POST",
        data: $scope.createJSON_newUser(newuser,pw,pw_repeat,email,email_repeat),
        headers:{}
      }).success(function (data, status, headers, config) {
            $scope.result = "Der Benutzer '"+newuser+"' wurde erfolgreich erstellt. Bitte melden Sie sich nun an.";
            $scope.createLoginSuccess = true;
          //  window.open("loginresult.html");
        }).error(function (data, status, headers, config) {
            if (status == '400'){
                $scope.result = "Sie haben Fehler bei der Eingabe Ihrer Daten gemacht, bitte versuchen Sie es erneut.";
            }
            console.log(data + headers);
            if (status == '403'){
                $scope.result = "Der Nutzer '"+newuser+"' existiert bereits, bitte nehmen Sie einen anderen Namen.";
                $scope.createLoginSuccess = false;
              //  window.open("loginresult.html"_self);
                }
        });


}

}