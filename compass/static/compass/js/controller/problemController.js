var myModule = angular.module('myModule', [], function($compileProvider) {

    $compileProvider.urlSanitizationWhitelist(/^\s*(https?|ftp|mailto|file):/);

});

function problemController($scope, $http,  $timeout) {
    $scope.problem = []; 
    $scope.hiddenComment = true;
    $scope.problemId = "";
    $scope.result = "";
    $scope.rating = new Object();
    $scope.myRating = new Object();
    $scope.similiarProblems = [];
    $scope.active = false;
    $scope.show = new Object();
    $scope.choosenSolution = new Object();
    $scope.liveEditHide = true;
    $scope.liveEditValue = "";
    $scope.showEditArea = true;
    $scope.modifiedtext = "";
    $scope.finalSolution = [];
    $scope.isSolution = false;
    


$scope.getProblem = function() {
        $http({
            url: "/rest/problems/" + localStorage.getItem("problemId"),
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            method: "GET",
        }).success(function(data, status, headers, response) {
            $scope.problem = data;
            console.log($scope.problem);
            $scope.checkForOwner();
        });
    }

$scope.getRatings = function(problemId, solutionId) {
        $http({
            url: "/rest/problems/"+problemId+"/solutions/"+solutionId+"/feedback",
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            method: "GET",
        }).success(function(data, status, headers, response) {
            $scope.rating[solutionId] = data;
            console.log("Rating = "+$scope.rating[solutionId].difficulty);
        });
    }

$scope.checkForOwner = function(                                                                                                                            )
{
    console.log("username");
    console.log($scope.problem.user.username);
            if($scope.problem.user.username == localStorage.getItem("loggedInUserName"))
            {
            $scope.isOwner = true;
            }
            else
            $scope.isOwner = false;
}

    $scope.getSolution = function(probId, solId){
        console.log("getSolution arbeitet");
        if (localStorage.getItem("loggedInUserBase64") == null) return;
       $http({
            url:  "/rest/problems/"+probId+"/solutions/"+solId,
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            method: "GET",
        }).success(function(data, status, headers, response) {
            console.log(data);
            $scope.choosenSolution = data;
            console.log("aufegrufen");
            $scope.showSol[solId] = true;
            console.log($scope.show[solId]);
        });
    }


    $scope.showSol = function(solId, probId){
    angular.forEach($scope.show, function(value, key){
        console.log(value);
        $scope.show[key] = true;
        document.getElementById("sol"+key).setAttribute("aria-expanded","false");
    })
    $scope.liveEditHide=true;


        if ($scope.show[solId+"-"+probId] == false)
            {
                $scope.show[solId+"-"+probId] = true;
                document.getElementById("sol"+solId+'-'+probId).setAttribute("aria-expanded","false");
            }
        else
            {
                $scope.show[solId+"-"+probId] = false;
                document.getElementById("sol"+solId+'-'+probId).setAttribute("aria-expanded","true");
            }
    }

        $scope.setTrue = function(solId, probId){
            $scope.show[solId+"-"+probId] = true;
        }

     

     $scope.liveEdit = function(value, solId, probId, $event){
        
        if($scope.liveEditHide == true)
            {
                $scope.liveEditHide = false;
                document.getElementById("a"+solId+'-'+probId).setAttribute("aria-expanded","true");
                $event.preventDefault();
                setTimeout(function(){
                    document.getElementById("liveEdit"+solId+'-'+probId).focus();
                    document.getElementById("liveEdit"+solId+'-'+probId).value = document.getElementById("liveEdit"+solId+'-'+probId).value+' ';},10);
            }
        else
            {
                $scope.liveEditHide = true;
                console.log(document.getElementById("a"+solId+'-'+probId));
                document.getElementById("a"+solId+'-'+probId).setAttribute("aria-expanded","false");
            }
        $scope.liveEditValue = value;
     

     }   


$scope.setFinalSolution = function(problemId, solutionId) {
        JSONFinal = new Object();
        JSONFinal.chosen_solution_id = solutionId;
        $http({
            url: "/rest/problems/"+problemId,
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            method: "PUT",
            data: JSONFinal,
        }).success(function(data, status, headers, response) {
            window.location.reload();
            console.log("Finale Lösung wurde gesetzt!");
        });
    }

$scope.myRating = function(problemId, solutionId) {
    if (localStorage.getItem("loggedInUserBase64") == null) return;
        $http({
            url: "/rest/problems/"+problemId+"/solutions/"+solutionId+"/feedback/"+localStorage.getItem("loggedInUserName"),
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            method: "GET",
        }).success(function(data, status, headers, response) {
            $scope.myRating[solutionId] = data;
        });
    }


$scope.vote = function(problemId, solutionId, voteType, answer) {
    if (localStorage.getItem("loggedInUserBase64") == null) return;
      $scope.ratingUpdate = new Object();
      $scope.JSONVote = new Object();
      $scope.JSONVote[voteType] = answer;
      console.log($scope.JSONVote);
        $http({
            url: "/rest/problems/"+problemId+"/solutions/"+solutionId+"/feedback/"+localStorage.getItem("loggedInUserName"),
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            method: "PUT",
            data: $scope.JSONVote,
        }).success(function(data, status, headers, response) {
            $scope.getRatings(problemId,solutionId);
        });
    }

    $scope.getSimiliarProblems = function(id){ 
        $http({
            url: "/rest/problems/"+id+"/suggestions",
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            data: new Object({
                only_solved:false,
                with_solution_proposals:true,
                explain:true,
            }),
            method: "POST",
        }).success(function(data, status, headers, response) {
            $scope.similiarProblems = data;
            console.log($scope.similiarProblems);
            $scope.active = true; 
            window.location = '#suggestions';
            document.getElementById("suggestions").focus();
        });
    $scope.alert = function(content, id){
        console.log(content.explanation.facts.problem_explanation[id].text);
        alert(content.explanation.facts.problem_explanation[id].text);
    }

    }

    $scope.suggestSolution = function(id,solId, solution){
         if (localStorage.getItem("loggedInUserBase64") == null) return;
        
       if($scope.liveEditHide == false){
            $scope.JSON = new Object();
            $scope.JSON.text = solution;
                $http({
            url: '/rest/problems/'+id+'/solutions/',
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
            data: $scope.JSON,
           method: "POST",
        }).success(function(data, status, headers, response) {
            $scope.result = "Diese Lösung wurde erfolgreich vorgeschlagen.";
            $scope.getProblem();
            $scope.liveEditHide = true;
        }).error(function(data, status, headers, config) {
            $scope.result = "Es ist ein Fehler aufgetreten, bitte versuchen Sie es erneut.";
        });
        }
        else{

        $http({
            url: '/rest/problems/'+id+'/solutions/'+solId,
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
           method: "PUT",
        }).success(function(data, status, headers, response) {
            $scope.result = "Diese Lösung wurde erfolgreich vorgeschlagen.";
            $scope.getProblem();
            $scope.liveEditHide = true;
        }).error(function(data, status, headers, config) {
            $scope.result = "Es ist ein Fehler aufgetreten, bitte versuchen Sie es erneut.";
        });
    }
    }

    $scope.suggestProblemModifiedProblem = function(id,solution){
         $scope.JSON = new Object();
         console.log(solution);
         $scope.JSON.text = solution;
         if (localStorage.getItem("loggedInUserBase64") == null) return;
        $http({
            url: '/rest/problems/'+id+'/solutions/',
            headers: {
                'Authorization': localStorage.getItem("loggedInUserBase64")
            },
           data: $scope.JSON,
           method: "POST",
        }).success(function(data, status, headers, response) {
            $scope.result = "Diese Lösung wurde erfolgreich vorgeschlagen.";
            $scope.getProblem();
        }).error(function(data, status, headers, config) {
            $scope.result = "Es ist ein Fehler aufgetreten, bitte versuchen Sie es erneut.";
        });
    }


$scope.showHiddenCommentBox = function() {
          console.log("wird aufgerufen");
            if ($scope.hiddenComment == true){
                $scope.hiddenComment = false;
                setTimeout(function() {
                    document.getElementById("comment").focus();
                }, 10);
            } else
                $scope.hiddenComment = true;
        }


$scope.commentProblem = function(comment, problemId) {
            $scope.JSON = new Object();
            $scope.JSON.text = comment;
            $http({
                url: '/rest/problems/' + problemId + '/comments/',
                method: "POST",
                headers: {
                    'Authorization': localStorage.getItem("loggedInUserBase64")
                },
                data: $scope.JSON,
            }).success(function(data, status, headers, config) {
                $scope.result = "Kommentar erfolgreich erstellt.";
                $scope.getProblem();
            }).error(function(data, status, headers, config) {
                (status == 403)
                $scope.result = "Sie sind nicht angemeldet. Bitte melden Sie sich an.";
                (status == 400)
                $scope.result = "Kommentar erstellen fehlgeschlagen. Bitte überprüfen Sie Ihre Eingaben.";
            });
        }


$scope.solutionProblem = function(solution) {
            $scope.JSON = new Object();
            $scope.JSON.text = solution;
            $http({
                url: '/rest/problems/' + localStorage.getItem("problemId") + '/solutions/',
                method: "POST",
                headers: {
                    'Authorization': localStorage.getItem("loggedInUserBase64")
                },
                data: $scope.JSON,
            }).success(function(data, status, headers, config) {
                $scope.result = "Lösung erfolgreich erstellt.";
                $scope.getProblem();
            }).error(function(data, status, headers, config) {
                (status == 403)
                $scope.result = "Sie sind nicht angemeldet. Bitte melden Sie sich an.";
                (status == 400)
                $scope.result = "Antwort erstellen fehlgeschlagen. Bitte überprüfen Sie Ihre Eingaben.";
            });

    }

    $scope.edit = function(value){
        if($scope.showEditArea == false){
            $scope.showEditArea = true;
            document.getElementById("modifybutton").setAttribute("aria-expanded","false");
                //$event.preventDefault();

        }
        else
        {
            $scope.showEditArea = false;
            document.getElementById("modifybutton").setAttribute("aria-expanded","true");
            setTimeout(function(){document.getElementById("modify").focus();
                document.getElementById("modify").value = document.getElementById("modify").value+' ';},10);

  }

        $scope.modifiedtext = value;

    }


    $scope.modify = function(newValue, probId){
            $scope.JSON = new Object();
            $scope.JSON.description = newValue;
            $http({
                url: '/rest/problems/'+probId,
                method: "PUT",
                headers: {
                    'Authorization': localStorage.getItem("loggedInUserBase64")
                },
                data: $scope.JSON,
            }).success(function(data, status, headers, config) {
                console.log(newValue);
                $scope.getProblem();
            }).error(function(data, status, headers, config) {
            });
    }

    $scope.formatToPercent = function(input){
        input = (input*100).toFixed(3);
        return input;
    }

    $scope.checkForFinalSolution = function(finalSolId, solId){
        console.log("arbeit");
        if(finalSolId==solId){
            $scope.isSolution = true;
            $scope.finalSolution[solId] = "Diese Antwort ist als Lösung markiert."
        }
        else{
          $scope.isSolution  = false;   
          $scope.finalSolution[solId] = "Diese Antwort ist nicht als Lösung markiert."
        }
           
    }
}


