    

function myProblemController($scope, $http, $log, $timeout) {

$scope.myProblems = [];
$scope.myStatus = [];
$scope.newComments = [];
$scope.newSolutions = [];
$scope.newCommentsNames = [];
$scope.newSolutionsNames = [];

$scope.getInformations = function(){
    $scope.getMyProblems();
} 

$scope.getMyProblems = function()
{
      $scope.JSON = new Object();
      $scope.JSON.LATEST = true;
      $http({
        url: '/rest/users/'+localStorage.getItem("loggedInUserName")+'/problems',
        method: "GET",
        headers:{'Authorization': localStorage.getItem("loggedInUserBase64")}
      }).success(function (data, status, headers, config) {
            $scope.myProblems = data; console.log($scope.myProblems);
            $scope.getMyStatus(); 
       }).error(function (data, status, headers, config) {
            console.log("Keine Suche möglich.");
        });
} 


$scope.getMyStatus = function()
{
      $http({
        url: '/rest/users/'+localStorage.getItem("loggedInUserName")+'/status',
        method: "GET",
        headers:{'Authorization': localStorage.getItem("loggedInUserBase64")}
      }).success(function (data, status, headers, config) {
            $scope.myStatus = data;     
        }).error(function (data, status, headers, config) {
            console.log("Keine neuen Informationen.");
        });
} 


$scope.getNameById = function(id)
{
  console.log($scope.myProblems);
if ($scope.myProblems != [])
  {

   for(var i=0; i<$scope.myProblems.length; i++){
    if(parseInt($scope.myProblems[i].id)==parseInt(id)){
        console.log("gefunden");
        return $scope.myProblems[i].title;
   } 
      }
  } 
  else
    console.log("Leer");
}

}

