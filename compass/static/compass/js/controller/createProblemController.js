function createProblemController($scope, $http, $log){   
  
$scope.choosenTags = [];
$scope.allTags = [];
$scope.problemName = "";
$scope.result = "";

    /*Fügt einen Tag optisch zur HTML-Seite hinzu. */
  

    $scope.createProblem = function (description){
      $http({
        url: '/rest/problems/',
        method: "POST",
        data: $scope.createJSON_title_description(/*localStorage.getItem("problem.name")*/ $scope.problemName, description, $scope.choosenTags),
        headers:{'Authorization': localStorage.getItem("loggedInUserBase64")}
      }).success(function (data, status, headers, config) {
            $scope.result = "Die Frage wurde erstellt und kann nun in 'Meine Fragen' angesehen werden.";
            document.getElementById("submit").disabled = true;
        }).error(function (data, status, headers, config) {
            if(status == 400 || status == 500 )
                $scope.result = "Sie haben einen Fehler bei der Eingabe Ihrer Daten gemacht. Bitte versuchen Sie es erneut.";
            if(status == 403)
                $scope.result = "Sie sind nicht angemeldet. Bitte melden Sie sich an, damit die Frage erstellt werden kann."
        });
}; 
    
        $scope.getAllTags = function (){
      $http({
        url: '/rest/tags/',
        method: "GET",
        headers:{'Authorization': localStorage.getItem("loggedInUserBase64")}
      }).success(function (data, status, headers, config) {
            $scope.allTags = data;
            console.log($scope.allTags);
        }).error(function (data, status, headers, config) {
            console.log("Tags laden nicht möglich.");
        });
}; 

   
    /*Sucht den ausgewählten (angeklickten) Tag und entfernt es 
    sowohl von der optischen Tag-Liste, sowie von der Auswahlliste (choosenTags)*/
    
            $scope.checkIfEnter = function($event, tag)
            {
                console.log($event);
/*                 if ($event.which==13)
                 {*/
                     $scope.deleteFromView(tag);
                     //$event.preventDefault(); 
                  /*}*/
            }

            $scope.addByEnter = function($event, tag)
            {
                var count=0;
                console.log($event);
                 if ($event.which==13)
                 {
                     $scope.addChoosenTag(tag);
                     angular.forEach($scope.foundTags, function(value, key)
                     {
                        count++;
                         if (value==tag){
                            console.log("DeleteFormSuggestedView: foundTags soll der Tag "+tag+" entfernt werden"); 
                            console.log("DeleteFormSuggestedView: foundTags = "+$scope.foundTags);                      
                            $scope.foundTags.splice(count-1,1);
                            console.log("DeleteFormSuggestedView: " +$scope.foundTags); 
                     }   
                 }); 
                     $event.preventDefault(); 
                 }
            }

         $scope.addChoosenTag = function(tag){
            var count = 0;
            console.log("AddchoosenTag: "+$scope.choosenTags);
            if (($scope.choosenTags.toString().indexOf(tag) == -1) && (tag != null)){
              console.log("Push "+$scope.choosenTags.push(tag)); 
               console.log("AddchoosenTag: deleteFromSuggestedView wird mit "+tag+" aufgerufen.");
               //$scope.deleteFromSuggestedView(tag);
           }
           else {
            angular.forEach($scope.foundTags, function(value, key)
                 {count++;
                   if (value==tag){
                        console.log("DeleteFormSuggestedView: foundTags soll der Tag "+tag+" entfernt werden");
                        $scope.addChoosenTag(tag);  
                        console.log("DeleteFormSuggestedView: foundTags = "+$scope.foundTags);                      
                        $scope.foundTags.splice(count-1,1);
                        console.log("DeleteFormSuggestedView: " +$scope.foundTags); 
                }   
                 }); 
           }
         }

         $scope.deleteFromView = function(tag){
         var count=0;
                 angular.forEach($scope.choosenTags, function(value, key)
                 {
                    count++;
                    if (value==tag)
                    {
                        $scope.choosenTags.splice(count-1,1);   
                    }  
                 }); 
             }
             $scope.ucFirst = function(string) 
             {  console.log("Hier ist der umzuwandelnde String: "+string);
                return string.substring(0, 1).toUpperCase() + string.substring(1);
              }

         $scope.deleteFromSuggestedView = function(tag){
         var count=0;

                 angular.forEach($scope.foundTags, function(value, key)
                 {count++;
                   if (value==tag){
                        console.log("DeleteFormSuggestedView: foundTags soll der Tag "+tag+" entfernt werden");
                        $scope.addChoosenTag(tag);  
                        console.log("DeleteFormSuggestedView: foundTags = "+$scope.foundTags);                      
                        $scope.foundTags.splice(count-1,1);
                        console.log("DeleteFormSuggestedView: " +$scope.foundTags); 
                }   
                 }); 

         }
   


/*Decode(array) wandelt ein Array in einen String um und gibt diesen zurück*/  

   $scope.foundTags = [];


       var str = "";
        $scope.decode = function(array){
            str="";
            for (i in array){
             str+=String.fromCharCode(array[i]);
            }
            return str.toLocaleLowerCase();
        }

/*record überwacht die Tastatureingaben nach einem Leerzeichen, zeichnet diese auf und gibt sie an findTags(String) weiter*/

        $scope.rec = [];
        $scope.record = function($event)
            {
            $scope.rec.push($event.keyCode);
            if ($event.which==8)
                $scope.rec.pop();
            if(($event.which==32) || ($event.which==100) || (($event.which==49) && ($event.which==32)) || (($event.which==16) && ($event.which==32))){
           // $scope.decode($scope.rec);
            if ($scope.choosenTags != ""){
            angular.forEach($scope.choosenTags, function(key, value){

                if($scope.ucFirst(($scope.decode($scope.rec))).indexOf(key)){
                    console.log("Hier in der schleife "+$scope.ucFirst(key));
                    console.log($scope.decode($scope.rec));
                    $scope.findTags($scope.ucFirst($scope.decode($scope.rec)));
                }
            });
        } else
        {
            console.log("Hier "+$scope.ucFirst($scope.decode($scope.rec)));
            var test = $scope.decode($scope.rec);
            var test1 = $scope.ucFirst(test);
            console.log(test);
            console.log(test1);
            $scope.findTags($scope.ucFirst($scope.decode($scope.rec)));
        }
            $scope.rec = [];

            }
            
    }

/*findTags(String) überprüft ob der übergebene String ein Tag aus der Menge aller verfügbaren Tags ist*/
         var tempWord ="";
         $scope.findTags = function(rec){
            {   

                angular.forEach($scope.allTags, function(key, value){
                      if((rec.indexOf(key.name) == 0) && ($scope.foundTags.toString().indexOf(key.name) != 0))
                {
                console.log("In der findTags-Schleife"+$scope.foundTags);
                $scope.foundTags.push(key.name);
                console.log($scope.foundTags);
                }
             })
            }
         
         }

         $scope.setProblemName = function(name){
            $scope.problemName = name;
         }

         $scope.enabledButton = function(){
            console.log("funkt");
            document.getElementById("submit").disabled = false;
         }

      


        
    }

      

