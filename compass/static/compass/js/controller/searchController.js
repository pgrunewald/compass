    

function createSearchController($scope, $http, $log) {

$scope.choosenTags = [];
$scope.showResults = "";
$scope.searchquestions = [];
$scope.searchQuestionsPerPage = 10;
$scope.pageCount = 0; 
$scope.noresults = true;
$scope.noresultstext = "";

$scope.search = function(title, tags, topic){

        $scope.JSON = new Object();
        $scope.JSON.PER_PAGE = $scope.searchQuestionsPerPage;
        $scope.JSON.PAGE = $scope.pageCount;
        $scope.JSON.ALL = false;
        $scope.JSON.title = $scope.splitSearchItems(title);
        console.log($scope.JSON.title);
        $scope.JSON.tags = tags;
        $scope.JSON.topic = topic;

        console.log("pageCount = "+$scope.pageCount);
    if (localStorage.getItem("searchFromSearchfield") == "true"){
        title = localStorage.getItem("searchpara");
        localStorage.setItem("searchFromSearchfield", "false"); 
    }
            console.log($scope.JSON);
      $http({
        url: '/rest/search/problems/',
        method: "POST",
        data: $scope.JSON,
        headers:{'Authorization': localStorage.getItem("loggedInUser")}
      }).success(function (data, status, headers, config) {
        $scope.noresults = true;
            $scope.searchquestions[$scope.pageCount] = data;

             if ( data.more_results == true)
             {
                $scope.pageCount++;
                $scope.search(title, tags);
                console.log("In der If-Bedingung");
             }

            $scope.showResults = $scope.searchquestions[0]; 
            $scope.choosenTags = [];  
            console.log(data.results.length == 0);
            if (data.results.length == 0){

                $scope.noresults = false;
                $scope.noresultstext = "Ihre Suche hat keine Ergebnisse gefunden."
            } 
            
        }).error(function (data, status, headers, config) {
            console.log("Keine Suche möglich.");
        });
} 

$scope.splitSearchItems = function(text){
  return text.split(" ");
}



$scope.loadSearchPageCount = function(pageCount){
$scope.showResults = $scope.searchquestions[pageCount];
console.log($scope.showResults);
}

$scope.resetPageCount = function(){
    $scope.pageCount = 0;
}

$scope.resetSearchQuestions = function(){
    $scope.searchquestions = [];
}

$scope.newSearch = function(title, tags, topic){
    $scope.resetPageCount();
    $scope.resetSearchQuestions();
    $scope.search(title, tags, topic);
}

$scope.searchByEnter = function($event, title, tags){
    if ($event.which==13){
       $scope.newSearch(title, tags); 
    }
}
$scope.addChoosenTag = function(tag){
            console.log("Übergebener Tag = "+tag);

            if (($scope.choosenTags.toString().indexOf(tag) == -1) && (!(tag === undefined) || (tag != null)))
               $scope.choosenTags.push(tag);   
         }

         $scope.deleteFromView = function(tag){
         var count=0;
                 angular.forEach($scope.choosenTags, function(value, key)
                 {count++;
                   console.log(value,key)
                   if (value==tag){
                        console.log(count);
                        $scope.choosenTags.splice(count-1,1);
                        console.log($scope.choosenTags);    
                }   
        })
         }

            $scope.checkIfEnter = function(event, tag)
                {
                console.log(event);
                     if (event.which==13)
                         {
                             $scope.deleteFromView(tag);
                             event.preventDefault();
                             console.log("Per 'Enter' gelöscht.");
                         }
                }
            $scope.temp = [];
 
 $scope.checkOnload = function()
    {
            $scope.temp.push(localStorage.getItem("searchTag"));
                
                
               if (localStorage.getItem("searchFromSearchfield") == "true" )
               {
                console.log("In der suchbediengung"); 
                $scope.newSearch(localStorage.getItem("searchpara"),[]);
               } 

               if (localStorage.getItem("searchedByTopic") == "true" )
               {
                console.log("topic"); 
                $scope.newSearch("",[],localStorage.getItem("searchTopic"));
                localStorage.setItem("searchedByTopic","false");
               } 
               
               if (localStorage.getItem("searchedByTag") == "true")
                   {
                     //$scope.searchByTag();
                     console.log("Vor dem newSearch");
                     console.log(localStorage.getItem("searchTag"));
                     $scope.newSearch("",$scope.temp);
                     localStorage.setItem("searchedByTag", "false");
                    }
            }

    $scope.deleteRequired = function(){
        document.getElementById("tagsearch").removeAttribute("required");
    }



 


 
}