

/*angular.module('testDirektive', [])
.controller('direktive' function($scope){
	$scope.tag ={h1open: '<h1>', h1close:'</h1>', value: 'testDirektive erfolgreich!!!'};})
.directive('myDirektive', function(){return {restrict: 'E',	template:'{{h1open}}{{value}}{{close}}'};});
*/

    angular.module('docsSimpleDirective', [])
    .directive('ntAttrDir', function factory() {
    var directiveDefinitionObject = {
    template: "<div> Test </div>",
    replace:false,
    restrict:"A"
    };
    return directiveDefinitionObject;
    
    });
  