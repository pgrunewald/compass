# coding=utf-8
import os
import sys
import transaction

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from compass.models import (
    DBSession,
    Base,
    Problem,
    Tag,
    Comment,
    User,
    Solution,
    ProblemSolutionProposal,
    FeedbackType,
    Topic
    )


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    if 'preserve' not in argv:
        Base.metadata.drop_all(engine)
        transaction.commit()

    Base.metadata.create_all(engine)

    # Add dummy data
    from compass.security import create_password_hash_with_salt
    password_hash, salt = create_password_hash_with_salt(password=u'dummy-password', salt='dummy-salt')

    # Tags for assistive technology based on ISO 9999
    iso = Tag(name=u"Technische Hilfsmittel",
              description=u"Allgemein Hilfsmittel, assistive Technologien, Unterstützungstechnologien")
    # ISO 9999-22 Hilfsmittel für Kommunikation und Information
    iso_22 = Tag(name=u"Hilfsmittel für Kommunikation und Information",
                 description=u"")
    iso_22_03 = Tag(name=u"Sehhilfen",
                    description=u"zum Beispiel: Lupen, Fernrohre, Bildschirmlesegeräte")
    iso_22_06 = Tag(name=u"Hörhilfen",
                    description=u"Hilfsmittel, um die von einer Person mit Hörschädigung wahrgenommenen Töne zu konzentrieren, zu verstärken und zu modulieren.")
    iso_22_09 = Tag(name=u"Sprechhilfen",
                    description=u"Hilfsmittel für Personen, deren eigene Stimmlautstärke für die Verständigung nicht ausreicht.")
    iso_22_12 = Tag(name=u"Schreib- und Zeichenhilfen",
                    description=u"Hilfsmittel zur Übermittlung von Informationen über Bilder, Symbole oder Sprache.")
    iso_22_15 = Tag(name=u"Rechenhilfen",
                    description=u"zum Beispiel: Rechengeräte, Kalkulationssoftware")
    iso_22_18 = Tag(name=u"Hilfsmittel, die Audio- und Videoinformationen aufzeichnen, abspielen und anzeigen",
                    description=u"Hilfsmittel zur Aufzeichnung oder Ausgabe von Informationen in Audio- oder Videoformaten sowie Hilfsmittel, die diese Funktionen miteinander verbinden")
    iso_22_21 = Tag(name=u"Hilfsmittel für die Nahkommunikation",
                    description=u"Hilfsmittel, mit denen zwei am selbem Ort befindliche Personen sich verständigen können")
    iso_22_24 = Tag(name=u"Hilfsmittel für Telefonie und Telematik",
                    description=u"")
    iso_22_27 = Tag(name=u"Hilfsmittel für das Alarmieren, Anzeigen, Erinnern und Signalisieren",
                    description=u"zum Beispiel: Uhren, Türmelder, Regenanzeiger, Licht- und Farberkennungsgeräte")
    iso_22_30 = Tag(name=u"Lesehilfen",
                    description=u"zum Beispiel: Blattwender, Taktile Lesegeräte, Zeichen-Lesegeräte, Lesematerialien mit Sprachausgabe")
    iso_22_30_03 = Tag(name=u"Lesematerialien mit Sprachausgabe",
                       description=u"")
    iso_22_30_09 = Tag(name=u"Multimedia-Lesematerialien",
                       description=u"Hilfsmittel zur Speicherung geschriebener oder gedruckter Informationen und ihre Wiedergabe mit Hilfe mehrerer Medien, z.B. akustischer und optischer Medien")
    iso_22_30_21 = Tag(name=u"Zeichen-Lesegeräte",
                       description=u"Hilfsmittel, die geschriebene Texte lesen und in eine alternative Form der optischen, akustischen und/oder taktilen Kommunikation umwandeln")
    iso_22_30_24 = Tag(name=u"Taktile Lesegeräte",
                       description=u"Medien zur Darstellung der Inhalte in Braille")
    iso_22_30.children.extend([iso_22_30_03, iso_22_30_09, iso_22_30_21, iso_22_30_24])
    iso_22_33 = Tag(name=u"Computer und Terminals",
                    description=u"")
    iso_22_36 = Tag(name=u"Eingabegeräte für Computer",
                    description=u"zum Beispiel: Tastaturen, alternative Eingabegeräte, Eingabezubehör, Eingabesoftware")
    iso_22_36_03 = Tag(name=u"Tastaturen",
                       description=u"zum Beispiel: Braille-Tastaturen")
    iso_22_36_12 = Tag(name=u"Alternative Eingabegeräte",
                       description=u"zum Beispiel: optische Scanner, Spracherkennungseinheiten, Kontakttastaturen, Datenhandschuhe und Gehirn-Computer-Schnittstellen")
    iso_22_36_15 = Tag(name=u"Eingabezubehör",
                       description=u"Hilfsmittel zur Verbindung von Eingabesystemen und Computer")
    iso_22_36_18 = Tag(name=u"Eingabesoftware",
                       description=u"zum Beispiel: Einfingersteuerungen und Bildschirmtastaturen")
    iso_22_36_21 = Tag(name=u"Hilfsmittel zur Positionierung des Bildschirmzeigers und zur Auswahl von Objekten auf dem Computerbildschirm",
                       description=u"Alternativ zu einer Computermaus verwendete Hilfsmittel")
    iso_22_36.children.extend([iso_22_36_03, iso_22_36_12, iso_22_36_15, iso_22_36_18, iso_22_36_21])
    iso_22_39 = Tag(name=u"Ausgabegeräte für Computer",
                    description=u"zum Beispiel: Braillezeilen, Drucker, Anzeigebildschirme, Sprachausgaben, Plotter und Synthesizer")
    iso_22_39_04 = Tag(name=u"Visuelle Computeranzeigegeräte und Zubehör",
                       description=u"")
    iso_22_39_05 = Tag(name=u"Taktile Computerdisplays (braillezeilen)",
                       description=u"Geräte, die Informationen aus einem Computer über den Tastsinn anzeigen")
    iso_22_39_06 = Tag(name=u"Drucker",
                       description=u"zum Beispiel: Braille-Drucker/Plotter")
    iso_22_39_07 = Tag(name=u"Terminals mit Sprachausgabe",
                       description=u"Geräte, die Informationen aus einem Computer akustisch durch Ausgabe von Sprache oder anderen Tönen anzeigen")
    iso_22_39_12 = Tag(name=u"Spezielle Ausgabesoftware (Screenreader)",
                       description=u"Software zur Vergrößerung von Text und Grafiken zur Anzeige auf einem Computerbildschirm, Software für das Lesen des Displays und die Umwandlung in Sprache (Bildschirmlesegeräte)")
    iso_22_39.children.extend([iso_22_39_04, iso_22_39_05, iso_22_39_06, iso_22_39_07, iso_22_39_12])
    iso_22.children.extend([iso_22_03, iso_22_06, iso_22_09, iso_22_12, iso_22_15, iso_22_18, iso_22_21, iso_22_24, iso_22_27, iso_22_30, iso_22_33, iso_22_36, iso_22_39])
    iso.children.extend([iso_22])
    DBSession.add(iso)

    # Screenreader (source: en.wikipedia.org/wiki/List_of_screen_readers)
    reader_95reader = Tag(name=u"95Reader", description=u"Hersteller: SSCT")
    reader_brltty = Tag(name=u"BRLTTY", description=u"Hersteller: The BRLTTY Team")
    reader_browsealoud = Tag(name=u"BrowseAloud", description=u"Hersteller: Texthelp Systems Inc")
    reader_capture_assistant = Tag(name=u"Capture Assistant", description=u"Hersteller: Renovation Software")
    reader_chromevox = Tag(name=u"ChromeVox", description=u"Hersteller: Google")
    reader_microsurf = Tag(name=u"Microsurf", description=u"Hersteller: Microsurf")
    reader_claro_screenruler_suite = Tag(name=u"Claro ScreenRuler Suite", description=u"Hersteller: Claro Software")
    reader_clickhear = Tag(name=u"ClickHear", description=u"Hersteller: gh LLC")
    reader_clickhear_mobile = Tag(name=u"ClickHear Mobile", description=u"Hersteller: gh LLC")
    reader_clipspeak = Tag(name=u"ClipSpeak", description=u"Hersteller: Daniel Innala Ahlmark")
    reader_cobra = Tag(name=u"COBRA", description=u"Hersteller: BAUM Retec")
    reader_edbrowse = Tag(name=u"Edbrowse", description=u"Hersteller: Karl Dahlke")
    reader_emacspeak = Tag(name=u"Emacspeak", description=u"Hersteller: T. V. Raman")
    reader_fire_vox = Tag(name=u"Fire Vox", description=u"Hersteller: Charles L. Chen")
    reader_hal = Tag(name=u"HAL", description=u"Hersteller: Dolphin Computer Access")
    reader_ht_reader = Tag(name=u"HT Reader", description=u"Hersteller: HT Visual")
    reader_izoom = Tag(name=u"iZoom", description=u"Hersteller: Issist")
    reader_jaws = Tag(name=u"JAWS", description=u"Hersteller: Freedom Scientific")
    reader_kurzweil_3000_firefly = Tag(name=u"Kurzweil 3000-firefly", description=u"Hersteller: Cambium Learning Group Inc.")
    reader_kurzweil_1000 = Tag(name=u"Kurzweil 1000", description=u"Hersteller: Cambium Learning Group Inc.")
    reader_leitor_de_telas = Tag(name=u"Leitor de Telas", description=u"Hersteller: MC / CPqD")
    reader_lingspeak = Tag(name=u"Lingspeak", description=u"Hersteller: Lingit")
    reader_lookout = Tag(name=u"LookOUT", description=u"Hersteller: Choice Technology")
    reader_magic = Tag(name=u"MAGic", description=u"Hersteller: Freedom Scientific")
    reader_microsoft_narrator = Tag(name=u"Microsoft Narrator", description=u"Hersteller: Microsoft")
    reader_mobile_speak = Tag(name=u"Mobile Speak", description=u"Hersteller: Code Factory")
    reader_model_t_reader = Tag(name=u"Model T Reader", description=u"Hersteller: Dolphin Computer Access")
    reader_nonvisual_desktop_access_nvda = Tag(name=u"NonVisual Desktop Access (NVDA)", description=u"Hersteller: NonVisual Desktop Access project")
    reader_orca = Tag(name=u"Orca", description=u"Hersteller: GNOME")
    reader_pc_talker = Tag(name=u"PC-Talker", description=u"Hersteller: Kochi System Development")
    reader_pcvoz = Tag(name=u"PCVoz", description=u"Hersteller: EzHermatic")
    reader_project_metalmouth = Tag(name=u"Project metalmouth", description=u"Hersteller: Alistair Garrison")
    reader_proloquo = Tag(name=u"Proloquo", description=u"Hersteller: AssistiveWare")
    reader_readhear = Tag(name=u"ReadHear", description=u"Hersteller: gh LLC")
    reader_read_outloud = Tag(name=u"Read:Outloud", description=u"Hersteller: Don Johnston Inc.")
    reader_readspeaker = Tag(name=u"ReadSpeaker", description=u"Hersteller: ReadSpeaker Holding B.V.")
    reader_read_write = Tag(name=u"Read & Write", description=u"Hersteller: Texthelp Systems")
    reader_recite_me = Tag(name=u"Recite Me", description=u"Hersteller: Recite Me Ltd")
    reader_simply_talker = Tag(name=u"Simply Talker", description=u"Hersteller: EcoNet International")
    reader_sodelscot = Tag(name=u"SodelsCot", description=u"Hersteller: Sodels Factory")
    reader_speakeasy_media_system = Tag(name=u"SpeakEasy Media System", description=u"Hersteller: NDU")
    reader_spoken_web = Tag(name=u"Spoken Web", description=u"Hersteller: Eyal Shalom")
    reader_suse_blinux = Tag(name=u"SUSE-Blinux", description=u"Hersteller: Novell")
    reader_supernova = Tag(name=u"Supernova", description=u"Hersteller: Dolphin Computer Access")
    reader_system_access = Tag(name=u"System Access", description=u"Hersteller: Serotek")
    reader_talkback = Tag(name=u"TalkBack", description=u"Hersteller: Google")
    reader_talks = Tag(name=u"Talks", description=u"Hersteller: Nuance Communications")
    reader_teletender = Tag(name=u"TeleTender", description=u"Hersteller: TeleTender.org")
    reader_text_to_speech = Tag(name=u"Text to Speech", description=u"Hersteller: SpeakComputers.com")
    reader_thunder_screenreader = Tag(name=u"Thunder ScreenReader", description=u"Hersteller: Sensory Software")
    reader_virtual_vision = Tag(name=u"Virtual Vision", description=u"Hersteller: MicroPower")
    reader_voiceover = Tag(name=u"VoiceOver", description=u"Hersteller: Apple Inc.")
    reader_webanywhere = Tag(name=u"WebAnywhere", description=u"Hersteller: University of Washington")
    reader_winzoom = Tag(name=u"WinZoom", description=u"Hersteller: Clarity")
    reader_window_eyes = Tag(name=u"Window-Eyes", description=u"Hersteller: GW Micro")
    reader_screen_access = Tag(name=u"Screen Access for All", description=u"Hersteller: National Association for the Blind New Delhi")
    reader_zoomtext = Tag(name=u"ZoomText", description=u"Hersteller: Ai Squared")

    iso_22_39_12.children.extend([reader_95reader, reader_brltty, reader_browsealoud, reader_capture_assistant, reader_chromevox, reader_microsurf, reader_claro_screenruler_suite, reader_clickhear, reader_clickhear_mobile, reader_clipspeak, reader_cobra, reader_edbrowse, reader_emacspeak, reader_fire_vox, reader_hal, reader_ht_reader, reader_izoom, reader_jaws, reader_kurzweil_3000_firefly, reader_kurzweil_1000, reader_leitor_de_telas, reader_lingspeak, reader_lookout, reader_magic, reader_microsoft_narrator, reader_mobile_speak, reader_model_t_reader, reader_nonvisual_desktop_access_nvda, reader_orca, reader_pc_talker, reader_pcvoz, reader_project_metalmouth, reader_proloquo, reader_readhear, reader_read_outloud, reader_readspeaker, reader_read_write, reader_recite_me, reader_simply_talker, reader_sodelscot, reader_speakeasy_media_system, reader_spoken_web, reader_suse_blinux, reader_supernova, reader_system_access, reader_talkback, reader_talks, reader_teletender, reader_text_to_speech, reader_thunder_screenreader, reader_virtual_vision, reader_voiceover, reader_webanywhere, reader_winzoom, reader_window_eyes, reader_screen_access, reader_zoomtext])

    os = Tag(name=u"Betriebssystem", description=u"")

    linux = Tag(name=u"Linux", description=u"")
    windows = Tag(name=u"Windows", description=u"")
    macos = Tag(name=u"Mac OS", description=u"")

    windows95 = Tag(name=u"Windows 95", description=u"")
    windows98 = Tag(name=u"Windows 98", description=u"")
    windows98SE = Tag(name=u"Windows 98 Second Edition", description=u"")
    windows2000 = Tag(name=u"Windows 2000", description=u"")
    windowsME = Tag(name=u"Windows Millenium Edition", description=u"")
    windowsXP = Tag(name=u"Windows XP", description=u"")
    windowsXP64 = Tag(name=u"Windows XP 64 Bit", description=u"")
    windowsXPhome = Tag(name=u"Windows XP Home", description=u"")
    windowsXPPro = Tag(name=u"Windows XP Profesional", description=u"")
    windowsVista = Tag(name=u"Windows Vista", description=u"")
    windowsVista64 = Tag(name=u"Windows Vista 64 Bit", description=u"")

    windowsVistaHome = Tag(name=u"Windows Vista Home Basic", description=u"")
    windowsVistaHomePre = Tag(name=u"Windows Vista Home Premium", description=u"")
    windowsVistaUltimate = Tag(name=u"Windows Vista Ultimate", description=u"")

    windows7 = Tag(name=u"Windows 7", description=u"")
    windows7Ult = Tag(name=u"Windows 7 Ultimate", description=u"")
    windows7Pro = Tag(name=u"Windows 7 Profesional", description=u"")
    windows7HomePre = Tag(name=u"Windows 7 Home Premium", description=u"")
    windows7HomeBas = Tag(name=u"Windows 7 Home Basic", description=u"")

    windows8 = Tag(name=u"Windows 8", description=u"")
    windows81 = Tag(name=u"Windows 8.1", description=u"")

    debian = Tag(name=u"Debian Linux", description=u"")
    kali = Tag(name=u"Kali Linux", description=u"")
    dreamLinux = Tag(name=u"Dreamlinux", description=u"")

    knoppix = Tag(name=u"Knoppix", description=u"")
    ubuntu = Tag(name=u"Ubuntu Linux", description=u"")
    gentoo = Tag(name=u"Gentoo Linux", description=u"")
    redhat = Tag(name=u"Red Hat Linux", description=u"")
    suse = Tag(name=u"Suse Linux", description=u"")

    macosX10 = Tag(name=u"Mac OS X 10.0 (Cheetah)", description=u"")
    macosX101 = Tag(name=u"Mac OS X 10.1 (Puma)", description=u"")
    macosX102 = Tag(name=u"Mac OS X 10.2 (Jaguar)", description=u"")
    macosX103 = Tag(name=u"Mac OS X 10.3 (Panther)", description=u"")
    macosX104 = Tag(name=u"Mac OS X 10.4 (Tiger)", description=u"")
    macosX105 = Tag(name=u"Mac OS X 10.5 (Leopard)", description=u"")
    macosX106 = Tag(name=u"Mac OS X 10.6 (Snow Leopard)", description=u"")
    macosX107 = Tag(name=u"Mac OS X 10.7 (Lion)", description=u"")
    macosX108 = Tag(name=u"OS X 10.8 (Mountain Lion)", description=u"")
    macosX109 = Tag(name=u"OS X 10.9 (Mavericks)", description=u"")
    mobileOS = Tag(name=u"Mobile Betriebssysteme", description=u"")
    android = Tag(name=u"Android", description=u"")
    android1 = Tag(name=u"Android Base", description=u"")
    android15 = Tag(name=u"Android 1.5 Cupcake", description=u"")
    android16 = Tag(name=u"Android 1.6 Donat", description=u"")
    android2 = Tag(name=u"Android 2.0, 2.1 Eclair", description=u"")
    android22 = Tag(name=u"Android 2.2.x Froyo", description=u"")
    android23 = Tag(name=u"Android 2.3.x Gingerbread", description=u"")
    android3 = Tag(name=u"Android 3.x Honeycomb", description=u"")
    android4 = Tag(name=u"Android 4.0.x Ice Cream Sandwich", description=u"")
    android41 = Tag(name=u"Android 4.1.x Ice Jelly Bean", description=u"")
    android44 = Tag(name=u"Android 4.4.x KitKat", description=u"")
    iOS = Tag(name=u"iOS", description=u"")
    iOS1 = Tag(name=u"iOS 1", description=u"")
    iOS2 = Tag(name=u"iOS 2", description=u"")
    iOS3 = Tag(name=u"iOS 3", description=u"")
    iOS4 = Tag(name=u"iOS 4", description=u"")
    iOS5 = Tag(name=u"iOS 5", description=u"")
    iOS6 = Tag(name=u"iOS 6", description=u"")
    iOS7 = Tag(name=u"iOS 7", description=u"")
    windowsPhone = Tag(name=u"Windows Phone", description=u"")
    blackberryOS = Tag(name=u"Blackberry OS", description=u"")
    firefoxOS = Tag(name=u"Firefox OS", description=u"")
    badaOS = Tag(name=u"Bada OS", description=u"")

    os.children.extend([linux, windows, macos])

    windows.children.extend([windows95, windows98, windows2000, windowsME, windowsXP, windowsVista, windows7, windows8])
    windows98.children.append(windows98SE)
    windowsXP.children.extend([windowsXP64, windowsXPhome, windowsXPPro])
    windowsVista.children.extend([windowsVista64, windowsVistaHome, windowsVistaHomePre, windowsVistaUltimate])
    windows7.children.extend([windows7Ult, windows7Pro, windows7HomePre, windows7HomeBas])
    windows8.children.append(windows81)
    linux.children.append(debian)
    debian.children.extend([kali, dreamLinux])
    linux.children.extend([debian, knoppix, ubuntu, gentoo, redhat, suse])
    macos.children.extend([macosX10, macosX101, macosX102, macosX103, macosX104, macosX105, macosX106, macosX107, macosX108, macosX109])
    os.children.append(mobileOS)
    mobileOS.children.extend([android, iOS, windowsPhone, blackberryOS, firefoxOS, badaOS])
    iOS.children.extend([iOS1, iOS2, iOS3, iOS4, iOS5, iOS6, iOS7])
    android.children.extend([android1, android15, android16, android2, android22, android23, android3, android4, android41, android44])

    software = Tag(name=u"Software",
                   description=u"")

    browser = Tag(name=u"Browser", description=u"")

    browser_firefox = Tag(name=u"Firefox", description=u"")
    browser_firefox_3 = Tag(name=u"Firefox 3", description=u"")
    browser_firefox_3_6 = Tag(name=u"Firefox 3.6", description=u"")
    browser_firefox_4 = Tag(name=u"Firefox 4", description=u"")
    browser_firefox_5 = Tag(name=u"Firefox 5", description=u"")
    browser_firefox_6 = Tag(name=u"Firefox 6", description=u"")
    browser_firefox_7 = Tag(name=u"Firefox 7", description=u"")
    browser_firefox_8 = Tag(name=u"Firefox 8", description=u"")
    browser_firefox_9 = Tag(name=u"Firefox 9", description=u"")
    browser_firefox_10 = Tag(name=u"Firefox 10", description=u"")
    browser_firefox_11 = Tag(name=u"Firefox 11", description=u"")
    browser_firefox_12 = Tag(name=u"Firefox 12", description=u"")
    browser_firefox_13 = Tag(name=u"Firefox 13", description=u"")
    browser_firefox_14 = Tag(name=u"Firefox 14", description=u"")
    browser_firefox_15 = Tag(name=u"Firefox 15", description=u"")
    browser_firefox_16 = Tag(name=u"Firefox 16", description=u"")
    browser_firefox_17 = Tag(name=u"Firefox 17", description=u"")
    browser_firefox_18 = Tag(name=u"Firefox 18", description=u"")
    browser_firefox_19 = Tag(name=u"Firefox 19", description=u"")
    browser_firefox_20 = Tag(name=u"Firefox 20", description=u"")
    browser_firefox_21 = Tag(name=u"Firefox 21", description=u"")
    browser_firefox_22 = Tag(name=u"Firefox 22", description=u"")
    browser_firefox_23 = Tag(name=u"Firefox 23", description=u"")
    browser_firefox_24 = Tag(name=u"Firefox 24", description=u"")
    browser_firefox_25 = Tag(name=u"Firefox 25", description=u"")
    browser_firefox_26 = Tag(name=u"Firefox 26", description=u"")
    browser_firefox_27 = Tag(name=u"Firefox 27", description=u"")
    browser_firefox_28 = Tag(name=u"Firefox 28", description=u"")
    browser_firefox_29 = Tag(name=u"Firefox 29 beta", description=u"")
    browser_firefox_30 = Tag(name=u"Firefox 30 aurora", description=u"")
    browser_firefox.children.extend([browser_firefox_3, browser_firefox_3_6, browser_firefox_4, browser_firefox_5, browser_firefox_6, browser_firefox_7, browser_firefox_8, browser_firefox_9, browser_firefox_10, browser_firefox_11, browser_firefox_12, browser_firefox_13, browser_firefox_14, browser_firefox_15, browser_firefox_16, browser_firefox_17, browser_firefox_18, browser_firefox_19, browser_firefox_20, browser_firefox_21, browser_firefox_22, browser_firefox_23, browser_firefox_24, browser_firefox_25, browser_firefox_26, browser_firefox_27, browser_firefox_28, browser_firefox_29, browser_firefox_30])

    browser_chrome = Tag(name=u"Chrome", description=u"")
    browser_chrome_14 = Tag(name=u"Chrome 14", description=u"")
    browser_chrome_15 = Tag(name=u"Chrome 15", description=u"")
    browser_chrome_16 = Tag(name=u"Chrome 16", description=u"")
    browser_chrome_17 = Tag(name=u"Chrome 17", description=u"")
    browser_chrome_18 = Tag(name=u"Chrome 18", description=u"")
    browser_chrome_19 = Tag(name=u"Chrome 19", description=u"")
    browser_chrome_20 = Tag(name=u"Chrome 20", description=u"")
    browser_chrome_21 = Tag(name=u"Chrome 21", description=u"")
    browser_chrome_22 = Tag(name=u"Chrome 22", description=u"")
    browser_chrome_23 = Tag(name=u"Chrome 23", description=u"")
    browser_chrome_24 = Tag(name=u"Chrome 24", description=u"")
    browser_chrome_25 = Tag(name=u"Chrome 25", description=u"")
    browser_chrome_26 = Tag(name=u"Chrome 26", description=u"")
    browser_chrome_27 = Tag(name=u"Chrome 27", description=u"")
    browser_chrome_28 = Tag(name=u"Chrome 28", description=u"")
    browser_chrome_29 = Tag(name=u"Chrome 29", description=u"")
    browser_chrome_30 = Tag(name=u"Chrome 30", description=u"")
    browser_chrome_31 = Tag(name=u"Chrome 31", description=u"")
    browser_chrome_32 = Tag(name=u"Chrome 32", description=u"")
    browser_chrome_33 = Tag(name=u"Chrome 33", description=u"")
    browser_chrome_34 = Tag(name=u"Chrome 34 beta", description=u"")
    browser_chrome.children.extend([browser_chrome_14, browser_chrome_15, browser_chrome_16, browser_chrome_17, browser_chrome_18, browser_chrome_19, browser_chrome_20, browser_chrome_21, browser_chrome_22, browser_chrome_23, browser_chrome_24, browser_chrome_25, browser_chrome_26, browser_chrome_27, browser_chrome_28, browser_chrome_29, browser_chrome_30, browser_chrome_31, browser_chrome_32, browser_chrome_33, browser_chrome_34])

    browser_internet_explorer = Tag(name=u"Internet Explorer", description=u"")
    browser_internet_explorer_6 = Tag(name=u"Internet Explorer 6", description=u"")
    browser_internet_explorer_7 = Tag(name=u"Internet Explorer 7", description=u"")
    browser_internet_explorer_8 = Tag(name=u"Internet Explorer 8", description=u"")
    browser_internet_explorer_9 = Tag(name=u"Internet Explorer 9", description=u"")
    browser_internet_explorer_10 = Tag(name=u"Internet Explorer 10", description=u"")
    browser_internet_explorer_11 = Tag(name=u"Internet Explorer 11", description=u"")

    browser_internet_explorer.children.extend([browser_internet_explorer_6, browser_internet_explorer_7, browser_internet_explorer_8, browser_internet_explorer_9, browser_internet_explorer_10, browser_internet_explorer_11])

    browser_opera = Tag(name=u"Opera", description=u"")
    browser_opera_10_6 = Tag(name=u"Opera 10.6", description=u"")
    browser_opera_11_1 = Tag(name=u"Opera 11.1", description=u"")
    browser_opera_11_5 = Tag(name=u"Opera 11.5", description=u"")
    browser_opera_11_6 = Tag(name=u"Opera 11.6", description=u"")
    browser_opera_12_10 = Tag(name=u"Opera 12.10", description=u"")
    browser_opera_12_14 = Tag(name=u"Opera 12.14", description=u"")
    browser_opera_12_15 = Tag(name=u"Opera 12.15", description=u"")
    browser_opera_12_16 = Tag(name=u"Opera 12.16", description=u"")
    browser_opera_15 = Tag(name=u"Opera 15", description=u"")
    browser_opera_16 = Tag(name=u"Opera 16", description=u"")
    browser_opera_17 = Tag(name=u"Opera 17", description=u"")
    browser_opera_18 = Tag(name=u"Opera 18", description=u"")
    browser_opera_19 = Tag(name=u"Opera 19", description=u"")
    browser_opera_20 = Tag(name=u"Opera 20", description=u"")
    browser_opera_21_dev = Tag(name=u"Opera 21 dev", description=u"")

    browser_opera.children.extend([browser_opera_10_6, browser_opera_11_1, browser_opera_11_5, browser_opera_11_6, browser_opera_12_10, browser_opera_12_14, browser_opera_12_15, browser_opera_12_16, browser_opera_15, browser_opera_16, browser_opera_17, browser_opera_18, browser_opera_19, browser_opera_20, browser_opera_21_dev])

    browser_safari = Tag(name=u"Safari", description=u"")
    browser_safari_4 = Tag(name=u"Safari 4", description=u"")
    browser_safari_5 = Tag(name=u"Safari 5", description=u"")
    browser_safari_5_1 = Tag(name=u"Safari 5.1", description=u"")
    browser_safari.children.extend([browser_safari_4, browser_safari_5, browser_safari_5_1])

    browser_lynx = Tag(name=u"Lynx", description=u"")

    browser_webie = Tag(name=u"WebbIE", description=u"")

    browser.children.extend([browser_firefox, browser_internet_explorer, browser_safari, browser_opera, browser_webie, browser_lynx])

    software.children.append(browser)

    apps = Tag(name=u"Anwendung", description=u"")

    apps_mail = Tag(name=u"E-Mail-Programme", description=u"")

    app_thunderbird = Tag(name=u"Thunderbird", description=u"")
    app_windows_outlook = Tag(name=u"Windows Outlook", description=u"")
    app_windows_live_mail = Tag(name=u"Windows Live Mail", description=u"")
    apps_mail.children.extend([app_thunderbird, app_windows_outlook, app_windows_live_mail])

    apps_pdf_reader = Tag(name=u"PDF-Reader", description=u"")

    app_adobe_acrobat_reader = Tag(name=u"Adobe Acrobat Reader", description=u"")
    apps_pdf_reader.children.extend([app_adobe_acrobat_reader])

    apps_text_processors = Tag(name=u"Programm zur Textverarbeitung", description=u"")
    app_open_office = Tag(name=u"Open Office", description=u"")
    app_libre_office = Tag(name=u"LibreOffice", description=u"")
    app_tex_live = Tag(name=u"Tex Live", description=u"")
    app_abi_word = Tag(name=u"AbiWord", description=u"")
    app_word = Tag(name=u"Microsoft Word", description=u"")
    app_pdflatex = Tag(name=u"pdflatex", description=u"")
    apps_text_processors.children.extend([app_open_office, app_libre_office, app_tex_live, app_abi_word, app_word, app_pdflatex])

    apps_instant_messenger = Tag(name=u"Instant Messenger", description=u"")
    app_skype = Tag(name=u"Skype", description=u"")
    apps_instant_messenger.children.extend([app_skype])

    app_ocr = Tag(name=u"Texterkennung (OCR)", description=u"")
    app_fine_reader = Tag(name=u"Fine Reader", description=u"")
    app_ocr.children.extend([app_fine_reader])

    apps.children.extend([apps_mail, apps_pdf_reader, apps_text_processors, apps_instant_messenger, app_ocr])
    software.children.append(apps)

    DBSession.add(software)

    content_types = Tag(name=u"Inhaltstypen", description=u"")
    content_type_websites = Tag(name=u"Webseiten", description=u"")
    content_type_table = Tag(name=u"Tabellen", description=u"")
    content_type_image = Tag(name=u"Bilder", description=u"")
    content_type_flash = Tag(name=u"Adobe Flash", description=u"")

    content_type_websites.children.extend([content_type_table, content_type_image, content_type_flash])

    content_type_documents = Tag(name=u"Dokumente", description=u"")
    document_pdf = Tag(name=u"PDF-Dokumente", description=u"")
    document_word = Tag(name=u"Word-Dokumente", description=u"")

    content_type_documents.children.extend([document_pdf, document_word])

    content_types.children.extend([content_type_websites, content_type_documents])

    DBSession.add(content_types)


    ting = User(username=u'Theressa_Ting', email=u'ting@mail.com', password_hash=password_hash, password_salt=salt)
    koepp = User(username=u'Kira_Koepp', email=u'koepp@mail.com', password_hash=password_hash, password_salt=salt)
    auvil = User(username=u'Annmarie_Auvil', email=u'auvil@mail.com', password_hash=password_hash, password_salt=salt)
    rempe = User(username=u'Reinaldo_Rempe', email=u'rempe@mail.com', password_hash=password_hash, password_salt=salt)
    jaeger = User(username=u'Jule_Jaeger', email=u'jaeger@mail.com', password_hash=password_hash, password_salt=salt)
    easterday = User(username=u'Emiko_Easterday', email=u'easterday@mail.com', password_hash=password_hash, password_salt=salt)
    whitehouse = User(username=u'Winfred_Whitehouse', email=u'whitehouse@mail.com', password_hash=password_hash, password_salt=salt)
    beadles = User(username=u'Bernarda_Beadles', email=u'beadles@mail.com', password_hash=password_hash, password_salt=salt)
    forster = User(username=u'Francesca_Forster', email=u'forster@mail.com', password_hash=password_hash, password_salt=salt)
    # caprio = User(username=u'Carolyne_Caprio', email=u'caprio@mail.com', password_hash=password_hash, password_salt=salt)
    clements = User(username=u'Cassie_Clements', email=u'clements@mail.com', password_hash=password_hash, password_salt=salt)
    isham = User(username=u'Ivy_Isham', email=u'isham@mail.com', password_hash=password_hash, password_salt=salt)
    riggle = User(username=u'Raymond_Riggle', email=u'riggle@mail.com', password_hash=password_hash, password_salt=salt)
    drovin = User(username=u'Deneen_Drovin', email=u'drovin@mail.com', password_hash=password_hash, password_salt=salt)
    iler = User(username=u'Ivonne_Iler', email=u'iler@mail.com', password_hash=password_hash, password_salt=salt)
    ruff = User(username=u'Rena_Ruff', email=u'ruff@mail.com', password_hash=password_hash, password_salt=salt)
    chappell = User(username=u'Candance_Chappell', email=u'chappell@mail.com', password_hash=password_hash, password_salt=salt)
    ingles = User(username=u'Illa_Ingles', email=u'ingles@mail.com', password_hash=password_hash, password_salt=salt)
    # rossiter = User(username=u'Reanna_Rossiter', email=u'rossiter@mail.com', password_hash=password_hash, password_salt=salt)
    burruel = User(username=u'Bell_Burruel', email=u'burruel@mail.com', password_hash=password_hash, password_salt=salt)
    edman = User(username=u'Erin_Edman', email=u'edman@mail.com', password_hash=password_hash, password_salt=salt)
    # nadal = User(username=u'Normand_Nadal', email=u'nadal@mail.com', password_hash=password_hash, password_salt=salt)
    # melle = User(username=u'Markus_Melle', email=u'melle@mail.com', password_hash=password_hash, password_salt=salt)

    # feedback_type1 = FeedbackType(serialization_name=u'comprehensibility', name=u"Verständlichkeit",
    #                               description=u"", min_value=1, max_value=5, scope=FeedbackType.SCOPE_SOLUTION_ONLY)
    # feedback_type2 = FeedbackType(serialization_name=u'speed', name=u"Schnelligkeit",
    #                               description=u"", min_value=1, max_value=5, scope=FeedbackType.SCOPE_SOLUTION_ONLY)
    feedback_type1 = FeedbackType(serialization_name=u'difficulty', name=u"Schwierigkeitsgrad",
                                  description=u"", min_value=-1, max_value=1, scope=FeedbackType.SCOPE_SOLUTION_ONLY)

    usefulness_type1 = FeedbackType(serialization_name=u'helpful', name=u"Hilfreich?",
                                    description=u"Ich finde dieser Lösungsvorschlag enthält hilfreiche Informationen.",
                                    min_value=-1, max_value=1, scope=FeedbackType.SCOPE_SOLUTION_AND_PROBLEM)
    # usefulness_type2 = FeedbackType(serialization_name=u'solved', name=u"Gelöst?",
    #                                 description=u"Diese Lösung hat mir geholfen dieses Problem zu lösen.",
    #                                 min_value=0, max_value=1, scope=FeedbackType.SCOPE_SOLUTION_AND_PROBLEM)
    # usefulness_type3 = FeedbackType(serialization_name=u'appropriate', name=u"Passend?",
    #                                 description=u"Dieser Lösungsvorschlag passt zu diesem Problem.",
    #                                 min_value=0, max_value=1, scope=FeedbackType.SCOPE_SOLUTION_AND_PROBLEM)

    DBSession.add_all([feedback_type1])
    DBSession.add_all([usefulness_type1])

    ting_problem = Problem(title=u"""Fine Reader und Nero unter Windows 8, hat jemanden es probiert?, welche Version läuft und welche nicht?""", description=u"""Hallo nochmal,so, ein anderes Problem:Fine Reader, ganz wichtig fÚr mich: Jetzt habe ich den Fine Reader 8, und unter Windows XP, läuft es wunderbar. Weiß jemand ob unter Windows 8, es geht, oder sonst, welche neue Version läuft gut?.Uned mit Jaws und Nvda, läuft es?, welche Version?, hat jemanden die Erfahrung darÚber gemacht und gehabt?.Und  dasselbe  mit Nero: Ich habe die Version 6.3, aber ich habe gehÓrt, daß mit Windows 8 es nicht klapt, und daß n die neuen Versionen nicht gut Akcesibel sind. Weiß jemand etwas darÜber?.Wie immer, vielen Dank fÜr ihre HIlfe, und nochmal schÖnen Dienstag!Raymond""", user=ting, tags=[windows8, app_fine_reader])
    solution1 = Solution(text=u"""Hallo,
muss ja nicht richtig sein, aber der Finereader 8 läuft meines Erachtens
definitiv nicht unter Windows 8. Ich habe den Finereader 9.0 und der
läuft noch so grade unter Windows 7, allerdings gab es damals nur
Windows Vista und ABBYY hat hier tatsächlich damals Tests gemacht,
welche von ihren Programmen auch noch unter Windows 7 laufen. Also du
könntest auf www.abbyy.com nachschauen, ob da auch was von Windows 8
steht und für welche Versionen, als ich vor - na... - zwei Wochen? - Auf
der Seite war, stand da aber nur über Umwege (ich glaube ich musste
sogar googeln) auf der Supportseite eben die Info mit Windows 7. Schon
da stand der Finereader 8 glaube ich nicht bei. Zudem stand im Handbuch
des Finereader 9 explizit unter den neuen Funktionen drin, dass mit
Finereader 9 jetzt Mehrkernprozessoren unterstützt würden, somit würde
ich annehmen, dass dies in den Vorgängerversionen nicht der Fall ist und
ich weiß nicht, was das dann zu bedeuten hat, da ja eigentlich heute
alle Rechner Dualcore-Prozessoren oder ähnliches haben. Ich war echt
froh, dass ich meinen Finereader somit noch auf meinem PC mit Windows 7
und Dualcore nutzen kann. Den Abbyy PDF Transformer 2.0 zum Beispiel
habe ich gar nicht erst installiert. Hätte ich vielleicht probieren
können, aber so wichtig ist er mir nicht und der unterstützte nicht mal
Vista und schon gar nicht Mehrkernprozessoren.

Ich hoffe, du hast mein kompliziertes Deutsch jetzt einigermaßen
verstanden, ansonsten frag ruhig nochmal nach, dann versuch ichs nochmal
einfacher. Das ist immer eines meiner Mankos mit der leichten Sprache,
deshalb kann ich auch Kindern schlecht was erklären... ;-) Manchmal hat
Intelligenz und hohe Bildung doch eher Nachteile. Aber egal. ;-)
--
Viele Grüße
Rena""", user=ruff)

    ting_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=ruff))

    koepp_problem = Problem(title=u"""Narrator unter Windows 7""", description=u"""Hallo!

Wie bekomme ich den Narrator unter Windows 7 32-Bit aktiviert? Nach Aufruf der narrator.exe tut sich bei mir leider nichts.

Auf meinem System sind bereits JAWS und NVDA in der jeweils aktuellen Version installiert.

Schon mal vielen Dank im voraus für eure Antworten.

Gruß

Kira""", user=koepp, tags=[windows7, reader_nonvisual_desktop_access_nvda, reader_jaws])

    comment1 = Comment(text=u"""Oh mein Gott, hiiiiilfeee! *Lach*
also ich habe vorhin einfach im Startmenü mal "Narrator" eingegeben und
das erste und einzige Programm, welches ich angeboten bekam geöffnet und
siehe da, er begann schon mit einer englischen Stimme zu sprechen und
wollte, dass ich eine Stimme installiere, allerdings sprach er sehr
seltsame Wörter, womöglich deutsche Wörter auf englisch aber auch nicht
unbedingt immer das, was gerade auf dem Bildschirm stand, wie ich mit
COBRA verfolgen konnte. Z.B. sprach er beim TAB-Drücken ständig was von
"Registerkarte", obwohl da nur ein Schließn-Schalter stand. Also laufen
tut der Narrator bei mir, ich habe allerdings Windows 7 64-Bit. Und dann
hab ich ihn beendet, nachdem ich erst den Prompt zum Installieren von
Stimmen geschlossen habe und das Ding sprach noch hundert Jahre lang
weiter, aber ich glaub, jetzt ist er dann wirklich aus. Habe jetzt keine
Zeit, das weiter zu testen.
--
Viele Grüße
Rena""", user=ruff)
    comment2 = Comment(text=u"""Hallo!,ich bin nicht sicher welche von den 2 Optionen richtig ich, aber ich glaube,daß wenn du Alt N oder Windowstaste und dann Entertaste drÜckst, fang den Narrador an. Um zu schweigen, ist es (das ist sicher) mit Blckshift plus F1.Ich ahbe nur es auf Spanisch proviert, weil bei default diese aktiviert bei mir ich, und ich kann sagen, daß es wunderbar funktioniert, so wunderbar, daß ich Jaws nicht brauchen wÚrde, so er könnte ganz gut Jaws ersetzen, ich brauche noch Jaws, wegen WörterBÜcher aber sonst, kÖnnte ich nur diesen Narrador und nicht Jaws benutzen.Ich möchte ganz gerne den Narrador auf Deutsch proviert, ich habe gelesen, daß er auch auf Deutsch verfÚgbar ist, aber ich weiß nicht wie ich machen kann, um die Stimme und die Sprache zu ¨Ndern.So, wenn jemand endeckt etwas neues darÜber, bitte, mach einen Zeichen.ubrigens, habe ich mit Windows 8 64 bits proviert.Candance""",
                       user=chappell)
    comment3 = Comment(text=u"""Hallo Candance,

du sprachst doch von Windows 8, u. hier will jemand dies unter Windows
7, dort gibt es nur die Englische-Sprachausgabe u. keine Deutsche!

Stimmt es dann so, dass du meintest, dies gilt für Windows 8, bitte gib
mir mal bescheidt, da ich mir dem nächst ein neues Notebook zu legen
muss, da mein Altes-Schlepptop den Geist aufgegeben hat, lächel u. Dank
im Voraus!

Lg. Winfred
""", user=whitehouse)
    comment4 = Comment(text=u"""Hallo Winfred,ja ja, ich gebe  dich  Bescheid. Unter Windows 8 gibt es den Narrator auf Deutsch, ich habe es auf der Seite von mIcrosoft gelesen, so bin ich sicher.Als meinen Rechner bei Default auf Spanisch konfiguriert ist, der Narrator spricht auf Spanisch (und er macht es wunderbar, die Stimme ist vbiel besser als Eloquence), wie gesagt, ich weiß und ich bin sicher, daß auch auf Deutsch es gibt, mein Problem ist, daß ich weiß noch nicht, wie es die Stimme ändert kann, aber ichc habe jetzt an MIcosoft gemeldet, und wenn ich die Antwort haben wird, werde ich euch es sagen.  Jedenfalls, mit Widnows 8, gibt es die Deutsche Stimme und in  dienem REchnen wird bei Default auf Deutsch, und so, mit er Deutsche Stimmen konfiguriert.LG Rena""",
                       user=forster)
    comment5 = Comment(text=u"""Hallo,
ich will das nur als Info geben, weil ich selber genauso wenig Ahnung
vom Narrator habe: Es gibt auf der Microsoft Seite deutsche Stimmen zum
kostenlosen Download, diese sollen angeblich sowohl unter Windows 7 als
auch unter Windows 8 nutzbar sein, soweit ich gehört habe. Allerdings
weiß ich nicht, ob man unter Windows 7 dann auch die
Narrator-Systemsprache auf Deutsch einstellen kann. Fakt ist, dass ich
irgendiw den Eindruck hatte, dass die englische Stimme gestern versucht
hat, deutsche Wörter zu sprechen, womöglich geht das unter Windows 7
auch. Normalerweise verstehe ich auch das amerikanische Englisch dieser
Stimmen immer recht gut, aber da hab ich nichts verstanden. Daher würde
ich den Leuten, die das wirklich interessiert und die ihre Zeit opfern
möchten, empfehlen, den Narrator mal zu starten und dann gibt es da ja
irgend so einen Prompt zum Installieren von Stimmen. Allerdings ist mir
nicht klar, ob das ein Prompt ist, mit dem man dann direkt Stimmen
installieren kann oder ob das nur eine Info ist, dass man noch deutsche
Stimmen installieren soll. Fakt ist, dass ich über den
Schließen-Schalter nicht hinwegkam gestern. Aber so wichtig ist mir der
Narrator im Moment auch nicht, würde mich aber dennoch interessieren,
wenn sich jemand damit unter Windows 7 beschäftigt, wie es läuft. Ich
hab da im Moment nicht mehr die Zeit zu, hättet ihr mich mal im Urlaub
gefragt, der ist seit Donnerstach vorbei... :-)
--
Viele Grüße
Rena""", user=ruff)
    koepp_problem.comments.extend([comment1, comment2, comment3, comment4, comment5])

    auvil_problem = Problem(title=u"""Narrator in Windows, es ist fantastisch!""", description=u"""Hallo zusammen,als ihr schon weißt habe ich einen REchner mit Windows 8 gekauft. Obwohl ich noch  vielen Sahcne sehen soll, bevor ich es benutzen kann, hbe ich eine wunderbare Endeckung gemacht, die mit euch mitteilen mÖchte, es ist gÜltig, in Windows 8 und in Windows 8 auch.So, MIcrosoft ht einen Program entwikelt,  die Nartor heißt. es kommt kostenloss und Direkt mit Windows, und es ist einen wirklichen Screen REader, ich habe auf Spanisch proviert, und ich kann euch sagen, daß es funktioniertwirklich wunderbar! Ich brauche noch Jaws, wegen einigen WörterbÜcher, die mit diesen nicht klappen, aber sonst, wäre ich Jaws nicht installiert, weil dieser Narrador alles wirklich macht!.Ich habe einbißchen in der hIlfe von Windows gelesen, und es ist auch auf Deutsch, Englisch, FranzÓsisch (und ja, auch Japanisch und chinesisch) verfÜgbar. Wen ihr noch nicht kenet, empfehle ich euch es provieren, mir hat es wirkilich seher  gefahlen.Fals jeman noch es benutz, weiß jemand wie ich die Sprache
  und die Stime ändern kann?, ich hbe es nicht gefunden, aber ich habe gelesen,m daß es möglich ist.So, viel Spaß!.SchÖnnen Sontag!Rena""", user=auvil, tags=[windows8])

    rempe_problem = Problem(title=u"""Faierfox als Standart-Browser festlegen, wie geht dies?""", description=u"""Hallo liebe Faierfox-Noutzer,

kann mir Jemand sagen, wie ich in Browser-Faierfox es so einstelle, dass
ich ihn als Standart-Browsser festlege?

Ich hab ein Betriebssystem-Windows 7 u. weis auch, wie man dort
Programme als Stadndart festlegt, will es aber nicht über Windows,
sondern wie Früher, über den Browser-Faierfox, weis halt nicht mehr, wo
bzw. wie es noch mal geht!

Danke für Info im Voraus!

Lg. Reinaldo""", user=rempe, tags=[windows7, browser_firefox])
    solution1 = Solution(text=u"""Hi Reinaldo,

öffne den Firefox und gehe mit der Tastenkombination ALT+x zu Extras.
Dort den Menüpunkt Einstellungen auswählen. Danach mt den Pfeiltasten
auf Eerweitert gehen. Die Registerkarte Allgemein wählen und mt Tab
durchlaufen bis Beim Start prüfen, ob Firefox der Standardbrowser ist
ein Haken setzen. dann einmal Tab und einen Haken "als Standardbrowser
Festlegen. Bis OK. weiter und fertig""", user=chappell)

    rempe_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=chappell))

    jaeger_problem = Problem(title=u"""OpenOffice""", description=u"""Hallo zusammen,

hat hier jemand Erfahrung mit dem Programm openoffice als kostenlose
Alternative zu Windows office? Ist das openoffice empfehlenswert und mit
screenreader bedienbar? Gibt es sonstige Hinweise zu openoffice für blinde
PC-Benutzer? Ich verwende Windows XP.


Grüße von Jule """, user=jaeger, tags=[app_open_office, windowsXP])

    solution1 = Solution(text=u"""
> Hallo zusammen,
Hallo Jule,

ich habe Open office mal probiert und war sehr unzufrieden damit. mag sein,
dass ich da etwas falsch gemacht habe.

ich kenne deine Ansprüche nicht. Für die privaterstellung von texten nutze
ich Dataword dass es bei www.softcologne.de gibt. Ich sage immer, es ist wie
Word, nur mit text-editor. Blockfunktionen und vieles andere mehr.

Gruß
Erin """, user=edman)
    jaeger_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=edman))

    easterday_problem = Problem(title=u"""Telefonbuch für Blinde""", description=u"""Hallo zusammen,

kann mir jemand ein internettelefonbuch nennen, dass nur mit dem nötigsten
aufwartet? Wenn ich über google einen Arzt eingebe, kommen immer die gelben
seiten oder sonst ein kram, der völlig überladen ist. Das manko so mancher
internetseite.

danke und gruß
Emiko""", user=easterday, tags=[content_type_websites])
    solution1 = Solution(text=u"""Hallo Emiko,

Am 28.09.2013 11:58, schrieb Emiko_Easterday:

> kann mir jemand ein internettelefonbuch nennen, dass nur mit dem nötigsten
> aufwartet? Wenn ich über google einen Arzt eingebe, kommen immer die gelben
> seiten oder sonst ein kram, der völlig überladen ist. Das manko so mancher
> internetseite.

Ich nutze gerne die mobile Variante von dasoertliche.de, also
mobile.dasoertliche.de. Diese Seite beschränkt sich auf das nötigste,
jedenfalls gelingt es mir damit recht schnell Telefonnummern oder
Adressen zu finden. Gleiches gilt auch z.B. für die Seite der Bahn, wenn
ich eine Verbindung suche nutze ich auch viel lieber die mobile Seite
also mobile.bahn.de.

Gruß
Daneen""", user=drovin)
    solution2 = Solution(text=u"""Ergänzend dazu sei noch erwähnt, daß es auch eine PDA-Version von
Klicktel gibt:
pda.klicktel.de
Die nutze ich recht gerne, weil sie sehr einfach gehalten ist.""", user=ingles)
    easterday_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=drovin))
    easterday_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution2, user=ingles))

    whitehouse_problem = Problem(title=u"""Kontakte in Skype erstellen!""", description=u"""Hallo!

Ich habe einen Skype Kontakt unter .. , aber ich weiß nicht mehr
wie man einen Skype Kontakt erstellt, ich möchte nämlich für meine Frau
einen Skype Kontakt einrichten. Weiß jemand wie das geht?

LG
Winfred Whitehouse""", user=whitehouse, tags=[app_skype])

    comment1 = Comment(text=u"""Hallo Winfred,

hast du deiner Frau Skype schon auf den Rechner installiert?
Willst du ihr ein Skypekonto einrichten oder ihr deinen Skypenamen geben?
Welche Skypeversion hast du?

LG, Raymond""", user=riggle)

    comment2 = Comment(text=u"""Hallo Raymond!

Ich möchte meiner Frau ein Skype Konto erstellen. Die Skypeversion weiß ich
nicht. Es dürfte die neueste Version sein.

LG
Daneen""", user=whitehouse, parent=comment1)

    comment3 = Comment(text=u"""hey Winfred,

wo ist dann dein problem?
einfach am PC mit deinen skypekonto abmelden, mal über den
startbildschirm vom unangemelditen skype taben, und "Erstellen Sie ein
Konto" anwählen.

------ mit freundlichem Gruß Ivone""", user=iler)

    whitehouse_problem.comments.extend([comment1, comment2, comment3])

    solution1 = Solution(text=u"""hey Winfred,
alternativ kannst du natürlich auch die folgende Adresse nutzen:
http://www.skype.com/go/registration?setlang=de&intsrc=client|reg-a|0/6.6.0.106/272""", user=auvil)

    solution2 = Solution(text=u"""Hallo Winfred,
okay.
Dann gehe am besten so vor, dass du deiner Frau Skype auf den Rechner
installierst.
Wenn du dies gemacht hast, dann öffne bitte Skype.
Wenn Skype geöffnet ist, gehe so lange auf Tap bis der Link "Erstellen Sie
ein Konto" kommt. Diesen bestätigst du mit "ok". Es öffnet sich dann ein
Fenster, wo du den Skypenamen, das Kennwort, dann noch einmal das Kennwort
zur Wiederholung und die Mailadresse eingeben musst. Dies bestätigst du und
dann dauert es etwas, bis das Konto erstellt wird. Wärend du die Eingaben
eingibst kommen auch so fragen, ob du die Google-Tulbar mit installieren
magst. Dies habe ich deaktiviert, da ich das nicht wollte. Dann wirst du
auch gefragt, ob Skype beim Windowsstart ausgeführt  werden soll. Dies
kannst du aktivieren oder deaktivieren. Dann kannst du noch aktivieren oder
deaktivieren, ob du immer angemeldet bleiben magst oder nicht. also ob du es
so haben magst, dass du beim anmelden in Skype immer dein Kennwort eingeben
magst oder nicht.

Liebe Grüße

Raymond""", user=riggle)

    whitehouse_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=auvil))
    whitehouse_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution2, user=riggle))

    comment4 = Comment(text=u"""Hallo Raymond!

Danke für deine Infos.

LG
Winfred""", user=whitehouse)

    comment5 = Comment(text=u"""Hallo Winfred,
gern geschehn.
Hat es mit der Kontoerstellung geklappt?
LG, Raymond""", user=riggle, parent=comment4)

    comment6 = Comment(text=u"""Hallo Raymond!

Es hat leider nicht funktioniert, da man das Rechapka eingeben muss und die
Audioabspielung versteht man nicht. Wir haben bei der Hilfe zwar schon ein
Mail geschrieben, aber noch keine Antwort bekommen.

LG
Winfred""", user=whitehouse, parent=comment5)

    comment7 = Comment(text=u"""hey Winfred,
nutzt du den firefox mit webwisum? im algemeinen komme ich mit dieser
kombination und nvda gannz gut zurecht.""", user=iler)

    whitehouse_problem.comments.extend([comment4, comment5, comment6, comment7])

    solution3 = Solution(text=u"""hey Winfred,
eine idee:
du schreibst mir per pm eine mail mit einer telefonnummer, wo ich dich
erreichen kann, oder mit den daten, die ich angeben soll, und ich
versuche mein Glück mit webvisum""", user=iler)

    whitehouse_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution3, user=iler))

    comment8 = Comment(text=u"""Hallo Winfred,
das verstehe ich jetzt ehrlich gesagt nicht, denn als ich mich damals in
Skype angemeldet hatte, musste ich keinen Code oder so was in der Richtung
eingeben.
LG, Raymond""", user=riggle)

    comment9 = Comment(text=u"""Hallo Raymond!

Als ich mich damals vor einigen Jahren angemeldet habe, musste ich auch
keinen Code eingeben. Nur jetzt muss man es leider tun und das ist für mich
unmöglich.

LG
Winfred""", user=whitehouse, parent=comment8)

    comment10 = Comment(text=u"""Hallo Winfred,
ich würde dir sehr gerne weiterhelfen.
Ich habe zwar Firefox nur leider noch kein Webvisum.
Sonnst hätte ich dir beim Kontoeinrichten geholfen.
Sorry.
LG, Raymond""", user=riggle)

    whitehouse_problem.comments.extend([comment8, comment9, comment10])

    comment11 = Comment(text=u"""Hi Raymond,
doch, Christian hat schon recht. Ich habe mir letztes Jahr im Februar zum
ersten Mal ein Skype-Konto anlegen wollen und da gab es auch schon den
graphischen Code, den man lösen musste. Ich habe mit Webvisum versucht, den
Code zu lösen, aber ich kam mit dem Firefox nicht wirklich auf der Seite
zurecht. Außerdem kriegte Webvisum den Code irgendwie überhaupt nicht
gelöst, weshalb ich mir das Konto dann schlussendlich von einer sehenden
Freundin habe einrichten lassen. Das war alles ziemlich enttäuschend für
mich.

LG Kira """, user=koepp)

    comment12 = Comment(text=u"""Hallo Kira,
ich habe ja nicht gesagt, dass ich Christian nicht glaube ;:-).
Ich musste nur damals bei meiner Anmeldung keinen Code lösen, daher wusste
ich nicht, das sich das geändert hat.
LG, Raymond""", user=riggle, parent=comment11)

    comment13 = Comment(text=u"""Hi,

Ja mit den Kods heuft sich. Ja OK-Mail war noch einer der Anbieter die bei
der Einrichtung eines Mailkontos kein grafischen Kod haben wollt, aber nun
musst ich feststellen das die jetzt auch ein haben wollen.

Bell.""", user=burruel)

    comment14 = Comment(text=u"""hey Raymond,
scheinbar gibt es dort wirklich ein kaptca, allerdings scheint webwisum
bei mir etwas erkannt zu haben, sodass ich nach wie vor meine hilfe
anbieten würde.""", user=iler)

    comment15 = Comment(text=u"""Hi,

ich glaube, ich installiere mir Webvisum.
Wo kann ich das downloaden?
LG, Raymond""", user=riggle, parent=comment14)

    comment16 = Comment(text=u"""hey Raymond,
einfach im suchfenster der adonns "webvisum" eingeben, und die
suchergebnisse auswählen""", user=iler, parent=comment15)

    comment17 = Comment(text=u"""Hi Nils,
okay, mache ich.
LG, Raymond""", user=riggle)

    whitehouse_problem.comments.extend([comment12, comment13, comment14, comment15, comment16, comment17])

    solution4 = Solution(text=u"""Hallo,
für alle, die auch mit Audio-Capchas Probleme haben, evtl. folgende
kleine Tipps. Nein, ich möchte kein Klugscheißer sein, aber ich weiß aus
eigener Erfahrung, dass man manchmal ziemlich bescheuert sein kann, als
wäre man vor eine Beton-Wand gelaufen. :-)

Folgende Erfahrungen habe ich mit Audio-Capchas bisher gemacht:
1. Am Besten versteht man sie natürlich, wenn man einen Kopfhörer
aufsetzt. Einen Kopfhörer anzuschließen und damit die Audio-Capcha
anzuhören ist somit m. E. ein Muss. Eine sehr gute Stereoanlage, an
deren Box(en) man recht nahe sitzt, tuts auch. Computerboxen oder der
eingebaute Lautsprecher des Computers hingegen sind nicht unbedingt eine
gute Wahl, denn je nach dem, wie gut die Boxen eingestellt sind oder wie
der Resonanzraum / die Membranen der Boxen ausgestaltet sind, ist der
Klang nicht linear und es werden falsche Frequenzen verstärkt. Zudem ist
Kopfhörer am Besten, weil man bei einer Anlage oder Boxen den Equalizer
in einer Art und Weise eingestellt haben könnte, der die wichtigen
Frequenzen verschluckt. Das wäre z.B. der Fall, wenn man sehr basslastig
eingestellt hat und die mittleren Frequenzen weitestgehend ausgeblendet
sind. Aber gerade die Mitten sind für solche Audiocapchas wichtig.
2. Man hat ja glücklicherweise die Möglichkeit, sich die Capcha immer
wieder anzuhören. Wenn sie einmal vorgelesen wurde, ist es zwar oftmals
so, dass man da, wo man auf Audio oder so geklickt hatte einen Link
zurück zur Grafik angeboten bekommt und damit dann wieder zurück zur
Grafik kommt, aber hernach ist ja dann der Audiolink wieder da und man
kann ihn erneut anklicken. Ich weiß nicht mehr, ob das bei Skype so war
oder bei Googlemail damals, das kann ich nicht mehr sagen. Auf manchen
Seiten kann man das Audio auch direkt mehrmals hören.
3. Je nach Zahlen und / oder Buchstaben, die im Code enthalten sind,
kann man den Code entweder besser oder schlechter verstehen. Deshalb
würde ich immer empfehlen, wenn ihr nicht versteht oder euch nicht
sicher seid das einzugeben, was es evtl. sein könnte. Wenn ihr Glück
hattet war es richtig, wenn ihr Pech hattet, dann wird ein neuer Code
generiert. Und den könnt ihr dann womöglich besser verstehen und habt
damit mehr Glück. Ich habe meist auch mehrere Versuche gebraucht,
manchmal hat es mich auch zur Weißglut gebracht, weil ich einfach immer
die am schlechtesten Verständlichen Codes drin hatte, wo ständig zwei
oder drei drin war und ich dazwischen nicht unterscheiden konnte aber
irgendwann hatte ich dann glück. Ich glaube bei der Einrichtung von
Googlemail hatte ich damals bestimmt 20 Versuche gebraucht... Hat sich
aber dann doch gelohnt... *Lach*

Und zum guten Schluss, bevor die Frage kommt: Diese Audio-Capchas kann
man nicht verständlicher machen, weil sie dann durch
Spracherkennungssoftware zu einfach entschlüsselt werden könnten und das
will man verhindern. Die Codes hat man ja nur deshalb eingeführt, damit
nicht ein Bot z.B. ein Mail- oder Skypekonto einrichtet und das dann
missbräuchlich verwendet. Somit ist es für uns dann auch wieder von
Vorteil, dass dadurch nicht den Bots wieder Tür und Tor geöffnet ist.
--
Viele Grüße
Francesca""", user=forster)

    whitehouse_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution4, user=iler))

    beadles_problem = Problem(title=u"""Thunderbird 12 and Screen Reader Issues""", description=u"""I am using the Screen Reader JAWS 13 and NVDA (not together) and Thunderbird 12 has issues with JAWS 13 and NVDA. It does NOT read properly. It worked quite well with all other prior releases. My suggestion is Mozilla while BETA Testing ALL future releases work with Freedom Scientific and/or NVDA to ensure BOTH Screen Readers work without any issues.""", user=beadles, tags=[app_thunderbird, reader_jaws, reader_nonvisual_desktop_access_nvda])

    comment1 = Comment(text=u"""I don't think Mozilla has the resources to test compatibility of new releases with all third-party software utilities such as screen readers and anti-virus programs. But if you want to revert to an earlier version of TB until a compatible version of JAWS is released, get the installer here:

ftp://ftp.mozilla.org/pub/thunderbird/releases/""", user=auvil)

    comment2 = Comment(text=u"""Fully Understandable. Is it possible for Mozilla to encourage Visually impaired consumers to beta test?""", user=clements)
    comment3 = Comment(text=u"""I think that Mozilla already welcomes feedback from any user who finds deficiencies in the core program, but I just don't see them paying much attention to incompatible non-TB software - "that's beyond our control' - is the likely response. It's probably enough work just to ensure compatibility with several operating systems.""", user=auvil)
    beadles_problem.comments.extend([comment1, comment2, comment3])

    forster_problem = Problem(title=u"""Accessibility issues with pdflatex""", description=u"""Currently, I'm generating the following pdf with pdflatex: http://dl.dropbox.com/u/2682197/book.pdf

Unfortunately, this document is a bit incompatible with adobe screen reader (for blind people).

This audio recording demonstrates the result of using adobe screen reader with that document: http://dl.dropbox.com/u/2682197/speech.mp3

In the recording, it's impossible to hear the chapter or section headers.

Any ideas of how to build a "more accessible" document?""", user=forster, tags=[app_pdflatex, app_adobe_acrobat_reader, document_pdf])

    solution1 = Solution(text=u"""Your PDF doesn't has the right Character Map defined. You can see that when you copy an heading:

CucZ Z u«±§¶±¶§Z o £§u±
You have the same problem many people have, that you can't copy or search the text in the pdf. There are basically two different things you can use: cmap or \pdfgentounicode. How and when you should use which, is explained here by Heiko. I have found that it resolves all issues I had with copying and now I can even copy most math.

There is also the package mmap, which you can use instead of cmap. More is explained here: http://tex.stackexchange.com/a/64457/5042""", user=easterday)

    forster_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=easterday))

    # caprio_problem = Problem(title=u"""""", description=u"""""", user=caprio, tags=[pdf_tag])
    clements_problem = Problem(title=u"""PDFs bearbeiten""", description=u"""Hallo!

Gibt es eine Möglichkeit, PDF-Dateien bzw. -Formulare als Blinder unter
Windows zu bearbeiten?
--
Ciao!

Cassie""", user=clements, tags=[iso_22_39_12, windows, document_pdf])

    solution1 = Solution(text=u"""Guten MOrgen, Alex,ja, klar daß es möglich ist. Ich empfehle dir das Program Fine Reader, in meiner Meinung sit das beste. Mit diesem, kanst du die Pdf Datein in Word Datein konvertieren, und dann kannst du mit der Datein alles machen, so, auch di Formular ausfÜllen, usw.LG und schÓnen Tag:Raymond""",
                        user=ting)

    clements_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=ting))

    comment1 = Comment(text=u"""Hallo Rena!

Tja, das Problem ist bei dieser Sache nur, daß diese Umwandlung ja auch
nur die Textvariante des Formulars ist und damit nicht die
Formularfelder selbst enthalten sind. Ich dachte an eine Möglichkeit,
direkt PDFs zu bearbeiten bzw. deren Formularfelder auszufüllen. Mit dem
Adobe Reader soll das ja möglich sein, allerdings hatte ich gehofft, daß
es noch eine andere Möglichkeit gibt. Trotzdem danke.
--
Ciao!

Cassie""", user=clements)

    comment2 = Comment(text=u"""moinsen Cassie,

ist zwar unconventionell, hat bei mir aber mal funktioniert. sofern diepdf
datei mit o c r erstellt wurde, kann man sie komplett markieren und dann die
gesamte datei in die zwischenablage kopieren, dann in eine Word datei
einfügen und alles ausfüllen. danach muss man halt den krams nochmals
ausdrucken und wieder einscannen, ums ausgefüllt zurückzusenden. dabei kann
es auch passieren, das ein din a4 blatt dann um einige zeilen größer wurde
und es halt dann mehrere seiten als vorher sind. alles recht umständlich,
aber immerhin zweckdienlich. ist mit sicherheit keine professionelle lösung,
aber ne lösung des problems. geht aber nur dann, wie gesagt, wenn die pdf
mit OCR erstellt wurde, sonst kann man sie erst gar nicht markieren und
somit auch nicht kopieren. viel glück wünscht

Francesca """, user=forster, parent=comment1)

    comment3 = Comment(text=u"""Hallo Francesca!

Tja, ich weiß leider nicht, ob diese PDF mit einer OCR erstellt wurde.
Es ist ein Formular von der Stadt. Aber ich werde deinen Trick trotzdem
mal ausprobieren, vielen Dank.
--
Ciao!

Cassie""", user=clements, parent=comment2)

    comment4 = Comment(text=u"""Hallo Cassie,
ich versteh grad überhaupt nicht, wo du das Problem hast. Wenn es
normale ausfüllbare PDF-Dateien sind, dann öffnest du die im Adobe
Reader und der Text wird dir angezeigt und die Felder sollten (a8ßer,
wenn du Virgo benutzt...) normalerweise von deinem Screenreader auch
entsprechend auszulesen und auch ausfüllbar sein. Das einzige, was
leider nicht geht im Reader ist, dieses ausgefüllte Formular dann
abzuspeichern, sodass du es dann also ausdrucken musst und dann - in dem
Falle - entweder der Stadt per Post schicken musst oder zum Amt zum
abgeben. Oder verstehe ich dich komplett falsch? Ich würde aber auf
keinen Fall herumexperimentieren mit irgendwelchen Kopier- oder
OCR-Aktionen, dann ist das ganze nämlich nicht mehr dokumentensicher und
damit ungültig! Wie gesagt: Wenn ich dich falsch verstanden habe, aber -
wahrscheinlich weil ich dich falsch verstanden habe - verstehe ich dein
Problem nicht.

Viele Grüße
Francesca""", user=ruff)

    comment5 = Comment(text=u"""Hallo Francesca!

Mein Problem ist, daß ich den Adobe Reader einfach nicht leiden kann und
ihn auch nicht auf meinem System haben möchte. Ich hatte es irgendwann
nochmal versucht, aber da dieses Ding sich ziemlich ins System festsetzt
und so viele weitere Komponenten installieren will, hab ich das
irgendwann gelassen. Deshalb suche ich eigentlich schon seit einiger
Zeit eine gute Alternative zu Adobe und nun halt eben auch eine gute
Software zum Bearbeiten von PDF-Formularen.
--
Ciao!

Cassie""", user=clements)

    clements_problem.comments.extend([comment1, comment2, comment3, comment4, comment5])

    isham_problem = Problem(title=u"""Paßt mein JAWS zu Windows 7 ?""", description=u"""Orakel, oh Orakel !

Wieder näher ich mich dir. In der Linken ein Räucherstäbchen, und rechts ne
Frage und erhoffe Weisheit aus dem Internetz.

Habe mich zu Windows 7 entschlossen. Meine Frage ist nun, ob mein JAWS 10.0
dazu paßt? Hat jemand diese Kombi und es funktioniert ? Oder wo kann ich das
mal nachfragen, nachgucken?

Herzlichen Gruß euch allen von Ivy""", user=isham, tags=[reader_jaws, windows7])

    solution1 = Solution(text=u"""Guten Morgen Ivy,

nein, leider wird das nicht mit JAWS 10 klappen. Besser wäre da JAWS 13 oder 14. Dann mußt du darauf achten, ob du Windows 7 32-Bit oder 64-Bit hast.

Liebe Grüße

Bernarda""", user=beadles)

    isham_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=beadles))

    riggle_problem = Problem(title=u"""Braillezeile Baum VArio model Amica, läuft under Windows 7 64 bits?""", description=u"""Hallo zusammen,es ist nicht fÜr mich, ich benutze die Braillezielen von Handy Tech, aber fÜr eine Freundin von mir. Als im Betreff steht, möchte sie wissen ob die Braillezeile Baum Vario Amica (gekauft vonr 7 Jahren) ) läuftunter Windows 7 64 bits.Als mit Baum habe ich keine Erfahrung, stelle ich an euch die Frage, die mehr darÜber weißt.Wie immer, vielen Dank und gute Nacht:Raymond
""", user=riggle, tags=[iso_22_36_03, windows7])

    solution1 = Solution(text=u"""Hallo Rena,
Ja, die BAUM Vario 40 oder Vario 80 läuft auch unter windows 64-Bit,
allerdings nur unter zwei voraussetzungen:

1. Da die Braillezeile eine serielle Schnittstelle hat, benötigt ihr
vermutlich einen Seriell zu USb Wandler.
Und für den benötigt ihr dann natürlich auch 64-bit Treiber.

2. Für JAWS Versionen 11 und höher gibt es leider keine Treiber für diese
Braillezeile.

In diesem Fall einfach auf einen anderen Screenreader (COBRA, Window eyes
o.ä.) ausweichen.

Mfg
Bell""", user=burruel)

    riggle_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=burruel))

    drovin_problem = Problem(title=u"""Routenplaner bei Google-Maps nicht mehr Da!""", description=u"""Hallo liebe Nutzer von outenplaner-Online-Nachschlage-Werke
Ich hab bei Routen-Planer immer die Sache MapsGoogle genutzt, die wahr
mit NVDA ja so bedienbar, jetzt wollte ich für einen Bekannten eine
Route dort nachschlagen, u. hab bemerkt, dasss es Maps bei google.de
nicht mehr gibt, hab eine AlternativenRoutenplaner von Falk gefunden,
dieser ging auch mit NVDa zu bedienen, zu mindest soweit, dass ich die
Strecke finden konte, hat Jemand Ahnung, mit den Planer von Falk, oder
benutzt  ihr was Anderes!

Hier ist der Routenplaner von Falk:
http://www.falk.de/routenplaner

Hab aber noch nicht raus bekommen, ob er sich für die Fussplanung
eignet, vieleicht hat ja hier auch jemand Lösung?

Vielen Dank im Voraus!

Lg. Daneen""", user=drovin, tags=[content_type_websites])

    solution1 = Solution(text=u"""Hallo Daneen und andere,

> Routenplaner bei Google-Maps nicht mehr Da!
Doch er ist noch da!

Wenn Du eine Suche Startes im nächten Ergebnisfenster findest Du ihn.
Suche nach dem Link Maps.

Warum auch immer sie das gemacht haben?

Gruß Bernarda
""", user=beadles)

    drovin_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=beadles))

    iler_problem = Problem(title=u"""Internet Explorer 11 wieder rückgängig machen""", description=u"""Hallo zusammen,

für diejenigen bei denen der Windows-Update automatisch den Internet
Explorer 11 auf den Rechner installiert hat, kann ich folgenden Tipp
geben, wie man es wieder rückgängig macht.
Habe es soeben bei mir mit Erfolg ausprobiert, da der IE11 nicht bei
allen Screenreadern funktioniert.

Allerdings gilt dies nur für Windows 7.

Unter Windows 7 wird der IE als automatisches Update ausgerollt,
sofern dies nicht blockiert ist. Wollt Ihr den neuen Browser
unter Windows 7 wieder deinstallieren, geht Ihr wie folgt vor:

Im Suchfeld des Startmenüs "Installierte Updates" eingeben
und dann "Installierte Updates anzeigen" auswählen.
Je nach System kann es durchaus etwas dauern, bis die Liste
vollständig angezeigt wird.
Dort wird auch der "Internet Explorer 11" unter den Windows-Updates gelistet.
Also mit Cursor-runter dorthin wandern.

Dann Klickt Ihr mit der rechten Maustaste auf den Eintrag und wählt
Deinstallieren. Die danach folgende Abfrage beantwortet Ihr mit Ja.

Das Deinstallieren dauert je nach System durchaus seine Zeit,
Danach will das System einen Neustart haben.
Also vorher noch alle Anwendungen schliessen.

nach erfolgtem Neustart könnt Ihr wieder mit dem Internet Explorer 10
oder einer vorherigen Version arbeiten.

Viel Erfolg und Grüße

Ivonne :-)""", user=iler, tags=[browser_internet_explorer_11, browser_internet_explorer_10, windows7, reader_cobra])

    comment1 = Comment(text=u"""Hallo Ivonne,

für welche Screemreader gilt dies denn?

Mfg. Daneen""", user=edman)

    comment2 = Comment(text=u"""Hallo Daneen,

es handelt sich bei mir um den Screenreader Cobra, der noch nicht für den IE11
fit ist.
Ich vermute, dass auch die anderen Screenreader wie Jaws, NVDA oder wie sie alle
haeißen, auch Schwierigkeiten haben werden.

LG Ivonne""", user=iler, parent=comment2)

    comment3 = Comment(text=u"""
    Hallo Ivonne,

danke für Info, hast du denn auch eine Alternativ-Browser-Lösung, da ja
nicht Alle-Prowser bedienbar sind!

Ich bin jetzt kein Baum-Benutzer, sondern mein
Kommanzieller-Screemraider ist Jaws, aber ich bnutze auch NVDa, u. mit
dem, meistens Faierfox, was aber mit Faierfox u. Jaws zwar geht, aber
nur bediengt, ich kann dir in Moment, leider nicht  nicht sagen, ob ich
auch die Probleme mit dem Neuen-Internet-Browser von Microsoft hab, da
ich in noch am Fertigstellen des Neuen-Schlepptops bin, u. ich mir erst
die Jaws-Schlüssel freistellen muss, u. Schleppto-System bzw auch
Betriebssystem sichern muss, aber erst wenn der
Kommanzielle-Sxreemraider freigeschalten ist, u. ich ihn erst dann
installiere, kann ich dir Antwort geben!##Bis dem nächst, mfg. Daneen""", user=edman, parent=comment3)

    comment4 = Comment(text=u"""Hallo Daneen,
ich benutze nur den Internet Explorer, weil ich auch auf der Arbeit damit
arbeite.
Zurzeit verwende ich den IE10.
Der lässt sich mit Cobra gut bedienen.

LG Ivonne""", user=iler)

    iler_problem.comments.extend([comment1, comment2, comment3, comment4])

    ruff_problem = Problem(title=u"""Mehrere computertechnische Fragen""", description=u"""Hallo,

hier ein paar Fragen zu meinen Ultrabookproblemen:
1. Welche Tastenkombi muss ich in Windows Live Mail drücken, um eine
Lesebestätigung vom Mailempfänger anzufordern? Also ich möchte mir nicht die
Nachrichtenregel einstellen, dass immer immer meine Mails bestätigt werden
sondern ich würde das gerne immer nur dann, wenn ich es für nötig empfinde
einstellen.
2. Wenn ich die linke Großschreibtaste drücke, kommt jedes Mal, wenn ich z.
B. die Uhrzeit abfragen möchte ein Piepgereusch. Wie kann ich dies
ausschalten?
3. Wenn ich eine geöffnete Mail oder eine Internetseite habe wo ich nur
einen Teil vom Text kopieren möchte, dann geht dies nicht, wenn ich Strg.
+linke Shift und Pfeil rechts drücke. JAWS sagt dann: "Nichts markiert."
Wenn ich aber eine geöffnete Mail habe und auf beantworten gehe, dann kann
ich ohne Probleme den Text kopieren. Was muss ich einstellen, damit ich ohne
Probleme Texte kopieren kann?

Ich arbeite mit Windows 7, Mozilla Firefox und Windows Live Mail. Als
Screenreader nutze ich JAWS.

Vielen Dank schon mal für eure Hilfe und liebe Grüße

Raymond""", user=ruff, tags=[windows7, browser_firefox, app_windows_live_mail])

    solution1 = Solution(text=u"""Hallo Raymond,

ich kann Dir nur die Frage 1 beantworten:

Mit Alt+m ins Menü "Nachrichten"
Dann mit Alt+z ins Untermenü "Zustellung"
Dann mit Alt+l ins Untermenü von Zustellung den Eintrag Lesebestätigung
anklicken oder mit Enter aktivieren

Dies gilt immer nur für eine EMail-Sitzung.
Also musst Du dies bei der nächsten Mail alles wiederholen, wenn diese auch eine
Lesebestätigung haen soll.

Spätabendliche Grüße

Ivonne :-)""", user=iler)

    comment1 = Comment(text=u"""Hallo Ivonne,
vielen Dank dir für deine ausführliche Mail und Hilfe. Jetzt ist eine Frage
schon geklährt ;:-).
LG, Raymond """, user=ruff)

    ruff_problem.comments.extend([comment1])
    ruff_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=iler))

    chappell_problem = Problem(title=u"""Zatto ist mit NVDa nicht bedienbar, gibt es eine Alternative""", description=u"""Hallo liebe Nutzer von Fernseh-Geschichtemn im Internet,

ich hab jetzt seit langer Zeit mal mir die Software von Zatto
runtergelutscht, u. wollte die mit NVDa bedienen, aber pech, da ging
nichts zu bedienen, gibt es eine Alternative-Geschichte zu Satto?

Ich hab auch keinen Plan, ob es jemals mit einen Screemraider bedienbar
wahr, wäre dies bezüglich auch an Infos aus!

Danke im Voraus, mfg. Daneen.""", user=chappell, tags=[content_type_websites, reader_nonvisual_desktop_access_nvda])

    solution1 = Solution(text=u"""Ja, die Zattoo Webseite. ;-) Soweit ich mich erinnern kann, hatte man
da, wenn man eingeloggt war, die ganzen Sender irgendwo untereinander
stehen als Links. Ich weiß das allerdings nicht, ob das immernoch so
ist, da ich schon seit Ewigkeiten Zattoo nicht mehr genutzt habe. Die
Software damals war zwar an sich bedienbar, das Problem war nur, dass
man zwar mit den Cursortasten durch die Senderliste fahren konnte,
dieser Sender dann aber sofort abgespielt wurde und der Rechner
abstürzte, wenn ich dann schnell weiterfuhr, um einen anderen Sender zu
suchen. Dann hörte ich gar nix mehr. Aber, wie gesagt, schau dir deine
persönliche Webseite da mal genauer an, wenn du dich mit Usernamen und
Passwort einloggst.""", user=clements)

    comment1 = Comment(text=u"""Hallo Francesca,

herzlichen Dank für deine Info!

Hast du da vieleicht eine Andere-Alternativezu Zatto, die mit
Screemreader bedienbar wäre?""", user=chappell)

    chappell_problem.comments.extend([comment1])
    chappell_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=clements))


    ingles_problem = Problem(title=u"""Falsche Jaws/Version freigeschalten, da ich die Autorisierungs/CD von Alter/Version genommen hab!""", description=u"""Hallo liebe Jaws/Nutzer von Jaws,

ich hab folgendes Problem:

ich hab ein Jaws von Version 12 installiert, aber mit
AlteAutorisierungs-CD (meine von Älteren-Jaws-Version) ausversehen
genommen, jetzt hab ich versucht, über Programme bei Jaws die
Autoresierung bzw. Freischaltung zurück zu setzen, u. anschließend eine
Deinstallations-Vorgang zu starten, aber ohne erfolg, da bei der
Neu-Installation von Jaws mit richtiger-Autorisierungs-Cd, von Jaws
immer erkannt wird, dass diese Version mit Autoresierungs-Cd nicht
stimmt, u.

Kann mir Jemand helfen, was ich zu tun hab, dass die Alte-Version von
Jaws sauber deinstalliert wird, u.ich die Neue-Jaws-Version sauber
installiern kann!

Weis jetzt auch nicht, ob ich hier in der Liste richtig bin, da es ja
eine Liste für Jaws gibt, hab jetzt aber keinen Plan, wo ich mich zwecks
Liste anmelden muss (meine Jaws-Liste)!

Vielen Dank für Info u. Hilfe im Voraus, mfg. Daneen!""", user=ingles, tags=[reader_nonvisual_desktop_access_nvda])

    solution1 = Solution(text=u"""Da Du nicht geschrieben hast mit welcher Windowsversion Du arbeitest ist das
jetzt mit dem exakten Pfad nicht ganz einfach.
Bei Win7 64 Bit:
"\Program Files\Freedom Scientific\Authorization" In diesem Ordner die
JAWS.cps löschen.

VG Candance""", user=chappell)

    ingles_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=chappell))

    # rossiter_problem = Problem(title=u"""""", description=u"""""", user=rossiter, tags=[])
    burruel_problem = Problem(title=u"""Contacts on the Epic with 2.2.""", description=u"""It is not possible to add a new contact from the stock call log. The stock call log includes an option for adding an entry to contacts by hitting the menu key and choosing add to contacts. This places you in a list of your existing contacts. The next logical step is to arrow to a New Contact button, click it, and start an entry with the phone number from the call log. However, this is the step that is inaccessible as there is no New Contact button that can be reached with the arrow keys. Navigating above the contact list takes you to a search box, where you type the name of a contact you are looking for, and moving to the bottom of the list doesn't produce anything either. So your only real option is to add the number to an existing contact. """, user=burruel, tags=[android22])

    solution1 = Solution(text=u"""Download an app called Go SMS by Go Dev Team. It is available from the Android Market and is free to use.

Go SMS is almost identical to the stock messaging app. There is an unlabeled button at the top of the screen; clicking on it allows you to compose a new message. The typing boxes have labels and work normally.

When you launch go sms, focus is in the box where you specify who the message will go to. In this case, you can type the first few letters and use your arrows to scroll through a list of matches. Just hit enter when you find the contact you want, and the text is filled into the message box.

After you finish filling in the recipient, hit the down arrow to get into the message body and type your text. This behaves appropriately as well. Then to send the message, hit the right arrow from here to move to an unlabeled button, which sends the message as it does in the stock messaging app.

Other areas of Go SMS also behave similarly to the stock messaging app. For instance, when you launch Go SMS and use your arrows, you can browse through your conversations with friends. Clicking on a conversation allows you to either send a message to that person or reread all of the prior messages in the thread. There are no unusual quirks here.

Note: If you open the Go SMS from a status bar notification, the New Text Message alert does not clear automatically. To clear it, you must either go into the stock messenger or clear the notification manually from the status bar.
""", user=iler)

    burruel_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=iler))

    edman_problem = Problem(title=u"""caller ID on the Epic in 2.2 """, description=u"""The option to have caller ID spoken is no longer available. Samsung has removed it. I consider this one of the most annoying accessibility regressions of the Epic. Previously, I could have the Epic announce the name of the caller in place of the ring tone itself. """, user=edman, tags=[android22])

    solution1 = Solution(text=u"""Download an app called Call Announcer by Codean software. It's available from the Android Market in both free and licensed versions; I'm using the free version.

After Call Announcer is installed, I simply go into the app, and check the setting Enable Callback Announcer. This lets the app speak the name of the caller over the ringtone. A quieter ringtone works better; otherwise, the voice may be drowned out by the tone itself. I use the system default which is called Luminescence. Note that, if you put the ringtone to silent, the free version of Call Announcer does not speak the caller ID information. """, user=iler)

    edman_problem.problem_solutions_association.append(ProblemSolutionProposal(solution=solution1, user=iler))
    # nadal_problem = Problem(title=u"""""", description=u"""""", user=nadal, tags=[])
    # melle_problem = Problem(title=u"""""", description=u"""""", user=melle, tags=[])



    DBSession.add_all([ting_problem, koepp_problem, auvil_problem, rempe_problem, easterday_problem,
                       whitehouse_problem, beadles_problem, forster_problem, clements_problem,
                       isham_problem, riggle_problem, drovin_problem, iler_problem, ruff_problem, chappell_problem,
                       ingles_problem, burruel_problem, edman_problem])
    transaction.commit()

    # Automatically create Topics based on the root tags
    for root in Tag.get_trees():
        DBSession.add(Topic(name=root.name, description=root.description, tags=[root]))
    transaction.commit()


if __name__ == "__main__":
    main()