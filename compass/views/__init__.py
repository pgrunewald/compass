from compass import RootFactory

__author__ = 'user'
__all__ = ['tags', 'problems']

from pyramid.view import view_config
from pyramid.httpexceptions import HTTPClientError, HTTPFound
from compass.models import DBSession


@view_config(context=AssertionError)
@view_config(context=ValueError)
def failed_validation(exc, request):
    """
    This view catches input errors and returns it as 400.
    """
    msg = exc.args[0] if exc.args else ""
    DBSession.rollback()
    return HTTPClientError(explanation=msg)


@view_config(context=RootFactory)
def create_suggestions(context, request):
    return HTTPFound('static/compass/index.html')
