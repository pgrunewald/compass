from pyramid.view import view_config
from pyramid.httpexceptions import HTTPCreated, HTTPOk
from compass.models import DBSession

from compass.security import get_authenticated_user
from compass.resources import (
    ISearchableResource
)


@view_config(context=ISearchableResource, request_method='POST', accept='application/json', renderer='json',
             permission='read')
def search_resource(context, request):
    return context.search(request)