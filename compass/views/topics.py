from pyramid.view import view_config
from pyramid.httpexceptions import HTTPCreated, HTTPOk

from compass.resources import (
    TopicCollection,
    TopicResource,
    TopicTagCollection,
    TopicTagResource,
    NotYetBoundTopicTagResource,
)


@view_config(context=TopicCollection, request_method='GET', renderer='json', permission='read')
def read_topic_collection(context, request):
    return context.all()


@view_config(context=TopicCollection, request_method='POST', accept='application/json', permission='create')
def create_topic_in_collection(context, request):
    for needed_param in ['name', 'description', 'tags']:
        if not request.json_body.get(needed_param):
            raise ValueError(u"Missing parameter: {}".format(needed_param))
    new_res = context.create(dict(name=request.json_body['name'], description=request.json_body['description'],
                                  tags=request.json_body.get('tags')))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=TopicResource, request_method='GET', renderer='json', permission='read')
def topics_show_single(context, request):
    return context.obj


@view_config(context=TopicResource, request_method='PUT', accept='application/json', permission='update')
def update_topic_in_collection(context, request):
    context.update(request)
    return HTTPOk()


@view_config(context=TopicResource, request_method='DELETE', permission='delete')
def delete_topic_in_collection(context, request):
    context.delete()
    return HTTPOk()


@view_config(context=TopicTagCollection, request_method='GET', renderer='json', permission='read')
def read_tags_from_topic(context, request):
    return context.all()


@view_config(context=TopicTagCollection, request_method='DELETE', permission='delete')
def delete_tags_from_topic(context, request):
    context.delete()
    return HTTPOk()


@view_config(context=TopicTagResource, request_method='GET', renderer='json', permission='read')
def read_tag_from_topic(context, request):
    return context.obj


@view_config(context=TopicTagResource, request_method='PUT', renderer='json', permission='create')
def bind_erroneously_same_tag_on_topic(context, request):
    raise ValueError(u"Tag '{}' is already bound.".format(request.traversed[-1]))


@view_config(context=NotYetBoundTopicTagResource, request_method='POST', renderer='json', permission='create')
def bind_existing_tag(context, request):
    new_res = context.bind(tag_name=request.traversed[-1])
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=NotYetBoundTopicTagResource, request_method='PUT', renderer='json', permission='create')
def bind_existing_tag_with_put(context, request):
    new_res = context.bind(tag_name=request.traversed[-1])
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=TopicTagResource, request_method='DELETE', renderer='json', permission='delete')
def delete_tag_from_topic(context, request):
    context.unbind(tag_name=request.traversed[-1])
    return HTTPOk()