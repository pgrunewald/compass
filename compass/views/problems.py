from pyramid.view import view_config
from pyramid.httpexceptions import HTTPCreated, HTTPOk

from compass.security import get_authenticated_user
from compass.resources import (
    ProblemCollection,
    ProblemResource,
    ProblemTagCollection,
    ProblemTagResource,
    NotYetBoundProblemTagResource,
    ProblemCommentResource,
    ProblemCommentCollection,
    ProblemCommentRepliesCollection,
    ProblemSolutionCollection,
    ProblemSolutionProposalResource,
    ProblemSolutionNotProposedResource,
    SolutionFeedbackCollection,
    UserSolutionFeedbackResource,
    ProblemSuggestionsCollection)


@view_config(context=ProblemCollection, request_method='GET', renderer='json', permission='read')
def read_problem_collection(context, request):
    return context.all()


@view_config(context=ProblemCollection, request_method='POST', accept='application/json', permission='create')
def create_problem_in_collection(context, request):
    for needed_param in ['title', 'description', 'tags']:
        if not request.json_body.get(needed_param):
            raise ValueError(u"Missing parameter: {}".format(needed_param))
    new_res = context.create(dict(title=request.json_body['title'], description=request.json_body['description'],
                                  tags=request.json_body.get('tags'), user=get_authenticated_user(request)))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=ProblemResource, request_method='GET', renderer='json', permission='read')
def problems_show_single(context, request):
    return context.obj


@view_config(context=ProblemResource, request_method='PUT', accept='application/json', permission='update')
def update_problem_in_collection(context, request):
    context.update(request)
    return HTTPOk()


@view_config(context=ProblemResource, request_method='DELETE', permission='delete')
def delete_problem_in_collection(context, request):
    context.delete()
    return HTTPOk()


@view_config(context=ProblemTagCollection, request_method='GET', renderer='json', permission='read')
def read_tags_from_problem(context, request):
    return context.all()


@view_config(context=ProblemTagCollection, request_method='DELETE', permission='delete')
def delete_tags_from_problem(context, request):
    context.delete()
    return HTTPOk()


@view_config(context=ProblemTagResource, request_method='GET', renderer='json', permission='read')
def read_tag_from_problem(context, request):
    return context.obj


@view_config(context=ProblemTagResource, request_method='PUT', renderer='json', permission='create')
def bind_erroneously_same_tag_on_problem(context, request):
    raise ValueError(u"Tag '{}' is already bound.".format(request.traversed[-1]))


@view_config(context=NotYetBoundProblemTagResource, request_method='POST', renderer='json', permission='create')
def bind_existing_tag(context, request):
    new_res = context.bind(tag_name=request.traversed[-1])
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=NotYetBoundProblemTagResource, request_method='PUT', renderer='json', permission='create')
def bind_existing_tag_with_put(context, request):
    new_res = context.bind(tag_name=request.traversed[-1])
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=ProblemTagResource, request_method='DELETE', renderer='json', permission='delete')
def delete_tag_from_problem(context, request):
    context.unbind(tag_name=request.traversed[-1])
    return HTTPOk()


@view_config(context=ProblemCommentCollection, request_method='GET', renderer='json', permission='read')
def read_comments_from_problem(context, request):
    return context.all()


@view_config(context=ProblemCommentCollection, request_method='POST', accept='application/json', permission='create')
def create_comment_in_collection(context, request):
    for needed_param in ['text']:
        if not request.json_body.get(needed_param):
            raise ValueError(u"Missing parameter: {}".format(needed_param))
    new_res = context.create(dict(text=request.json_body['text'], user=get_authenticated_user(request)))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=ProblemCommentResource, request_method='GET', renderer='json', permission='read')
def read_comment_from_problem(context, request):
    return context.obj


@view_config(context=ProblemCommentResource, request_method='PUT', renderer='json', permission='update')
def update_comment_from_problem(context, request):
    context.update(request)
    return HTTPOk()


@view_config(context=ProblemCommentResource, request_method='DELETE', renderer='json', permission='delete')
def delete_comment_from_problem(context, request):
    context.delete()
    return HTTPOk()


@view_config(context=ProblemCommentRepliesCollection, request_method='GET', renderer='json', permission='read')
def read_comment_replies_from_problem(context, request):
    return context.all()
    return HTTPOk()


@view_config(context=ProblemCommentRepliesCollection, request_method='POST', renderer='json', permission='create')
def create_comment_reply_from_problem(context, request):
    for needed_param in ['text']:
        if not request.json_body.get(needed_param):
            raise ValueError(u"Missing parameter: {}".format(needed_param))
    new_res = context.create(dict(text=request.json_body['text'], user=get_authenticated_user(request)))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=ProblemSolutionCollection, request_method='GET', renderer='json', permission='read')
def read_solutions_from_collection(context, request):
    return context.all()


@view_config(context=ProblemSolutionCollection, request_method='POST', renderer='json', permission='create')
def create_and_propose_solution_in_collection(context, request):
    new_res = context.create_and_propose(dict(text=request.json_body['text'], user=get_authenticated_user(request)))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=ProblemSolutionNotProposedResource, request_method='PUT', renderer='json', permission='create')
def propose_existing_solution_for_problem(context, request):
    context.propose(solution_id=request.traversed[-1], user=get_authenticated_user(request))
    return HTTPOk()


@view_config(context=ProblemSolutionProposalResource, request_method='GET', renderer='json', permission='read')
def read_proposed_solution_from_problem(context, request):
    # Expose only the solution of this association object
    return context.obj.solution


@view_config(context=ProblemSolutionProposalResource, request_method='PUT', renderer='json', permission='create')
def propose_already_proposed_solution(context, request):
    # This is a fallback to respond with OK, even if the same solution has been proposed already.
    return HTTPOk()


@view_config(context=ProblemSolutionProposalResource, request_method='DELETE', renderer='json', permission='delete')
def revoke_solution_proposal_for_problem(context, request):
    context.revoke()
    return HTTPOk()


@view_config(context=SolutionFeedbackCollection, request_method='GET', renderer='json', permission='read')
def read_all_solution_feedback(context, request):
    return context.show_stats()


@view_config(context=SolutionFeedbackCollection, request_method='POST', renderer='json', permission='create')
def create_or_update_solution_feedback(context, request):
    new_res = context.vote(data=request.json_body, user=get_authenticated_user(request))
    return HTTPCreated(request.resource_url(new_res))

@view_config(context=UserSolutionFeedbackResource, request_method='GET', renderer='json', permission='read')
def read_user_feedback(context, request):
    return context.get_user_feedback()


@view_config(context=UserSolutionFeedbackResource, request_method='PUT', renderer='json', permission='create')
def update_solution_feedback(context, request):
    context.parent.vote(data=request.json_body, user=get_authenticated_user(request))
    return HTTPOk()


@view_config(context=UserSolutionFeedbackResource, request_method='DELETE', renderer='json', permission='delete')
def delete_all_solution_feedback(context, request):
    context.delete_user_feedback()
    return HTTPOk()


@view_config(context=ProblemSuggestionsCollection, request_method='POST', accept='application/json',
             renderer='json', permission='read')
def create_suggestions(context, request):
    return context.retrieve_similar_problems(request)
