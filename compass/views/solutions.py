from pyramid.view import view_config
from pyramid.httpexceptions import HTTPOk

from compass.security import get_authenticated_user
from compass.resources import (
    SolutionCollection,
    SolutionResource)

@view_config(context=SolutionCollection, request_method='GET', renderer='json', permission='read')
def read_solution_collection(context, request):
    return context.all()


@view_config(context=SolutionResource, request_method='GET', renderer='json', permission='read')
def read_solution_from_collection(context, request):
    return context.obj


@view_config(context=SolutionResource, request_method='PUT', accept='application/json', permission='update')
def update_problem_in_collection(context, request):
    context.update(request)
    return HTTPOk()

@view_config(context=SolutionResource, request_method='DELETE', accept='application/json', permission='delete')
def delete_problem_in_collection(context, request):
    context.delete()
    return HTTPOk()
