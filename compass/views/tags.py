from pyramid.view import view_config
from pyramid.httpexceptions import HTTPCreated, HTTPOk
from compass.resources import TagCollection, TagResource, TagNotFoundResource, TagChildrenCollection


@view_config(context=TagCollection, request_method='GET', renderer='json', permission='read')
def read_tag_collection(context, request):
    return context.all(request)


@view_config(context=TagCollection, request_method='POST', accept='application/json', permission='create')
def create_tag_in_collection(context, request):
    new_res = context.create(name=request.json_body.get('name'), description=request.json_body.get('description'))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=TagResource, request_method='GET', renderer='json', permission='read')
def read_tag(context, request):
    return context.obj


@view_config(context=TagNotFoundResource, request_method='POST', renderer='json', permission='create')
def create_new_tag(context, request):
    new_res = context.parent.create(name=request.traversed[-1], description=request.json_body.get('description'))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=TagNotFoundResource, request_method='PUT', renderer='json', permission='create')
def create_not_found_tag_with_put(context, request):
    new_res = context.parent.create(name=request.traversed[-1], description=request.json_body.get('description'))
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=TagResource, request_method='PUT', renderer='json', permission='update')
def update_tag(context, request):
    context.update(name=request.json_body.get('name'), description=request.json_body.get('description'))


@view_config(context=TagResource, request_method='DELETE', permission='delete')
def tags_delete_single(context, request):
    context.delete()
    return HTTPOk()

@view_config(context=TagChildrenCollection, request_method='GET', renderer='json', permission='read')
def read_tag_children(context, request):
    return context.all()

@view_config(context=TagChildrenCollection, request_method='POST', accept='application/json', permission='create')
def create_tag_children_in_collection(context, request):
    new_res = context.create(name=request.json_body.get('name'), description=request.json_body.get('description'))
    return HTTPCreated(request.resource_url(new_res))