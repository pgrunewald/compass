from pyramid.view import view_config
from pyramid.httpexceptions import HTTPCreated, HTTPOk, HTTPClientError
from compass.models import DBSession

from compass.security import get_authenticated_user
from compass.resources import (
    UserStatusResource,
    UserResource,
    UserCollection,
    SolutionsFromUserResource,
    ProblemsFromUserResource,
)


@view_config(context=UserCollection, request_method='GET', renderer='json', permission='read')
def read_user_collection(context, request):
    return context.all()


@view_config(context=UserCollection, request_method='POST', accept='application/json', permission='create')
def create_user_from_collection(context, request):
    for needed_param in ['username', 'email', 'email_repeated', 'password', 'password_repeated']:
        if not request.json_body.get(needed_param):
            raise ValueError(u"Missing parameter: {}".format(needed_param))
    new_res = context.create(username=request.json_body['username'],
                             email=request.json_body['email'],
                             email_repeated=request.json_body['email_repeated'],
                             password=request.json_body['password'],
                             password_repeated=request.json_body['password_repeated'])
    return HTTPCreated(request.resource_url(new_res))


@view_config(context=UserResource, request_method='GET', renderer='json', permission='read')
def read_user_from_collection(context, request):
    return context.obj


@view_config(context=UserResource, request_method='PUT', renderer='json', permission='update')
def update_user_in_collection(context, request):
    return context.update(request)


@view_config(context=ProblemsFromUserResource, request_method='GET', renderer='json', permission='read')
@view_config(context=SolutionsFromUserResource, request_method='GET', renderer='json', permission='read')
def read_user_produced_collection(context, request):
    return context.all()


@view_config(context=UserStatusResource, request_method='GET', renderer='json', permission='read')
def read_user_status(context, request):
    return context.get_status(request)
