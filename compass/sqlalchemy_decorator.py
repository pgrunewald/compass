from sqlalchemy import DateTime
from sqlalchemy import TypeDecorator
from sqlalchemy.dialects.sqlite.pysqlite import SQLiteDialect_pysqlite

import pytz


class UTCDateTime(TypeDecorator):
    """Makes sure, that only UTC-based DateTimes are saved."""

    impl = DateTime

    def process_bind_param(self, value, dialect):
        """Way-in processing"""
        # pass-through NULL values
        if not value:
            return value
        # No time-zone given? Assume UTC then
        if not value.tzinfo:
            return value.replace(tzinfo=pytz.utc)
        else:
            # Time-zone given? Make sure to convert it to utc.
            return value.astimezone(tz=pytz.utc)

    def process_result_value(self, value, dialect):
        """Way-out processing only for databases backends known for unsufficient time zone support"""
        if value and isinstance(dialect, (SQLiteDialect_pysqlite,)):
            return value.replace(tzinfo=pytz.utc)
        else:
            return value