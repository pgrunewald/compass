# coding=utf-8
import base64
import os
from collections import defaultdict
import unittest
import transaction
from sqlalchemy.orm import exc
from sqlalchemy import exc as core_exc
from pyramid import testing
from pyramid.paster import get_appsettings
from compass import main
import compass

compass_path = os.path.dirname(compass.__file__)

from compass.models import (
    DBSession,
    Base,
    Problem,
    Tag,
    Comment,
    Solution,
    FeedbackType,
    SolutionFeedback,
    ProblemSolutionProposal,
    SolutionUsefulness,
    User,
    Topic
)


def setup_dummy_data():
    from helper import utcnow
    from compass.security import create_password_hash_with_salt
    password_hash, salt = create_password_hash_with_salt(password='dummy-password', salt='dummy-salt')
    # Setup dummy data
    with transaction.manager:

        admin = User(username=u'admin', email=u'admin@mail.com', password_hash=password_hash, password_salt=salt)
        bob = User(username=u'bob', email=u'bob@mail.com', password_hash=password_hash, password_salt=salt)
        alice = User(username=u'alice', email=u'alice@mail.com', password_hash=password_hash, password_salt=salt)
        mary = User(username=u'mary', email=u'mary@mail.com', password_hash=password_hash, password_salt=salt)
        pete = User(username=u'pete', email=u'pete@mail.com', password_hash=password_hash, password_salt=salt)

        foo_tag = Tag(u'foo')
        bar_tag = Tag(u'bar')
        umlaut_tag = Tag(u'äöü')

        problem1 = Problem(title=u'Problemtitel fuer foo', description=u'Beschreibung fuer Problem foo',
                           created=utcnow(), tags=[foo_tag], user=bob)
        problem2 = Problem(title=u'Problemtitel fuer bar', description=u'Beschreibung fuer Problem bar',
                           created=utcnow(), tags=[bar_tag], user=alice)
        problem3 = Problem(title=u'Problemtitel fuer foo+bar', description=u'Beschreibung fuer Problem foo+bar',
                           created=utcnow(), tags=[foo_tag, bar_tag], user=mary)
        problem4 = Problem(title=u'Problemtitel mit Umlauten äöüß', description=u'Beschreibung für Problem mit Umlauten',
                           created=utcnow(), tags=[umlaut_tag], user=mary)

        comment1 = Comment(text=u'[1] Ein Kommentar fuer foo', user=alice)
        comment2 = Comment(text=u'[2] Ein weiterer Kommentar fuer foo', user=mary)
        comment3 = Comment(text=u'[3] Antwort auf Kommentar [2]', parent=comment2, user=bob)
        comment4 = Comment(text=u'[4] Antwort auf Kommentar [3]', parent=comment3, user=mary)
        comment5 = Comment(text=u'[5] Antwort auf Kommentar [1]', parent=comment2, user=bob)

        problem1.comments.extend([comment1, comment2, comment3, comment4, comment5])

        solution1 = Solution(text=u"Lösung für Problem foo", user=mary)
        solution1_assoc = ProblemSolutionProposal(solution=solution1, user=mary)
        problem1.problem_solutions_association.append(solution1_assoc)
        feedback_type1 = FeedbackType(serialization_name=u'comprehensibility', name=u"Verständlichkeit",
                                      description=u"", min_value=1, max_value=5, scope=FeedbackType.SCOPE_SOLUTION_ONLY)
        feedback_type2 = FeedbackType(serialization_name=u'speed', name=u"Schnelligkeit",
                                      description=u"", min_value=1, max_value=5, scope=FeedbackType.SCOPE_SOLUTION_ONLY)
        feedback_type3 = FeedbackType(serialization_name=u'difficulty', name=u"Schwierigkeitsgrad",
                                      description=u"", min_value=1, max_value=5, scope=FeedbackType.SCOPE_SOLUTION_ONLY)

        usefulness_type1 = FeedbackType(serialization_name=u'helpful', name=u"Hilfreich?",
                                        description=u"Ich finde dieser Lösungsvorschlag enthält hilfreiche Informationen.",
                                        min_value=0, max_value=1, scope=FeedbackType.SCOPE_SOLUTION_AND_PROBLEM)
        usefulness_type2 = FeedbackType(serialization_name=u'solved', name=u"Gelöst?",
                                        description=u"Diese Lösung hat mir geholfen dieses Problem zu lösen.",
                                        min_value=0, max_value=1, scope=FeedbackType.SCOPE_SOLUTION_AND_PROBLEM)
        usefulness_type3 = FeedbackType(serialization_name=u'appropriate', name=u"Passend?",
                                        description=u"Dieser Lösungsvorschlag passt zu diesem Problem.",
                                        min_value=0, max_value=1, scope=FeedbackType.SCOPE_SOLUTION_AND_PROBLEM)

        DBSession.add_all([feedback_type1, feedback_type2, feedback_type3])
        DBSession.add_all([usefulness_type1, usefulness_type2, usefulness_type3])

        feedback1 = SolutionFeedback(feedback_type=feedback_type1, response=5, user=bob)
        feedback2 = SolutionFeedback(feedback_type=feedback_type1, response=1, user=mary)
        solution1.feedback.extend([feedback1, feedback2])

        # Add usefulness feedback
        useful_feedback1 = SolutionUsefulness(feedback_type=usefulness_type1, response=1, user=bob)
        useful_feedback2 = SolutionUsefulness(feedback_type=usefulness_type2, response=1, user=bob)
        useful_feedback3 = SolutionUsefulness(feedback_type=usefulness_type3, response=0, user=bob)
        useful_feedback4 = SolutionUsefulness(feedback_type=usefulness_type1, response=0, user=alice)
        useful_feedback5 = SolutionUsefulness(feedback_type=usefulness_type2, response=0, user=alice)
        useful_feedback6 = SolutionUsefulness(feedback_type=usefulness_type3, response=0, user=alice)

        solution1_assoc.usefulness.extend([useful_feedback1, useful_feedback2, useful_feedback3, useful_feedback4,
                                           useful_feedback5, useful_feedback6])
        DBSession.add_all([admin, pete])
        DBSession.add_all([problem1, problem2, problem3, problem4])

        foobar_topic = Topic(name=u"Foo und Bar", description=u"Weil foo und bar häufig zusammengenannt werden,"
                                                                   u"existiert dieses Topic.", tags=[foo_tag, bar_tag])
        DBSession.add(foobar_topic)


class TestDatabase(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

        from sqlalchemy import create_engine
        # Memory database
        engine = create_engine('sqlite://') #, echo=True)
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        setup_dummy_data()

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_initialize_main_db(self):
        from compass.scripts.initializedb import main as initialize_db_main
        # Clear all tables
        for table in reversed(Base.metadata.sorted_tables):
            DBSession.execute(table.delete())
        transaction.commit()
        # Check, if the initializedb script is working properly
        initialize_db_main(['intitializedb', os.path.join(compass_path, '../', 'testing.ini')])

    def load_users(self):
        for user in DBSession.query(User).all():
            setattr(self, user.username, user)

    def test_db_problem_existence(self):
        results = DBSession.query(Problem).all()
        self.assertEqual(len(results), 4)

    def test_db_problem_nonexistence(self):
        def search_bogus():
            DBSession.query(Problem).filter(Problem.title == u'Problemtitel #9999').one()
        self.assertRaises(exc.NoResultFound, search_bogus)

    def test_search_problems_by_tag(self):
        foo_tag = DBSession.query(Tag).filter(Tag.name == u'foo').one()
        problem = DBSession.query(Problem).filter(Problem.title == u'Problemtitel fuer foo').one()
        self.assertTrue(problem in foo_tag.problems)

    def test_update_other_tags(self):
        def check_for_name(tags, name):
            for tag in tags:
                if tag.name == name:
                    return True
            return False

        results = DBSession.query(Problem).all()
        tags = results[0].tags
        foo_tag = [tag for tag in tags if tag.name == u"foo"]
        self.assertNotEqual(foo_tag, [])
        foo_tag = foo_tag[0]
        foo_tag.name = u"foo2"
        transaction.commit()
        results = DBSession.query(Problem).all()
        tags = results[0].tags

        self.assertFalse(check_for_name(tags, u"foo"))
        self.assertTrue(check_for_name(tags, u"foo2"))

    def test_time_helpers(self):
        from helper import utcnow, default_strftime
        import pytz
        t1 = utcnow()
        self.assertTrue(isinstance(t1.tzinfo, type(pytz.UTC)))

    def test_timezone_in_db(self):
        from helper import utcnow
        import pytz
        self.load_users()
        problem = Problem(title=u'UTC-Zeit?', description=u'...', created=utcnow(), user=self.bob)
        old_time = problem.created
        self.assertTrue(isinstance(problem.created.tzinfo, type(pytz.UTC)))

        DBSession.add(problem)
        transaction.commit()
        qproblem = DBSession.query(Problem).filter(Problem.title == u'UTC-Zeit?').one()
        self.assertTrue(isinstance(qproblem.created.tzinfo, type(pytz.UTC)))
        difference = old_time - qproblem.created
        self.assertLessEqual(difference.seconds, 0, u'Timestamp changed when saving in the database.')

    def test_check_modified_timestamp(self):
        import datetime
        self.load_users()
        problem = Problem(title=u'Dummy', description=u'...', user=self.bob)
        DBSession.add(problem)
        self.assertIsNone(problem.modified, None)
        DBSession.flush()
        problem.title = u'Dummy2'
        DBSession.flush()
        self.assertIsInstance(problem.modified, datetime.datetime)

    def test_serialization(self):
        values = {
            'name': u'test',
            'description': u'test',
        }
        tag = Tag(**values)
        values2 = tag.__json__(None)
        values['id'] = None
        values['children'] = []
        values['problem_count'] = 0
        self.assertEqual(values, values2)

    def test_utf8(self):
        any_tag = DBSession.query(Tag).all()[0]
        self.assertIsInstance(any_tag.name, unicode)

    def test_get_all_children(self):
        problem1 = DBSession.query(Problem).get(1)
        self.assertTrue(len(problem1.comments.all()), 5)
        for comment in problem1.comments.all():
            self.assertTrue(problem1 == comment.problem)

            for child in comment.children:
                self.assertTrue(comment == child.parent)

    def test_check_comment_relationship(self):
        problem = DBSession.query(Problem).get(1)
        children_check = {
            1: [],
            2: [3, 5],
            3: [4],
            4: [],
            5: []
        }
        for comment in problem.comments.all():
            # Use my "virtual" ids from text to identify, since I cannot rely on auto-generated ids for comparison
            extract_ids = [int(child.text[1]) for child in comment.children]
            self.assertEqual(extract_ids, children_check[comment.id], msg='Comment #%s has the wrong children: %s != %s'
                                                                          % (comment.id, str(extract_ids),
                                                                             str(children_check[comment.id])))

    def test_comment_orphans_deleted(self):
        problem_id = 1
        comments = DBSession.query(Comment).filter(Comment.problem_id == problem_id).all()
        self.assertGreater(len(comments), 0)
        DBSession.delete(DBSession.query(Problem).get(problem_id))
        DBSession.flush()
        comments = DBSession.query(Comment).filter(Comment.problem_id == problem_id).all()
        self.assertEqual(len(comments), 0)

    def test_solution_feedback(self):
        self.load_users()
        # Add solution with erroneous input
        feature = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Verständlichkeit").one()
        solution = DBSession.query(Solution).filter(Solution.text == u"Lösung für Problem foo").one()
        f1 = SolutionFeedback(response = 1, feedback_type = feature, solution = solution, user = self.alice)
        DBSession.add(f1)
        DBSession.flush()  # Should work
        f1.response = 5
        DBSession.flush()  # Should also work
        f1.response = 999
        self.assertRaises(AssertionError, lambda: DBSession.flush())  # Should NOT work

    def test_solution_get_feedback_criteria(self):
        solution = DBSession.query(Solution).get(1)
        expected_result = [(1, u'comprehensibility', 3.0),
                           (2, u'speed', None),
                           (3, u'difficulty', None)]
        self.assertEqual(set(solution.feedback_avg_criteria), set(expected_result))

    def test_solution_feedbacks(self):
        self.load_users()
        problem_bar = DBSession.query(Problem).filter(Problem.title == u"Problemtitel fuer bar").one()
        solution_bar = Solution(text=u"Lösung für Problem bar", user=self.bob)
        problem_bar.problem_solutions_association.append(ProblemSolutionProposal(solution=solution_bar, user=self.bob))
        DBSession.flush()

        expected_result = [(1, u'comprehensibility', None),
                           (2, u'speed', None),
                           (3, u'difficulty', None)]
        self.assertEqual(set(solution_bar.feedback_avg_criteria), set(expected_result))

        feature_comprehension = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Verständlichkeit").one()
        feature_speed = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Schnelligkeit").one()
        feature_difficulty = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Schwierigkeitsgrad").one()

        solution_bar.feedback.append(SolutionFeedback(response=3, feedback_type=feature_comprehension, user=self.bob))
        solution_bar.feedback.append(SolutionFeedback(response=3, feedback_type=feature_comprehension, user=self.mary))
        solution_bar.feedback.append(SolutionFeedback(response=2, feedback_type=feature_speed, user=self.bob))
        solution_bar.feedback.append(SolutionFeedback(response=5, feedback_type=feature_difficulty, user=self.mary))
        solution_bar.feedback.append(SolutionFeedback(response=3, feedback_type=feature_difficulty, user=self.bob))

        expected_result = [(1, u'comprehensibility', 3.0),
                           (2, u'speed', 2.0),
                           (3, u'difficulty', 4.0)]
        self.assertEqual(set(solution_bar.feedback_avg_criteria), set(expected_result))

    def test_usefulness_feedback(self):
        problem = DBSession.query(Problem).get(1)
        solution_id = problem.solution_proposals[0].id
        solution_assoc = problem.problem_solutions_association.filter(ProblemSolutionProposal.solution_id ==
                                                                      solution_id).one()
        expected_result = [(4, u'helpful', 0.5),
                           (5, u'solved', 0.5),
                           (6, u'appropriate', 0.0)]
        self.assertEqual(set(solution_assoc.avg_usefulness), set(expected_result))

    def test_duplicate_usefulness_feedback(self):
        self.load_users()
        problem = DBSession.query(Problem).get(1)
        solution_id = problem.solution_proposals[0].id
        solution_assoc = problem.problem_solutions_association.filter(ProblemSolutionProposal.solution_id ==
                                                                      solution_id).one()
        feature_helpful = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Hilfreich?").one()
        feature_solved = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Gelöst?").one()
        feature_appropriate = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Passend?").one()

        useful1 = SolutionUsefulness(response=1, feedback_type=feature_helpful, user=self.mary)
        useful2 = SolutionUsefulness(response=1, feedback_type=feature_solved, user=self.mary)
        useful3 = SolutionUsefulness(response=0, feedback_type=feature_appropriate, user=self.mary)
        useful4 = SolutionUsefulness(response=0, feedback_type=feature_helpful, user=self.mary)
        useful5 = SolutionUsefulness(response=0, feedback_type=feature_solved, user=self.mary)
        useful6 = SolutionUsefulness(response=0, feedback_type=feature_appropriate, user=self.mary)

        solution_assoc.usefulness.extend([useful1, useful2, useful3, useful4, useful5, useful6])
        self.assertRaises(core_exc.IntegrityError, lambda: DBSession.flush())

    def test_invalid_usefulness_feedback(self):
        self.load_users()
        problem = DBSession.query(Problem).get(1)
        solution_id = problem.solution_proposals[0].id
        solution_assoc = problem.problem_solutions_association.filter(ProblemSolutionProposal.solution_id ==
                                                                      solution_id).one()
        feature_helpful = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Hilfreich?").one()
        feature_solved = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Gelöst?").one()
        feature_appropriate = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Passend?").one()

        useful1 = SolutionUsefulness(response=1, feedback_type=feature_helpful, user=self.mary)
        useful2 = SolutionUsefulness(response=2, feedback_type=feature_solved, user=self.mary)
        useful3 = SolutionUsefulness(response=-2, feedback_type=feature_appropriate, user=self.mary)

        solution_assoc.usefulness.extend([useful1, useful2, useful3])
        self.assertRaises(AssertionError, lambda: DBSession.flush())

    def test_delete_usefulness_feedback(self):
        problem = DBSession.query(Problem).get(1)
        solution_id = problem.solution_proposals[0].id
        solution_assoc = problem.problem_solutions_association.filter(ProblemSolutionProposal.solution_id ==
                                                                      solution_id).one()
        for feedback in solution_assoc.usefulness:
            DBSession.delete(feedback)
        # Should yield empty results
        expected_result = [(4, u'helpful', None),
                           (5, u'solved', None),
                           (6, u'appropriate', None)]
        self.assertEqual(set(solution_assoc.avg_usefulness), set(expected_result))

    def test_delete_problem_and_proposed_solutions(self):
        from sqlalchemy import and_
        # Check if dependent ProblemSolutionProposal was deleted
        problem = DBSession.query(Problem).get(1)
        solution_id = problem.solution_proposals[0].id
        solution_assoc = problem.problem_solutions_association.filter(ProblemSolutionProposal.solution_id ==
                                                                      solution_id).one()
        assoc_solution_id = solution_assoc.solution_id
        assoc_problem_id = solution_assoc.problem_id

        usefulness_feedback = DBSession.query(SolutionUsefulness).filter(
            and_(SolutionUsefulness.problem_id == assoc_problem_id,
                 SolutionUsefulness.solution_id == assoc_solution_id))

        self.assertNotEqual(len(usefulness_feedback.all()), 0)

        DBSession.delete(problem)
        DBSession.flush()

        self.assertIsNone(DBSession.query(Problem).filter(Problem.id == 1).first(), u'Problem was not deleted.')
        solution_assoc2 = DBSession.query(ProblemSolutionProposal).filter(
            and_(ProblemSolutionProposal.problem_id == assoc_problem_id,
                 ProblemSolutionProposal.solution_id == assoc_solution_id))
        # Check if associated ProblemSolutionProposal was deleted on cascade
        self.assertRaises(exc.NoResultFound, lambda: solution_assoc2.one())
        # Also their respective votes
        self.assertEqual(len(usefulness_feedback.all()), 0)

    def test_delete_solution_and_associations(self):
        from sqlalchemy import and_
        # Check if dependent ProblemSolutionProposal was deleted
        problem = DBSession.query(Problem).get(1)
        solution = problem.solution_proposals[0]
        assoc_solution_id = solution.id
        assoc_problem_id = problem.id

        usefulness_feedback = DBSession.query(SolutionUsefulness).filter(
            and_(SolutionUsefulness.problem_id == assoc_problem_id,
                 SolutionUsefulness.solution_id == assoc_solution_id))

        DBSession.delete(solution)

        solution_assoc2 = DBSession.query(ProblemSolutionProposal).filter(
            and_(ProblemSolutionProposal.problem_id == assoc_problem_id,
                 ProblemSolutionProposal.solution_id == assoc_solution_id))
        # Check if associated ProblemSolutionProposal was deleted on cascade
        self.assertRaises(exc.NoResultFound, lambda: solution_assoc2.one())
        # Also their respective votes
        self.assertEqual(len(usefulness_feedback.all()), 0)

    def test_solution_duplicate_feedback(self):
        self.load_users()
        problem_bar = DBSession.query(Problem).filter(Problem.title == u"Problemtitel fuer bar").one()
        solution_bar = Solution(text=u"Lösung für Problem bar", user=self.bob)

        problem_bar.problem_solutions_association.append(ProblemSolutionProposal(solution=solution_bar, user=self.bob))
        feature_comprehension = DBSession.query(FeedbackType).filter(FeedbackType.name == u"Verständlichkeit").one()

        solution_bar.feedback.append(SolutionFeedback(response=3, feedback_type=feature_comprehension, user=self.bob))
        DBSession.flush()
        solution_bar.feedback.append(SolutionFeedback(response=5, feedback_type=feature_comprehension, user=self.bob))
        self.assertRaises(core_exc.IntegrityError, lambda: DBSession.flush())

    def test_user_register_duplicate(self):
        DBSession.add(User(username=u'bob', email=u'xyz@blup.com'))
        self.assertRaises(core_exc.IntegrityError, lambda: DBSession.flush())
        DBSession.rollback()
        DBSession.add(User(username=u'bob-again', email=u'bob@mail.com'))
        self.assertRaises(core_exc.IntegrityError, lambda: DBSession.flush())

    def test_hybrid_attributes(self):
        problem_bar = DBSession.query(Problem).filter(Problem.title == u"Problemtitel fuer bar").one()
        self.assertFalse(problem_bar.solved_for_problem_creator)

        def set_solved(problem):
            problem.solved_for_problem_creator = True

        self.assertRaises(AttributeError, set_solved, problem_bar)

    def test_declare_as_solved_and_unsolved(self):
        problem_foo = DBSession.query(Problem).filter(Problem.title == u"Problemtitel fuer foo").one()
        solution_bar = DBSession.query(Solution).filter(Solution.text == u"Lösung für Problem foo").one()
        self.assertFalse(problem_foo.solved_for_problem_creator)
        DBSession.flush()
        problem_foo_id = problem_foo.id
        # Set as solution
        problem_foo.chosen_solution = solution_bar
        # .. Update
        DBSession.flush()
        problem_foo = DBSession.query(Problem).filter(Problem.id == problem_foo_id).one()
        self.assertTrue(problem_foo.solved_for_problem_creator)

        # Retract as solution
        problem_foo.chosen_solution = None
        # .. Update
        DBSession.flush()
        problem_foo = DBSession.query(Problem).filter(Problem.id == problem_foo_id).one()
        self.assertFalse(problem_foo.solved_for_problem_creator)

        # Alternative way to retract (via flag)
        problem_foo.chosen_solution = solution_bar
        problem_foo.solved_for_problem_creator = False
        # .. Update
        DBSession.flush()
        problem_foo = DBSession.query(Problem).filter(Problem.id == problem_foo_id).one()
        self.assertFalse(problem_foo.solved_for_problem_creator)
        self.assertIsNone(problem_foo.chosen_solution)

    def test_tag_hierarchies(self):

        def get_names_from_tags(tag_objects):
            return [tag_object.name for tag_object in tag_objects]

        root_level_tags = Tag.get_trees()
        name2tag = dict()
        for tag in root_level_tags:
            # build handy mapping
            name2tag[tag.name] = tag
            # all roots are there
            self.assertIn(tag.name, (u'foo', u'bar', u'äöü',))
            self.assertListEqual(tag.children, [])

        # Build small hierarchy:
        # foo -> foo-1
        #        foo-2
        #        foo-3
        # bar -> bar-1 -> bar-1-1
        #     -> bar-2

        foo_tag = name2tag[u'foo']
        bar_tag = name2tag[u'bar']

        # Build 1st level children
        for num, tag in zip([1, 2, 3], [foo_tag]*3) + zip([1, 2], [bar_tag] * 2):
            child_tag = Tag(name=u'{}-{}'.format(tag.name, num))
            tag.children.append(child_tag)
            name2tag[child_tag.name] = child_tag
            DBSession.add(child_tag)

        bar_child = name2tag[u'bar-1']

        # Build 2nd level children
        for num in [1]:
            child_tag = Tag(name=u'{}-{}'.format(bar_child.name, num))
            bar_child.children.append(child_tag)
            name2tag[child_tag.name] = child_tag
            DBSession.add(child_tag)

        expected_children = defaultdict(list)
        expected_children.update({
            u'foo': [u'foo-1', u'foo-2', u'foo-3'],
            u'bar': [u'bar-1', u'bar-2'],
            u'bar-1': [u'bar-1-1'],
        })

        def check_children(parent_tag, expected_children):
            ret = set(expected_children[parent_tag.name]) == set(get_names_from_tags(parent_tag.children))
            for tag in parent_tag.children:
                ret &= check_children(tag, expected_children)
            return ret

        # Check against the expected outcome
        for tag in Tag.get_trees():
            self.assertTrue(check_children(tag, expected_children))


        expected_sub_children = defaultdict(list)
        expected_sub_children.update({
            u'foo': [u'foo-1', u'foo-2', u'foo-3'],
            u'bar': [u'bar-1', u'bar-2', u'bar-1-1'],
            u'bar-1': [u'bar-1-1'],
        })

        def check_sub_children(parent_tag, expected_sub_children):
            ret = set(expected_sub_children[parent_tag.name]) ==\
                      set(get_names_from_tags(parent_tag.get_all_sub_children()))
            for tag in parent_tag.children:
                ret &= check_sub_children(tag, expected_children)
            return ret

        # Check against the expected outcome
        for tag in root_level_tags:
            check_sub_children(tag, expected_sub_children)

    def test_tag_hierarchies_delete(self):

        def get_names_from_tags(tag_objects):
            return [tag_object.name for tag_object in tag_objects]

        name2tag = dict([(tag.name, tag,) for tag in Tag.get_trees()])
        # Building another hierarchy:
        # foo -> foo-1 -> foo-1-1
        #              -> foo-1-2
        #     -> foo-2 -> foo-2-1
        #              -> foo-2-2 -> foo-2-2-1
        foo_tag = name2tag[u'foo']
        foo_tag.children.append(Tag(u'foo-1'))
        foo_tag.children[0].children.append(Tag(u'foo-1-1'))
        foo_tag.children[0].children.append(Tag(u'foo-1-2'))
        foo_tag.children.append(Tag(u'foo-2'))
        foo_tag.children[1].children.append(Tag(u'foo-2-1'))
        foo_tag.children[1].children.append(Tag(u'foo-2-2'))
        foo_tag.children[1].children[1].children.append(Tag(u'foo-2-2-1'))

        expected_children = defaultdict(list)
        expected_children.update({
            u'foo': [u'foo-1', u'foo-2'],
            u'foo-1': [u'foo-1-1', u'foo-1-2'],
            u'foo-2': [u'foo-2-1', u'foo-2-2'],
            u'foo-2-2': [u'foo-2-2-1'],
        })

        def check_children(parent_tag, expected_children):
            ret = set(expected_children[parent_tag.name]) == set(get_names_from_tags(parent_tag.children))
            for tag in parent_tag.children:
                ret &= check_children(tag, expected_children)
            return ret

        # Check against the expected outcome
        for tag in Tag.get_trees():
            self.assertTrue(check_children(tag, expected_children))

        transaction.commit()
        # Delete foo-1 (direct children deleted?)
        del expected_children[u'foo-1']
        expected_children[u'foo'].remove(u'foo-1')
        foo1_tag = DBSession.query(Tag).filter(Tag.name == u'foo-1').one()
        DBSession.delete(foo1_tag)
        transaction.commit()

        deleted = DBSession.query(Tag).filter(Tag.name in [u'foo-1', u'foo-1-1', u'foo-1-2']).all()
        self.assertEqual(deleted, [])

        # Check structure
        for tag in Tag.get_trees():
            self.assertTrue(check_children(tag, expected_children))

        # Delete foo-2 (also grand-children deleted?)
        del expected_children[u'foo-2']
        del expected_children[u'foo-2-2']
        expected_children[u'foo'].remove(u'foo-2')
        foo2_tag = DBSession.query(Tag).filter(Tag.name == u'foo-2').one()
        DBSession.delete(foo2_tag)
        transaction.commit()

        deleted = DBSession.query(Tag).filter(Tag.name in [u'foo-2', u'foo-2-1', u'foo-2-2', u'foo-2-2-1']).all()
        self.assertEqual(deleted, [])

        # Check structure
        for tag in Tag.get_trees():
            self.assertTrue(check_children(tag, expected_children))


class WebTest(unittest.TestCase):
    """Represents various functional tests (i.d. making requests). Those tests will run independently.
    """

    def basic_auth(self, username, password=u'dummy-password'):
        auth_credentials = base64.b64encode(u'{}:{}'.format(username, password,).encode('utf8')).strip()
        return {'Authorization': 'Basic {}'.format(auth_credentials)}

    def setUp(self):
        """Setup the application and init it with some test data.
        """
        settings = get_appsettings(os.path.join(compass_path, '../', 'testing.ini'))
        app = main({}, **settings)
        from webtest import TestApp
        self.testapp = TestApp(app)
        Base.metadata.create_all(bind=DBSession.bind)
        setup_dummy_data()

    def tearDown(self):
        """Deplete all tables.
        """
        for table in reversed(Base.metadata.sorted_tables):
            DBSession.execute(table.delete())
        transaction.commit()
        DBSession.remove()


class IntegrationTestCase(unittest.TestCase):
    """Represents a test as series of action, which may result in a state-dependent database.
    """

    def basic_auth(self, username, password=u'dummy-password'):
        auth_credentials = base64.b64encode(u'{}:{}'.format(username, password,).encode('utf8')).strip()
        return {'Authorization': 'Basic {}'.format(auth_credentials)}

    @classmethod
    def setUpClass(cls):
        settings = get_appsettings(os.path.join(compass_path, '../', 'testing.ini'))
        app = main({}, **settings)
        from webtest import TestApp
        cls.testapp = TestApp(app)
        Base.metadata.create_all(bind=DBSession.bind)
        setup_dummy_data()

    @classmethod
    def tearDownClass(cls):
        DBSession.remove()


class TestTags(IntegrationTestCase):
    """Testing the tag related views /rest/tags/*
    """

    def test_00_get_all_tags(self):
        response = self.testapp.get('/rest/tags/', status=200)
        ret = response.json
        self.assertIsInstance(ret, list)
        for element in ret:
            self.assertTrue(element[u'name'] in [u'foo', u'bar', u'äöü'])

    def test_01_get_single_tags(self):
        self.testapp.get('/rest/tags/foo', status=200)

    def test_02_does_not_exist(self):
        self.testapp.get('/rest/tags/doesNotExist', status=404)

    def test_03_create_tag_by_collection_unauthorized(self):
        self.testapp.post_json('/rest/tags/', dict(name=u"new"), status=403)

    def test_03_create_tag_by_collection(self):
        self.testapp.post_json('/rest/tags/', dict(name=u"new"), headers=self.basic_auth('admin'))
        self.testapp.get('/rest/tags/new', status=200)

    def test_04_create_tag_by_name(self):
        self.testapp.post_json('/rest/tags/new2', dict(), headers=self.basic_auth('admin'))
        self.testapp.get('/rest/tags/new2', status=200)

    def test_05_create_tag_with_put(self):
        self.testapp.put_json('/rest/tags/new3', dict(), headers=self.basic_auth('admin'))
        self.testapp.get('/rest/tags/new3', status=200)

    def test_06_rename_tag_with_put_unauthorized(self):
        self.testapp.put_json('/rest/tags/new3', dict(name=u'new4'), status=403)

    def test_06_rename_tag_with_put(self):
        self.testapp.put_json('/rest/tags/new3', dict(name=u'new4'), headers=self.basic_auth('admin'))
        self.testapp.get('/rest/tags/new4', status=200)

    def test_07_delete_tag_unauthorized(self):
        self.testapp.delete('/rest/tags/foo', status=403)

    def test_07_delete_tag(self):
        self.testapp.put_json('/rest/tags/new5', dict(), headers=self.basic_auth('admin'))
        self.testapp.delete('/rest/tags/new5', status=200, headers=self.basic_auth('admin'))
        self.testapp.get('/rest/tags/new5', status=404)

    def test_08_read_umlaut_tag(self):
        self.testapp.get('/rest/tags/äöü', status=200)

    def test_09_create_umlaut_tag(self):
        self.testapp.post_json('/rest/tags/', dict(name=u"ööö"), headers=self.basic_auth('admin'))
        self.testapp.get('/rest/tags/ööö', status=200)

    def test_10_create_empty_tag(self):
        self.testapp.post_json('/rest/tags/', dict(name=u""), headers=self.basic_auth('admin'), status=400)
        self.testapp.get('/rest/tags/foo', status=200)

    def test_11_rename_to_empty(self):
        self.testapp.put_json('/rest/tags/foo', dict(name=u""), headers=self.basic_auth('admin'), status=400)

    def test_12_update_description(self):
        self.testapp.put_json('/rest/tags/foo', dict(description=u"test"), headers=self.basic_auth('admin'), status=200)
        ret = self.testapp.get('/rest/tags/foo', status=200)
        self.assertEqual(ret.json_body['description'], u"test")

    def test_12_create_children_tag(self):
        ret = self.testapp.get('/rest/tags/foo/children', status=200)
        self.assertIsInstance(ret.json_body, list)
        self.assertEqual(len(ret.json_body), 0)
        ret = self.testapp.post_json('/rest/tags/foo/children', dict(name=u"foo-1"), headers=self.basic_auth('admin'),
                                     status=201)
        new_url_path = '/' + '/'.join(ret.normal_body.split()[-1].split('/')[3:])
        ret = self.testapp.get(new_url_path, status=200)
        self.assertEqual(ret.json_body['name'], u'foo-1')

    def test_13_tag_with_hierarchy(self):
        self.testapp.post_json('/rest/tags/bar/children', dict(name=u"bar-1"), headers=self.basic_auth('admin'),
                               status=201)
        self.testapp.post_json('/rest/tags/bar/children', dict(name=u"bar-2"), headers=self.basic_auth('admin'),
                               status=201)
        self.testapp.post_json('/rest/tags/bar-1/children', dict(name=u"bar-1-1"), headers=self.basic_auth('admin'),
                               status=201)
        # Getting all children and their hierarchy
        ret = self.testapp.get('/rest/tags/bar', dict(with_hierarchy=True), status=200)
        bar = ret.json_body
        self.assertEqual(set([child['name'] for child in bar['children']]), set([u'bar-1', u'bar-2']))
        bar_1 = [child for child in bar['children'] if child['name'] == u'bar-1'][0]
        self.assertEqual(set([child['name'] for child in bar_1['children']]), set([u'bar-1-1']))

        # Getting all immediate children but not whole hierarchy
        ret = self.testapp.get('/rest/tags/bar', dict(with_hierarchy=False), status=200)
        bar = ret.json_body
        for child in bar['children']:
            # No children of sub-children there
            self.assertIsNone(child.get('children'))

    def test_14_tag_with_hierarchy(self):
        ret = self.testapp.get('/rest/tags/', dict(tree_representation=True), status=200)

        def tag_by_name(taglist, name):
            return [element for element in taglist if element['name'] == name][0]

        root_level_tags = ret.json_body
        for element in root_level_tags:
            self.assertIn(element['name'], [u'foo', u'bar', u'äöü', u'new', u'new2', u'new3', u'new5', u'ööö'])

        foo = tag_by_name(root_level_tags, u'foo')
        self.assertEqual(set([child['name'] for child in foo['children']]), set([u'foo-1']))

        bar = tag_by_name(root_level_tags, u'bar')
        self.assertEqual(set([child['name'] for child in bar['children']]), set([u'bar-1', u'bar-2']))

        bar_1 = tag_by_name(bar['children'], u'bar-1')
        self.assertEqual(set([child['name'] for child in bar_1['children']]), set([u'bar-1-1']))


class TestProblems(WebTest):
    """Testing the tag related views /rest/problems/*
    """

    def test_00_get_all_problems(self):
        response = self.testapp.get('/rest/problems/', status=200)
        ret = response.json
        self.assertIsInstance(ret, list)
        for element in ret:
            self.assertTrue(element[u'title'] in [u'Problemtitel fuer foo', u'Problemtitel fuer bar',
                                                  u'Problemtitel fuer foo+bar', u'Problemtitel mit Umlauten äöüß'])

    def test_01_get_single_problems(self):
        self.testapp.get('/rest/problems/1', status=200)

    def test_02_does_not_exist(self):
        self.testapp.get('/rest/problems/9999', status=404)

    def test_03_create_problem_by_collection(self):
        ret = self.testapp.post_json('/rest/problems/', dict(title=u"Neuer Titel", description=u"Beschreibung",
                                                             tags=[u"foo"]), headers=self.basic_auth('bob'), status=201)
        new_url_path = '/' + '/'.join(ret.normal_body.split()[-1].split('/')[3:])
        ret = self.testapp.get(new_url_path, status=200)
        self.assertEqual(ret.json_body['title'], u"Neuer Titel")
        self.assertEqual(ret.json_body['description'], u"Beschreibung")
        self.assertEqual(ret.json_body['tags'][0]['name'], u"foo")

    def test_03_create_problem_by_collection_with_empty_input(self):
        self.testapp.post_json('/rest/problems/', dict(title=u"", description=u"", tags=[]),
                               headers=self.basic_auth('bob'), status=400)

    def test_03_create_problem_by_collection_with_non_existing_tag(self):
        self.testapp.post_json('/rest/problems/', dict(title=u"Neuer Titel", description=u"Beschreibung",
                                                       tags=[u"foo", u"*****"]), headers=self.basic_auth('bob'),
                               status=400)

    def test_04_create_problem_by_collection_with_no_login(self):
        self.testapp.post_json('/rest/problems/', dict(title=u"Neuer Titel", description=u"Beschreibung",
                                                       tags=[u"foo"]), status=403)

    def test_05_update_problem_by_collection(self):
        new_title = u"Problemtitel fuer foo (bearbeitet)"
        new_description = u"Andere Beschreibung"
        new_tags = [u'bar', u'äöü']
        self.testapp.put_json('/rest/problems/1', dict(title=new_title, tags=new_tags, description=new_description),
                              headers=self.basic_auth('bob'), status=200)
        ret = self.testapp.get('/rest/problems/1', status=200)
        self.assertEqual(ret.json_body['title'], new_title)
        self.assertEqual(ret.json_body['description'], new_description)
        self.assertEqual(set([tag['name'] for tag in ret.json_body['tags']]), set(new_tags))
        new_tags = []
        self.testapp.put_json('/rest/problems/1', dict(tags=new_tags), headers=self.basic_auth('bob'), status=200)
        ret = self.testapp.get('/rest/problems/1', status=200)
        self.assertEqual(set([tag['name'] for tag in ret.json_body['tags']]), set(new_tags))

    def test_05_update_problem_by_collection_with_empty_input(self):
        self.testapp.put_json('/rest/problems/1', dict(title=u"",), headers=self.basic_auth('bob'), status=400)

    def test_06_update_problem_by_collection_with_no_login(self):
        new_title = u"Problemtitel fuer foo (bearbeitet)"
        self.testapp.put_json('/rest/problems/1', dict(title=new_title,), status=403)

    def test_06_update_problem_by_collection_but_not_mine(self):
        new_title = u"Problemtitel fuer foo (bearbeitet)"
        self.testapp.put_json('/rest/problems/1', dict(title=new_title,), headers=self.basic_auth('mary'), status=403)

    def test_07_delete_problem(self):
        self.testapp.delete('/rest/problems/1', headers=self.basic_auth('bob'), status=200)
        self.testapp.get('/rest/problems/1', status=404)

    def test_07_delete_problem_but_not_mine(self):
        self.testapp.delete('/rest/problems/1', headers=self.basic_auth('mary'), status=403)

    def test_08_get_tags_from_problem(self):
        ret = self.testapp.get('/rest/problems/1/tags/')
        self.assertIsInstance(ret.json_body, list)

    def test_09_bind_tag_on_problem(self):
        ret = self.testapp.get('/rest/problems/1/tags/')
        self.assertEqual(len(ret.json_body), 1)
        self.testapp.put('/rest/problems/1/tags/bar', headers=self.basic_auth('bob'), status=201)
        ret = self.testapp.get('/rest/problems/1/tags/')
        self.assertEqual(len(ret.json_body), 2)

    def test_09_bind_tag_on_problem_with_no_login(self):
        self.testapp.put('/rest/problems/1/tags/bar', status=403)

    def test_09_bind_tag_on_problem_but_not_mine(self):
        self.testapp.put('/rest/problems/1/tags/bar', headers=self.basic_auth('mary'), status=403)

    def test_09_bind_non_existing_tag_on_problem(self):
        self.testapp.put('/rest/problems/1/tags/*****', headers=self.basic_auth('bob'), status=400)

    def test_09_bind_same_tag_on_problem(self):
        self.testapp.put('/rest/problems/1/tags/foo', headers=self.basic_auth('bob'), status=400)

    def test_10_unbind_tag_from_problem(self):
        self.testapp.get('/rest/problems/1/tags/foo', status=200)
        self.testapp.delete('/rest/problems/1/tags/foo', headers=self.basic_auth('bob'), status=200)
        self.testapp.get('/rest/problems/1/tags/foo', status=404)

    def test_10_unbind_tag_from_problem_with_no_login(self):
        self.testapp.delete('/rest/problems/1/tags/foo', status=403)

    def test_10_unbind_tag_from_problem_but_not_mine(self):
        self.testapp.delete('/rest/problems/1/tags/foo', headers=self.basic_auth('mary'), status=403)

    def test_10_unbind_non_existing_tag_on_problem(self):
        self.testapp.delete('/rest/problems/1/tags/******', headers=self.basic_auth('bob'), status=404)

    def test_10_unbind_all_tags_from_problem(self):
        ret = self.testapp.get('/rest/problems/1/tags/', status=200)
        self.assertGreater(len(ret.json_body), 0)
        self.testapp.delete('/rest/problems/1/tags/', headers=self.basic_auth('bob'), status=200)
        ret = self.testapp.get('/rest/problems/1/tags/', status=200)
        self.assertEqual(len(ret.json_body), 0)

    def test_10_unbind_all_tags_from_problem_with_no_login(self):
        self.testapp.delete('/rest/problems/1/tags/', status=403)

    def test_10_unbind_all_tags_from_problem_but_not_mine(self):
        self.testapp.delete('/rest/problems/1/tags/', headers=self.basic_auth('mary'), status=403)

    def test_11_delete_tag_check_if_removed(self):
        ret = self.testapp.get('/rest/problems/1/tags/', status=200)
        self.assertGreater(len(ret.json_body), 0)
        self.testapp.delete('/rest/tags/foo', headers=self.basic_auth('admin'), status=200)
        ret = self.testapp.get('/rest/problems/1/tags/', status=200)
        self.assertEqual(len(ret.json_body), 0)

    def test_12_get_comments_from_problem(self):
        ret = self.testapp.get('/rest/problems/1/comments/', status=200)
        self.assertIsInstance(ret.json_body, list)

    def test_13_create_comment_for_problem(self):
        ret = self.testapp.post_json('/rest/problems/1/comments/', dict(text=u"Neuer Kommentar"),
                                     headers=self.basic_auth('bob'), status=201)
        new_url_path = '/' + '/'.join(ret.normal_body.split()[-1].split('/')[3:])
        self.testapp.get(new_url_path, status=200)

    def test_14_create_comment_for_problem_empty_input(self):
        self.testapp.post_json('/rest/problems/1/comments/', dict(text=u""),
                               headers=self.basic_auth('bob'), status=400)

    def test_15_update_comment_for_problem(self):
        self.testapp.get('/rest/problems/1/comments/1', status=200)
        self.testapp.put_json('/rest/problems/1/comments/1', dict(text=u"*edit*"),
                              headers=self.basic_auth('alice'), status=200)
        ret = self.testapp.get('/rest/problems/1/comments/1', status=200)
        self.assertEqual(ret.json_body['text'], u"*edit*")

    def test_15_update_comment_for_problem_empty_input(self):
        self.testapp.put_json('/rest/problems/1/comments/1', dict(text=u""),
                              headers=self.basic_auth('alice'), status=400)

    def test_16_delete_comment_from_problem(self):
        self.testapp.delete('/rest/problems/1/comments/2', headers=self.basic_auth('mary'), status=200)
        self.testapp.get('/rest/problems/1/comments/2', status=404)

    def test_17_get_children_from_comment(self):
        ret = self.testapp.get('/rest/problems/1/comments/2/replies/', status=200)
        self.assertIsInstance(ret.json_body, list)
        self.assertEqual(len(ret.json_body), 2)

    def test_18_delete_comment_from_problem_children_deleted(self):
        ret = self.testapp.get('/rest/problems/1/comments/2/replies/', status=200)
        self.assertIsInstance(ret.json_body, list)
        children_ids = [comment['id'] for comment in ret.json_body]
        self.testapp.delete('/rest/problems/1/comments/2', headers=self.basic_auth('mary'), status=200)
        for child_id in children_ids:
            self.testapp.get('/rest/problems/1/comments/%i' % child_id, status=404)

    def test_19_create_answer_comment_for_problem(self):
        ret = self.testapp.post_json('/rest/problems/1/comments/2/replies/',
                                     dict(text=u"Neuer Antwortkommentar"), headers=self.basic_auth('bob'), status=201)
        new_url_path = '/' + '/'.join(ret.normal_body.split()[-1].split('/')[3:])
        ret = self.testapp.get(new_url_path, status=200)
        answer_comment_id = ret.json_body['id']
        ret = self.testapp.get('/rest/problems/1/comments/2/replies/', status=200)
        self.assertIn(answer_comment_id, [comment['id'] for comment in ret.json_body])

    def test_19_create_answer_comment_for_problem_with_no_login(self):
        self.testapp.post_json('/rest/problems/1/comments/2/replies/', dict(text=u"Neuer Antwortkommentar"), status=403)

    def test_20_get_solution_proposals_for_problem(self):
        ret = self.testapp.get('/rest/problems/1/solutions/', status=200)
        self.assertIsInstance(ret.json_body, list)
        self.assertGreater(len(ret.json_body), 0)

    def test_20_get_solution_proposals_for_problem_check_order(self):
        # Check if no previous solution
        ret = self.testapp.get('/rest/problems/3/solutions/', status=200)
        self.assertEqual(len(ret.json_body), 0)
        # Add first solution (aka now existing solution)
        self.testapp.post_json('/rest/problems/3/solutions/', dict(text=u"Alt"),
                               headers=self.basic_auth('bob'), status=201)
        ret = self.testapp.get('/rest/problems/3/solutions/', status=200)
        existing_solution = ret.json_body[0]
        # Add new solution
        self.testapp.post_json('/rest/problems/3/solutions/', dict(text=u"Brandneu"),
                               headers=self.basic_auth('alice'), status=201)
        ret = self.testapp.get('/rest/problems/3/solutions/', status=200)
        new_solution = ret.json_body[0]
        self.assertEqual(new_solution, ret.json_body[0], "New solution should come first.")
        self.assertEqual(existing_solution, ret.json_body[1], "Old solution should come second.")

        self.testapp.put_json('/rest/problems/3', dict(chosen_solution_id=existing_solution['id'],),
                              headers=self.basic_auth('admin'), status=200)
        ret = self.testapp.get('/rest/problems/3/solutions/', status=200)
        self.assertEqual(existing_solution, ret.json_body[0], "Older solution was accepted and should appear first.")

    def test_21_get_single_solution_proposals_for_problem(self):
        self.testapp.get('/rest/problems/1/solutions/1', status=200)

    def test_22_get_single_solution_proposals_for_problem(self):
        self.testapp.get('/rest/problems/1/solutions/1', status=200)

    def test_23_create_solution_for_problem(self):
        self.testapp.post_json('/rest/problems/1/solutions/', dict(text=u"Meine Lösung"),
                               headers=self.basic_auth('alice'), status=201)

    def test_24_create_solution_for_problem_with_no_login(self):
        self.testapp.post_json('/rest/problems/1/solutions/', dict(text=u"Meine Lösung"), status=403)

    def test_25_create_solution_for_problem_twice_for_user(self):
        self.testapp.post_json('/rest/problems/1/solutions/', dict(text=u"Meine Lösung"),
                               headers=self.basic_auth('mary'), status=201)

    def test_26_create_empty_solution_for_problem(self):
        self.testapp.post_json('/rest/problems/1/solutions/', dict(text=u""),
                               headers=self.basic_auth('alice'), status=400)

    def test_27_propose_existing_solution_for_problem(self):
        self.testapp.put('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=200)
        self.testapp.get('/rest/problems/2/solutions/1', status=200)

    def test_28_propose_twice_solution_for_problem(self):
        self.testapp.put('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=200)
        self.testapp.put('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=200)  # idempotent

    def test_29_propose_existing_solution_for_problem_without_login(self):
        self.testapp.put('/rest/problems/2/solutions/1', status=403)

    def test_30_revoke_proposed_solution_for_problem(self):
        self.testapp.put('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=200)
        self.testapp.delete('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=200)
        self.testapp.get('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=404)
        # Original solution does still exist?
        self.testapp.get('/rest/problems/1/solutions/1', headers=self.basic_auth('alice'), status=200)

    def test_30_revoke_proposed_solution_for_problem_without_login(self):
        self.testapp.put('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=200)
        self.testapp.delete('/rest/problems/2/solutions/1', status=403)

    def test_31_revoke_proposed_solution_for_problem_but_not_mine(self):
        self.testapp.put('/rest/problems/2/solutions/1', headers=self.basic_auth('alice'), status=200)
        self.testapp.delete('/rest/problems/2/solutions/1', headers=self.basic_auth('bob'), status=403)

    def test_set_as_solved_and_unsolved(self):
        # Ensure that problem is not solved
        ret = self.testapp.get('/rest/problems/1', status=200)
        self.assertEqual(ret.json_body['solved_for_problem_creator'], False)
        # Choose a solution
        self.testapp.put_json('/rest/problems/1', dict(chosen_solution_id=1,), headers=self.basic_auth('bob'), status=200)
        # Declared as solved?
        ret = self.testapp.get('/rest/problems/1', status=200)
        self.assertEqual(ret.json_body['solved_for_problem_creator'], True)
        self.assertEqual(ret.json_body['chosen_solution_id'], 1)
        # Retract decision
        self.testapp.put_json('/rest/problems/1', dict(chosen_solution_id=None,), headers=self.basic_auth('bob'), status=200)
        # Declared as not solved?
        ret = self.testapp.get('/rest/problems/1', status=200)
        self.assertEqual(ret.json_body['solved_for_problem_creator'], False)
        self.assertEqual(ret.json_body['chosen_solution_id'], None)

    def test_set_as_unsolved_by_flag(self):
        self.testapp.put_json('/rest/problems/1', dict(chosen_solution_id=1,), headers=self.basic_auth('bob'), status=200)
        # Retract decision
        self.testapp.put_json('/rest/problems/1', dict(solved_for_problem_creator=False,), headers=self.basic_auth('bob'), status=200)
        ret = self.testapp.get('/rest/problems/1', status=200)
        self.assertEqual(ret.json_body['solved_for_problem_creator'], False)
        self.assertEqual(ret.json_body['chosen_solution_id'], None)



class TestFeedback(WebTest):
    """Testing the tag related views /rest/problems/{problem_id}/solutions/{solution_id}/feedback
    """
    def test_00_avg_score(self):
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback', status=200)
        self.assertIsNotNone(ret.json_body)
        expected_result = {
            # Usefulness
            'helpful': 0.5,
            'solved': 0.5,
            'appropriate': 0,
            # Per Solution-rating
            'comprehensibility': 3.0,
            'speed': None,
            'difficulty': None,
        }
        for criteria, value in expected_result.items():
            self.assertEqual(ret.json_body[criteria]['average'], value)

    def test_00_sum_score(self):
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback', status=200)

        for criteria, criteria_dict in ret.json_body.items():
            sum_votes = 0
            for vote_value, vote_count in criteria_dict['votes'].items():
                sum_votes += int(vote_value) * vote_count
            self.assertEqual(sum_votes, criteria_dict['sum'])

    def test_01_create_rating_and_update_rating_for_solution(self):

        data = {
            # Usefulness
            'helpful': 1,
            'solved': 1,
            'appropriate': 1,
            # Per Solution-rating
            'comprehensibility': 5,
            'speed': 3,
            'difficulty': 3,
        }
        ret = self.testapp.post_json('/rest/problems/1/solutions/1/feedback', data, headers=self.basic_auth('pete'),
                                     status=201)
        new_url_path = '/' + '/'.join(ret.normal_body.split()[-1].split('/')[3:])
        self.assertEqual(new_url_path, '/rest/problems/1/solutions/1/feedback/pete')
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(data, ret.json_body)

        data = {
            # Usefulness
            'helpful': 0,
            'solved': 0,
            'appropriate': 0,
            # Per Solution-rating
            'comprehensibility': 1,
            'speed': 1,
            'difficulty': 1,
        }
        self.testapp.post_json('/rest/problems/1/solutions/1/feedback', data, headers=self.basic_auth('pete'),
                               status=201)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(data, ret.json_body)

        data = {
            # Usefulness
            'helpful': 0,
            'solved': 0,
            'appropriate': 0,
            # Per Solution-rating
            'comprehensibility': 5,
            'speed': 4,
            'difficulty': 3,
        }
        self.testapp.put_json('/rest/problems/1/solutions/1/feedback/pete', data, headers=self.basic_auth('pete'),
                              status=200)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(data, ret.json_body)

    def test_02_create_single_rating(self):
        expected_data = {
            # Usefulness
            'helpful': None,
            'solved': None,
            'appropriate': None,
            # Per Solution-rating
            'comprehensibility': 5,
            'speed': None,
            'difficulty': None,
        }
        self.testapp.post_json('/rest/problems/1/solutions/1/feedback', dict(comprehensibility=5),
                               headers=self.basic_auth('pete'), status=201)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(expected_data, ret.json_body)
        # Do another vote
        new_rating = dict(helpful=1)
        expected_data.update(new_rating)
        self.testapp.put_json('/rest/problems/1/solutions/1/feedback/pete', new_rating,
                              headers=self.basic_auth('pete'), status=200)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(expected_data, ret.json_body)

    def test_03_remove_single_ratings(self):
        expected_data = {
            # Usefulness
            'helpful': None,
            'solved': 0,
            'appropriate': None,
            # Per Solution-rating
            'comprehensibility': None,
            'speed': 5,
            'difficulty': None,
        }
        self.testapp.post_json('/rest/problems/1/solutions/1/feedback', dict(solved=0, speed=5),
                               headers=self.basic_auth('pete'), status=201)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(expected_data, ret.json_body)
        # Delete those votes
        new_rating = dict(solved=None, speed=None)
        expected_data.update(new_rating)
        self.testapp.put_json('/rest/problems/1/solutions/1/feedback/pete', new_rating,
                              headers=self.basic_auth('pete'), status=200)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(expected_data, ret.json_body)

    def test_04_delete_all_ratings(self):
        data = {
            # Usefulness
            'helpful': 0,
            'solved': 0,
            'appropriate': 0,
            # Per Solution-rating
            'comprehensibility': 5,
            'speed': 4,
            'difficulty': 3,
        }
        self.testapp.post_json('/rest/problems/1/solutions/1/feedback', data, headers=self.basic_auth('pete'),
                               status=201)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(data, ret.json_body)
        for key in data.keys():
            data[key] = None
        self.testapp.delete('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'), status=200)
        ret = self.testapp.get('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'),
                               status=200)
        self.assertEqual(data, ret.json_body)

    def test_05_update_rating_but_not_mine(self):
        self.testapp.post_json('/rest/problems/1/solutions/1/feedback', dict(solved=1), headers=self.basic_auth('pete'),
                               status=201)
        self.testapp.put_json('/rest/problems/1/solutions/1/feedback/pete', dict(solved=0),
                              headers=self.basic_auth('bob'), status=403)

    def test_06_delete_own_stuff_no_voting(self):
        self.testapp.delete('/rest/problems/1/solutions/1/feedback/pete', headers=self.basic_auth('pete'), status=200)


class TestUsers(WebTest):
    """Testing the user related views /rest/views/*
    """
    def test_00_get_all_users_with_no_login(self):
        self.testapp.get('/rest/users', status=403)

    def test_01_get_user_with_no_login(self):
        self.testapp.get('/rest/users/bob', status=403)

    def test_02_get_user_with_rightful_login(self):
        self.testapp.get('/rest/users/bob', headers=self.basic_auth('bob'), status=200)

    def test_03_get_user_but_not_mine(self):
        self.testapp.get('/rest/users/bob', headers=self.basic_auth('mary'), status=403)

    def test_04_get_all_users_as_admin(self):
        self.testapp.get('/rest/users', headers=self.basic_auth('admin'), status=200)

    def helper_valid_cases(self):
        # Working use case
        regular_input = dict(username=u'john',
                             password=u'my_password',
                             password_repeated=u'my_password',
                             email=u'john@mail.com',
                             email_repeated=u'john@mail.com')
        # Generate some other valid use cases
        cases = list()
        cases.append(regular_input)
        cases.append(dict(regular_input, username=u'JOHN_DOE'))
        cases.append(dict(regular_input, password=u'SUPER_LONG_MEGA_PASSWORD_WITH_UMLAUTS_SEE_ÖÄÜßöäü!%',
                          password_repeated=u'SUPER_LONG_MEGA_PASSWORD_WITH_UMLAUTS_SEE_ÖÄÜßöäü!%'))
        cases.append(dict(regular_input, email=u'äöüß@mail.sub.com',
                          email_repeated=u'äöüß@mail.sub.com'))
        return cases

    def test_05_create_user_account(self):

        for index, case in enumerate(self.helper_valid_cases()):
            # Make name and email unique
            case['username'] = u"{0}{1}".format(case['username'], index)
            case['email'] = case['email_repeated'] = u"{0}{1}".format(case['email'], index)
            ret = self.testapp.post_json('/rest/users', case, status=201)
            new_url_path = '/' + '/'.join(ret.normal_body.split()[-1].split('/')[3:])
            self.testapp.get(new_url_path, headers=self.basic_auth(case['username'], case['password']), status=200)

    def test_06_create_second_user_account(self):
        self.testapp.post_json('/rest/users', dict(username=u'john',
                                                   password=u'my_password',
                                                   password_repeated=u'my_password',
                                                   email=u'john@mail.com',
                                                   email_repeated=u'john@mail.com'),
                               headers=self.basic_auth('bob'), status=403)

    def helper_invalid_cases(self):
        # Working use case
        regular_input = dict(username=u'john',
                             password=u'my_password',
                             password_repeated=u'my_password',
                             email=u'john@mail.com',
                             email_repeated=u'john@mail.com')
        # Generate some invalid(!) use cases
        cases = list()
        cases.append(dict(regular_input, username=u'!"$öäüß_'))
        cases.append(dict(regular_input, password=u'', password_repeated=u''))
        cases.append(dict(regular_input, password=u'1', password_repeated=u'1'))
        cases.append(dict(regular_input, password=u'john', password_repeated=u'john'))
        cases.append(dict(regular_input, password=u'different'))
        cases.append(dict(regular_input, email=u'@mail.com', email_repeated=u'@mail.com'))
        cases.append(dict(regular_input, email=u'abc@.com', email_repeated=u'abc@.com'))
        cases.append(dict(regular_input, email=u'john@mail.', email_repeated=u'john@mail.'))
        cases.append(dict(regular_input, email=u'different@mail.com'))
        return cases

    def test_07_create_user_account_invalid_data(self):
        for case in self.helper_invalid_cases():
            self.testapp.post_json('/rest/users', case, status=400)

    def test_08_update_user_account(self):
        previous_password = u'dummy-password'
        for case in self.helper_valid_cases():
            del case['username']  # Ignore username changes
            self.testapp.put_json('/rest/users/bob', case, headers=self.basic_auth('bob',previous_password),
                                  status=200)
            previous_password = case.get('password', previous_password)

    def test_08_update_user_account_invalid_data(self):
        for case in self.helper_invalid_cases():
            if case.get('username'):  # Ignore username changes
                continue
            self.testapp.put_json('/rest/users/bob', case, headers=self.basic_auth('bob'), status=400)

    def test_08_update_others_user_account(self):
        self.testapp.put_json('/rest/users/bob', dict(password=u'my_account_now', password_repeated=u'my_account_now'),
                              headers=self.basic_auth('mary'), status=403)

    def test_09_read_problems_of_user(self):
        ret = self.testapp.get('/rest/users/bob/problems', headers=self.basic_auth('bob'), status=200)
        self.assertIsInstance(ret.json_body, list)
        self.assertEqual(len(ret.json_body), 1)
        ret = self.testapp.get('/rest/users/alice/problems', headers=self.basic_auth('alice'), status=200)
        self.assertEqual(len(ret.json_body), 1)
        ret = self.testapp.get('/rest/users/mary/problems', headers=self.basic_auth('mary'), status=200)
        self.assertEqual(len(ret.json_body), 2)

    def test_10_read_problems_of_user_without_credentials(self):
        self.testapp.get('/rest/users/bob/problems', status=403)
        self.testapp.get('/rest/users/bob/problems', headers=self.basic_auth('alice'), status=403)

    def test_11_read_solutions_of_user(self):
        ret = self.testapp.get('/rest/users/bob/solutions', headers=self.basic_auth('bob'), status=200)
        self.assertIsInstance(ret.json_body, list)
        self.assertEqual(len(ret.json_body), 0)
        ret = self.testapp.get('/rest/users/alice/solutions', headers=self.basic_auth('alice'), status=200)
        self.assertEqual(len(ret.json_body), 0)
        ret = self.testapp.get('/rest/users/mary/solutions', headers=self.basic_auth('mary'), status=200)
        self.assertEqual(len(ret.json_body), 1)

    def test_12_read_problems_of_user_without_credentials(self):
        self.testapp.get('/rest/users/bob/solutions', status=403)
        self.testapp.get('/rest/users/bob/solutions', headers=self.basic_auth('alice'), status=403)

    def test_13_read_status_no_param(self):
        ret = self.testapp.get('/rest/users/bob/status', headers=self.basic_auth('bob'), status=200)
        self.assertEqual(len(ret.json_body['problems']), 1)
        self.assertEqual(len(ret.json_body['problems']['1']['new_comments']), 5)
        self.assertEqual(len(ret.json_body['problems']['1']['new_solutions']), 1)

    def test_14_read_status_future_and_past_date(self):
        from datetime import timedelta
        from compass.helper import utcnow

        date_now = utcnow()
        date_future = utcnow() + timedelta(days=1)

        ret = self.testapp.get('/rest/users/bob/status', {'since': date_now.isoformat()},
                               headers=self.basic_auth('bob'), status=200)
        self.assertEqual(len(ret.json_body['problems']), 0)

        self.testapp.get('/rest/users/bob/status', {'since': date_future.isoformat()},
                         headers=self.basic_auth('bob'), status=400)

    def test_15_read_status_after_some_changes(self):
        from compass.helper import utcnow

        date_no_change = utcnow()

        ret = self.testapp.get('/rest/users/bob/status', {'since': date_no_change.isoformat()},
                               headers=self.basic_auth('bob'), status=200)
        self.assertEqual(len(ret.json_body['problems']), 0)

        date_one_comment = utcnow()

        # Add new comment
        self.testapp.post_json('/rest/problems/1/comments/', dict(text=u"Neuer Kommentar"),
                               headers=self.basic_auth('bob'), status=201)

        ret = self.testapp.get('/rest/users/bob/status', {'since': date_one_comment.isoformat()},
                               headers=self.basic_auth('bob'), status=200)
        self.assertEqual(len(ret.json_body['problems']), 1)
        self.assertEqual(len(ret.json_body['problems']['1']['new_comments']), 1)
        self.assertEqual(len(ret.json_body['problems']['1']['new_solutions']), 0)

        date_one_solution = utcnow()

        # Add new solutions
        self.testapp.post_json('/rest/problems/1/solutions', dict(text=u"Neue Lösung"),
                               headers=self.basic_auth('bob'), status=201)

        ret = self.testapp.get('/rest/users/bob/status', {'since': date_one_solution.isoformat()},
                               headers=self.basic_auth('bob'), status=200)
        self.assertEqual(len(ret.json_body['problems']), 1)
        self.assertEqual(len(ret.json_body['problems']['1']['new_comments']), 0)
        self.assertEqual(len(ret.json_body['problems']['1']['new_solutions']), 1)

        # Now get the updates since very first time stamp
        ret = self.testapp.get('/rest/users/bob/status', {'since': date_no_change.isoformat()},
                               headers=self.basic_auth('bob'), status=200)
        self.assertEqual(len(ret.json_body['problems']), 1)
        self.assertEqual(len(ret.json_body['problems']['1']['new_comments']), 1)
        self.assertEqual(len(ret.json_body['problems']['1']['new_solutions']), 1)


class TestSearch(WebTest):
    """Testing the user related views /rest/views/*
    """
    def test_tags_00_get_all_tags(self):
        ret = self.testapp.post_json('/rest/search/tags', dict(), status=200)
        self.assertIsInstance(ret.json_body['results'], list)
        for element in ret.json_body['results']:
            self.assertTrue(element[u'name'] in [u'foo', u'bar', u'äöü'])

    def test_tags_01_search_by_name(self):
        ret = self.testapp.post_json('/rest/search/tags', dict(name=u'foo'), status=200)
        for element in ret.json_body['results']:
            self.assertTrue(element[u'name'] != [u'foo'])

    def test_tags_02_search_by_names(self):
        ret = self.testapp.post_json('/rest/search/tags', dict(name=[u'foo', u'bar']), status=200)
        for element in ret.json_body['results']:
            self.assertTrue(element[u'name'] in [u'foo', u'bar'])

    def test_tags_03_search_with_count(self):
        ret = self.testapp.post_json('/rest/search/tags', dict(ALL=True), status=200)
        for tag in ret.json_body['results']:
            # Instead of list of tags, we get a list of json-objects
            ret2 = self.testapp.post_json('/rest/search/problems', dict(tags=[tag['name']]), status=200)
            self.assertEqual(tag['problem_count'], len(ret2.json_body['results']))

    def test_tags_04_search_with_count_ensure_descending(self):
        ret = self.testapp.post_json('/rest/search/tags', dict(ALL=True), status=200)
        previous = None
        is_descending = True
        for problem_count in [entry['problem_count'] for entry in ret.json_body['results']]:
            if previous:
                is_descending &= previous >= problem_count
            previous = problem_count
        self.assertTrue(is_descending)

    def test_tags_05_new_tag_has_count_zero(self):
        self.testapp.post_json('/rest/tags/', dict(name=u"loner"), headers=self.basic_auth('admin'))
        ret = self.testapp.post_json('/rest/search/tags', dict(ALL=True, name=u"loner"), status=200)
        entry = ret.json_body['results'][0]
        self.assertEqual(entry['problem_count'], 0)

    def test_tags_06_top_level_tags(self):
        # Adding some sub-tag which should not appear in result set
        self.testapp.post_json('/rest/tags/foo/children', dict(name=u"foo-1"), headers=self.basic_auth('admin'),
                               status=201)
        ret = self.testapp.post_json('/rest/search/tags', dict(ALL=True, top_level=True), status=200)
        for tag in ret.json_body['results']:
            self.assertTrue(tag[u'name'] in [u'foo', u'bar', u'äöü'])
            # Also check if it is possible to use both criteria mixed
            self.assertIsNotNone(tag['problem_count'])

    def test_problems_01_search_all_problems(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(), status=200)
        self.assertIsInstance(ret.json_body['results'], list)
        self.assertGreater(len(ret.json_body['results']), 0)

    def test_problems_02_search_by_title(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(title=u'foo'), status=200)
        self.assertIsInstance(ret.json_body['results'], list)
        self.assertGreater(len(ret.json_body['results']), 0)
        ret = self.testapp.post_json('/rest/search/problems', dict(title=u'bar'), status=200)
        self.assertIsInstance(ret.json_body['results'], list)
        self.assertGreater(len(ret.json_body['results']), 0)

    def test_problems_02_search_by_description(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(description=u'foo+bar'), status=200)
        self.assertEqual(len(ret.json_body['results']), 1)
        ret = self.testapp.post_json('/rest/search/problems', dict(description=u'*DOES NOT EXIST*'), status=200)
        self.assertEqual(len(ret.json_body['results']), 0)

    def test_problems_03_search_by_tag(self):
        # Search by foo
        ret = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo']), status=200)
        self.assertEqual(set([u'Problemtitel fuer foo', u'Problemtitel fuer foo+bar']),
                         set([problem['title'] for problem in ret.json_body['results']]))
        # Search by bar
        ret = self.testapp.post_json('/rest/search/problems', dict(tags=[u'bar']), status=200)
        self.assertEqual(set([u'Problemtitel fuer bar', u'Problemtitel fuer foo+bar']),
                         set([problem['title'] for problem in ret.json_body['results']]))
        # Search by umlaut
        ret = self.testapp.post_json('/rest/search/problems', dict(tags=[u'äöü']), status=200)
        self.assertEqual(set([u'Problemtitel mit Umlauten äöüß']), set([problem['title'] for problem in
                                                                        ret.json_body['results']]))
        # Search by tag foo AND with title bar (which should yield a singleton)
        ret = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo'], title=u'bar'), status=200)
        self.assertEqual(set([u'Problemtitel fuer foo+bar']), set([problem['title'] for problem in
                                                                   ret.json_body['results']]))

    def test_problems_04_search_by_tag_with_operator(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo', u'bar'], tags_operator='and'),
                                     status=200)
        self.assertEqual(set([u'Problemtitel fuer foo+bar']), set([problem['title'] for problem in
                                                                   ret.json_body['results']]))
        ret = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo', u'bar', u'äöü'], tags_operator='and'),
                                     status=200)
        self.assertEqual(set([]), set([problem['title'] for problem in ret.json_body['results']]))

    def test_problems_05_search_by_username(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(username=u'bob'), status=200)
        self.assertEqual(len(ret.json_body['results']), 1)
        self.assertEqual(ret.json_body['results'][0]['user']['username'], u'bob')
        ret = self.testapp.post_json('/rest/search/problems', dict(username=u'mary'), status=200)
        self.assertEqual(len(ret.json_body['results']), 2)
        for problem in ret.json_body['results']:
            self.assertEqual(problem['user']['username'], u'mary')

    def test_problems_06_search_by_multiple_titles(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(title=[u'foo', u'bar']), status=200)
        self.assertEqual(len(ret.json_body['results']), 3)  # Should yield foo, bar, foo
        ret = self.testapp.post_json('/rest/search/problems', dict(title=[u'foo', u'bar'],
                                                                   title_operator='and'), status=200)
        self.assertEqual(len(ret.json_body['results']), 1)  # Should yield foo+bar
        ret = self.testapp.post_json('/rest/search/problems', dict(title=[u'foo', u'bar', u'DOES NOT EXIST'],
                                                                   title_operator='and'), status=200)
        self.assertEqual(len(ret.json_body['results']), 0)  # Should yield none results,

    def test_problems_07_search_by_multiple_descriptions(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(description=[u'foo', u'bar']), status=200)
        self.assertEqual(len(ret.json_body['results']), 3)  # Should yield foo, bar, foo
        ret = self.testapp.post_json('/rest/search/problems', dict(description=[u'foo', u'bar'],
                                                                   description_operator='and'), status=200)
        self.assertEqual(len(ret.json_body['results']), 1)  # Should yield foo+bar
        ret = self.testapp.post_json('/rest/search/problems', dict(description=[u'foo', u'bar', u'DOES NOT EXIST'],
                                                                   description_operator='and'), status=200)
        self.assertEqual(len(ret.json_body['results']), 0)  # Should yield none results,

    def test_problems_06_search_latest(self):
        ret = self.testapp.post_json('/rest/search/problems/', dict(PER_PAGE=2, latest=True), status=200)
        self.assertEqual(len(ret.json_body['results']), 2)

    def test_users_01_search(self):
        ret = self.testapp.post_json('/rest/search/users', dict(username=u'bob'), status=200)
        self.assertEqual(len(ret.json_body['results']), 1)
        user_dict = ret.json_body['results'][0]
        self.assertEqual(set(user_dict.keys()), set(['id', 'username']), u'The resulting output returns too many'
                                                                         u'information!')
        self.assertEqual(user_dict['username'], u'bob')

    def test_solutions_01_search_by_text(self):
        ret = self.testapp.post_json('/rest/search/solutions', dict(text=u'foo'), status=200)
        self.assertEqual(len(ret.json_body['results']), 1)
        ret = self.testapp.post_json('/rest/search/solutions', dict(text=u'*DOES NOT EXIST*'), status=200)
        self.assertEqual(len(ret.json_body['results']), 0)

    def test_solutions_02_search_by_username(self):
        ret = self.testapp.post_json('/rest/search/solutions', dict(username=u'mary'), status=200)
        self.assertEqual(len(ret.json_body['results']), 1)
        self.assertEqual(ret.json_body['results'][0]['user']['username'], u'mary')
        ret = self.testapp.post_json('/rest/search/solutions', dict(username=u'bob'), status=200)
        self.assertEqual(len(ret.json_body['results']), 0)

    def test_search_unsolved_problems(self):
        ret = self.testapp.post_json('/rest/search/problems', dict(solved_for_problem_creator=False), status=200)
        # Should return all problems (four)
        self.assertEqual(len(ret.json_body['results']), 4)
        # Quick fix one problem
        self.testapp.put_json('/rest/problems/1', dict(chosen_solution_id=1,), headers=self.basic_auth('bob'), status=200)
        ret = self.testapp.post_json('/rest/search/problems', dict(solved_for_problem_creator=False), status=200)
        # Should return one less
        self.assertEqual(len(ret.json_body['results']), 3)
        # Also not appear in result list
        self.assertTrue(1 not in [problem['id'] for problem in ret.json['results']])

    def test_search_solved_problems(self):
        # Fix one problem
        self.testapp.put_json('/rest/problems/1', dict(chosen_solution_id=1,), headers=self.basic_auth('bob'), status=200)
        ret = self.testapp.post_json('/rest/search/problems', dict(solved_for_problem_creator=True), status=200)
        # Should return one problem
        self.assertEqual(len(ret.json_body['results']), 1)
        # and also appear in result list
        self.assertTrue(1 in [problem['id'] for problem in ret.json['results']])

    def test_pagination(self):
        def results(ret):
            return ret.json_body['results']
        ret = self.testapp.post_json('/rest/search/problems', dict(), status=200)
        overall_count = len(results(ret))
        self.assertEqual(overall_count, 4)
        self.assertEqual(ret.json_body['more_results'], False)
        ret = self.testapp.post_json('/rest/search/problems', dict(ALL=True), status=200)
        overall_count2 = len(results(ret))
        self.assertEqual(overall_count, overall_count2)

        ret = self.testapp.post_json('/rest/search/problems', dict(PER_PAGE=2), status=200)
        self.assertEqual(len(results(ret)), 2)
        self.assertEqual(ret.json_body['more_results'], True)

        ret = self.testapp.post_json('/rest/search/problems', dict(PER_PAGE=2, PAGE=1), status=200)
        self.assertEqual(len(results(ret)), 2)
        self.assertEqual(ret.json_body['more_results'], False)

    def test_search_problems_by_topic(self):
        # Topic 1 (with tags foo, bar) should yield the same result as the search of each tags.
        ret = self.testapp.post_json('/rest/search/problems', dict(topic=1), status=200)
        ret2 = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo', u'bar']), status=200)
        self.assertSetEqual(set([result['id'] for result in ret.json_body['results']]),
                            set([result['id'] for result in ret2.json_body['results']]))
        # Add foo -> foo-1, topics should include all sub-tags
        self.testapp.post_json('/rest/tags/foo/children', dict(name=u"foo-1"), headers=self.basic_auth('admin'),
                               status=201)
        ret = self.testapp.post_json('/rest/search/problems', dict(topic=1), status=200)
        ret2 = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo', u'bar', u'foo-1']), status=200)
        self.assertSetEqual(set([result['id'] for result in ret.json_body['results']]),
                            set([result['id'] for result in ret2.json_body['results']]))

    def test_search_problems_by_sub_tag(self):
        # Add foo -> foo-1, and a problem for foo-1
        self.testapp.post_json('/rest/tags/foo/children', dict(name=u"foo-1", description=u"blup"),
                               headers=self.basic_auth('admin'), status=201)
        self.testapp.post_json('/rest/problems/', dict(title=u"Foo-1 Problem", description=u"Beschreibung",
                                                       tags=[u"foo-1"]), headers=self.basic_auth('bob'), status=201)

        ret = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo', u'foo-1']), status=200)
        ret2 = self.testapp.post_json('/rest/search/problems', dict(tags=[u'foo'], with_sub_tags=True), status=200)
        self.assertSetEqual(set([result['id'] for result in ret.json_body['results']]),
                            set([result['id'] for result in ret2.json_body['results']]))

class TestSolutions(WebTest):
    """Testing the user related views /rest/solutions/*
    """

    def test_00_get_all_solutions(self):
        response = self.testapp.get('/rest/solutions/', status=200)
        ret = response.json
        self.assertIsInstance(ret, list)

    def test_01_get_single_solution(self):
        self.testapp.get('/rest/solutions/1', status=200)

    def test_02_does_not_exist(self):
        self.testapp.get('/rest/solutions/9999', status=404)

    def test_05_update_solution_by_collection(self):
        new_text = u"Neuer Text"
        self.testapp.put_json('/rest/solutions/1', dict(text=new_text), headers=self.basic_auth('mary'), status=200)
        ret = self.testapp.get('/rest/solutions/1', status=200)
        self.assertEqual(ret.json_body['text'], new_text)

    def test_06_update_solution_by_collection_with_empty_input(self):
        self.testapp.put_json('/rest/solutions/1', dict(text=u"",), headers=self.basic_auth('mary'), status=400)

    def test_07_update_solution_by_collection_with_no_login(self):
        new_text = u"Neuer Text"
        self.testapp.put_json('/rest/solutions/1', dict(text=new_text), status=403)

    def test_08_update_solution_by_collection_but_not_mine(self):
        new_text = u"Neuer Text"
        self.testapp.put_json('/rest/solutions/1', dict(text=new_text), headers=self.basic_auth('bob'), status=403)

    def test_09_delete_solution(self):
        self.testapp.delete('/rest/solutions/1', headers=self.basic_auth('mary'), status=200)
        self.testapp.get('/rest/solutions/1', status=404)

    def test_10_delete_solution_but_not_mine(self):
        self.testapp.delete('/rest/solutions/1', headers=self.basic_auth('bob'), status=403)

    def test_11_delete_solution_with_no_login(self):
        self.testapp.delete('/rest/solutions/1', status=403)


class TestTopics(WebTest):
    """Testing the tag related views /rest/topics/*
    """

    def test_00_get_all_topics(self):
        response = self.testapp.get('/rest/topics/', status=200)
        ret = response.json
        self.assertIsInstance(ret, list)
        for element in ret:
            self.assertTrue(element[u'name'] in [u'Foo und Bar'])

    def test_01_get_single_topic(self):
        self.testapp.get('/rest/topics/1', status=200)

    def test_02_does_not_exist(self):
        self.testapp.get('/rest/topics/9999', status=404)

    def test_03_create_topic_by_collection(self):
        ret = self.testapp.post_json('/rest/topics/', dict(name=u"Neues Topic", description=u"Beschreibung",
                                                           tags=[u"foo"]), headers=self.basic_auth('admin'), status=201)
        new_url_path = '/' + '/'.join(ret.normal_body.split()[-1].split('/')[3:])
        ret = self.testapp.get(new_url_path, status=200)
        self.assertEqual(ret.json_body['name'], u"Neues Topic")
        self.assertEqual(ret.json_body['description'], u"Beschreibung")
        self.assertEqual(ret.json_body['tags'][0]['name'], u"foo")

    def test_03_create_topic_by_collection_with_empty_input(self):
        self.testapp.post_json('/rest/topics/', dict(name=u"", description=u"", tags=[]),
                               headers=self.basic_auth('admin'), status=400)

    def test_03_create_topic_by_collection_with_non_existing_tag(self):
        self.testapp.post_json('/rest/topics/', dict(name=u"Neues Topic", description=u"Beschreibung",
                                                     tags=[u"foo", u"*****"]), headers=self.basic_auth('admin'),
                               status=400)

    def test_04_create_topic_by_collection_with_no_login(self):
        self.testapp.post_json('/rest/topics/', dict(name=u"Neuer Titel", description=u"Beschreibung",
                                                     tags=[u"foo"]), status=403)

    def test_04_create_topic_by_collection_with_user_login(self):
        self.testapp.post_json('/rest/topics/', dict(name=u"Neuer Titel", description=u"Beschreibung",
                                                     tags=[u"foo"]), status=403, headers=self.basic_auth('bob'))

    def test_05_update_topic_by_collection(self):
        new_name = u"Foo und Bar (bearbeitet)"
        new_description = u"Andere Beschreibung"
        new_tags = [u'bar', u'äöü']
        self.testapp.put_json('/rest/topics/1', dict(name=new_name, tags=new_tags, description=new_description),
                              headers=self.basic_auth('admin'), status=200)
        ret = self.testapp.get('/rest/topics/1', status=200)
        self.assertEqual(ret.json_body['name'], new_name)
        self.assertEqual(ret.json_body['description'], new_description)
        self.assertEqual(set([tag['name'] for tag in ret.json_body['tags']]), set(new_tags))
        new_tags = []
        self.testapp.put_json('/rest/topics/1', dict(tags=new_tags), headers=self.basic_auth('admin'), status=200)
        ret = self.testapp.get('/rest/topics/1', status=200)
        self.assertEqual(set([tag['name'] for tag in ret.json_body['tags']]), set(new_tags))

    def test_05_update_topic_by_collection_with_empty_input(self):
        self.testapp.put_json('/rest/topics/1', dict(name=u"",), headers=self.basic_auth('admin'), status=400)

    def test_06_update_topic_by_collection_with_no_login(self):
        new_name = u"Problemtitel fuer foo (bearbeitet)"
        self.testapp.put_json('/rest/topics/1', dict(name=new_name,), status=403)

    def test_07_delete_topic(self):
        self.testapp.delete('/rest/topics/1', headers=self.basic_auth('admin'), status=200)
        self.testapp.get('/rest/topics/1', status=404)

    def test_07_delete_topic_but_no_admin(self):
        self.testapp.delete('/rest/topics/1', headers=self.basic_auth('mary'), status=403)

    def test_08_get_tags_from_topic(self):
        ret = self.testapp.get('/rest/topics/1/tags/')
        self.assertIsInstance(ret.json_body, list)

    def test_09_bind_tag_on_topic(self):
        ret = self.testapp.get('/rest/topics/1/tags/')
        self.assertEqual(len(ret.json_body), 2)
        self.testapp.put('/rest/topics/1/tags/äöü', headers=self.basic_auth('admin'), status=201)
        ret = self.testapp.get('/rest/topics/1/tags/')
        self.assertEqual(len(ret.json_body), 3)

    def test_09_bind_tag_on_topic_with_no_login(self):
        self.testapp.put('/rest/topics/1/tags/äöü', status=403)

    def test_09_bind_tag_on_topic_but_no_admin(self):
        self.testapp.put('/rest/topics/1/tags/äöü', headers=self.basic_auth('mary'), status=403)

    def test_09_bind_non_existing_tag_on_topic(self):
        self.testapp.put('/rest/topics/1/tags/*****', headers=self.basic_auth('admin'), status=400)

    def test_09_bind_same_tag_on_topic(self):
        self.testapp.put('/rest/topics/1/tags/foo', headers=self.basic_auth('admin'), status=400)

    def test_10_unbind_tag_from_topic(self):
        self.testapp.get('/rest/topics/1/tags/foo', status=200)
        self.testapp.delete('/rest/topics/1/tags/foo', headers=self.basic_auth('admin'), status=200)
        self.testapp.get('/rest/topics/1/tags/foo', status=404)

    def test_10_unbind_tag_from_topic_with_no_login(self):
        self.testapp.delete('/rest/topics/1/tags/foo', status=403)

    def test_10_unbind_tag_from_topic_but_no_admin(self):
        self.testapp.delete('/rest/topics/1/tags/foo', headers=self.basic_auth('mary'), status=403)

    def test_10_unbind_non_existing_tag_on_topic(self):
        self.testapp.delete('/rest/topics/1/tags/******', headers=self.basic_auth('admin'), status=404)

    def test_10_unbind_all_tags_from_topic(self):
        ret = self.testapp.get('/rest/topics/1/tags/', status=200)
        self.assertGreater(len(ret.json_body), 0)
        self.testapp.delete('/rest/topics/1/tags/', headers=self.basic_auth('admin'), status=200)
        ret = self.testapp.get('/rest/topics/1/tags/', status=200)
        self.assertEqual(len(ret.json_body), 0)

    def test_10_unbind_all_tags_from_topic_with_no_login(self):
        self.testapp.delete('/rest/topics/1/tags/', status=403)

    def test_10_unbind_all_tags_from_topic_but_no_admin(self):
        self.testapp.delete('/rest/topics/1/tags/', headers=self.basic_auth('mary'), status=403)

    def test_11_delete_tag_check_if_removed(self):
        ret = self.testapp.get('/rest/topics/1/tags/', status=200)
        self.assertEqual(len(ret.json_body), 2)
        self.testapp.delete('/rest/tags/foo', headers=self.basic_auth('admin'), status=200)
        ret = self.testapp.get('/rest/topics/1/tags/', status=200)
        self.assertEqual(len(ret.json_body), 1)


class TestCBR(WebTest):

    def setup_database(self):
        # Let's solve problem foo+bar
        foo_and_bar_problem = DBSession.query(Problem).filter(Problem.title == u'Problemtitel fuer foo+bar').one()
        mary = DBSession.query(User).filter(User.username == u'mary').one()

        solution = Solution(text=u"Lösung für Problem foo+bar", user=mary)
        solution_assoc = ProblemSolutionProposal(solution=solution, user=mary)
        foo_and_bar_problem.problem_solutions_association.append(solution_assoc)
        foo_and_bar_problem.chosen_solution = solution

        transaction.commit()
        DBSession.flush()

    def setUp(self):
        """Overwritten method to include some extra conditions
        """
        super(TestCBR, self).setUp()
        self.setup_database()

    def test_01_retrieving_list(self):
        # Suggestions for foo problem
        ret = self.testapp.post_json('/rest/problems/1/suggestions', {'only_solved': False}, status=200)
        self.assertIsInstance(ret.json_body, dict)
        results = ret.json_body['results']
        self.assertIsInstance(results, list)

    def test_02_explanation_as_json(self):
        # Suggestions for foo problem
        ret = self.testapp.post_json('/rest/problems/1/suggestions', {'only_solved': False, 'explain': True},
                                     status=200)
        self.assertIsInstance(ret.json_body, dict)
        results = ret.json_body['results']
        self.assertIsInstance(results, list)
        self.assertTrue('explanation' in ret.json_body, "No explanation entry!")

    def test_03_only_solved(self):
        # Suggestions for foo problem
        ret = self.testapp.post_json('/rest/problems/1/suggestions', {'only_solved': True, 'explain': True},
                                     status=200)
        results = ret.json_body['results']
        self.assertEqual(len(results), 1)
        self.assertTrue(results[0]['problem']['title'], u'Problemtitel fuer foo+bar')