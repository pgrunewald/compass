from pyramid.authentication import BasicAuthAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import authenticated_userid

import hashlib
from os import urandom
hash_method = 'sha512'

from .models import (
    DBSession,
    User
)
from sqlalchemy.orm.exc import NoResultFound


def create_password_hash_with_salt(password, salt):
    m = hashlib.new(hash_method)
    m.update(password.encode('utf8'))
    m.update(salt)
    password_hash = m.hexdigest()
    return password_hash, salt


def create_password_hash_with_random_salt(password):
    salt = urandom(128).encode('hex')
    m = hashlib.new(hash_method)
    m.update(password.encode('utf8'))
    m.update(salt)
    password_hash = m.hexdigest()
    return password_hash, salt


def get_authenticated_user(request):
    return DBSession.query(User).filter(User.username == authenticated_userid(request)).one()


def user_check(login, password, request):
    """Returns the groups of users when being a valid member, otherwise return None."""
    try:
        user = DBSession.query(User).filter(User.username == login).one()
        password_hash, _ = create_password_hash_with_salt(password, user.password_salt)
        # TODO: Add later associated groups
        if password_hash != user.password_hash:
            return None
    except NoResultFound:
        return None
    return ['Member']

compass_authentication = BasicAuthAuthenticationPolicy(user_check, debug=True)
compass_authorization = ACLAuthorizationPolicy()