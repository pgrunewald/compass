# coding=utf-8
from __future__ import division
import math
import itertools
from compass.models import Tag, DBSession
from nltk.corpus import stopwords
from nltk.stem.snowball import GermanStemmer, EnglishStemmer
from nltk.tokenize import RegexpTokenizer


class SortedLimitedDict(object):
    """Sorted dictionary which stores limited element-value-pairs (count n).
    """

    def __init__(self, n=5):
        """
        :param n: limit of the pairs to be stored
        """
        self.n = n
        self.l = []
        self.d = {}

    def append(self, element, value):
        """Append the element-value pair. Do nothing if it already worse than the current lowest pair.
        :returns: Item dropped when the dictionary has reached its limit; otherwise it will return None.
        """

        if self.l and len(self.l) == self.n and value < self.d[self.l[-1]]:
            return

        if not self.l:
            self.l.append(element)
            self.d[element] = value
            return

        inserted = False
        for index, elem in enumerate(self.l):
            if value > self.d[elem]:
                self.l.insert(index, element)
                self.d[element] = value
                inserted = True
                break

        if len(self.l) > self.n:
            removed_element = self.l[-1]
            self.d[self.l[-1]]
            del self.d[self.l[-1]]
            del self.l[-1]
            return removed_element
        else:
            if not inserted:
                self.l.append(element)
                self.d[element] = value

    def __repr__(self):
        return "SortedLimitedDict(%s)" % self.l.__repr__()

    def get_elements(self):
        """Return elements, sorted by their descending value
        """
        return self.l

    def get_pairs(self):
        """Return element-value-pairs, sorted by their descending value
        """
        return [(element, self.d[element],) for element in self.l]

    def __len__(self):
        return len(self.l)


class TextNormalizer(object):
    """This class will perform on arbitrary strings containing several sentences to normalize all words.
    The following steps are being performed:
     1. Tokenize all words
     2. From that set filter stop words
     3. Lemmatize the remaining words
     4. Concatenate the lemmatized words.
    """
    iso2nltk_language_mapping = {'de': 'german',
                                 'en': 'english'}

    def __init__(self, language='de'):
        self.nltk_language = self.iso2nltk_language_mapping.get(language, 'german')
        # Using word tokenizer based on regular expressions for performance reasons
        self.word_tokenizer = RegexpTokenizer('\w+|\$[\d\.]+|\S+')
        self.stopwords = set(stopwords.words(self.nltk_language))
        if self.nltk_language == 'german':
            self.stemmer = GermanStemmer()
        else:
            self.stemmer = EnglishStemmer()

    def __call__(self, text):
        """Returns normalized text.
        :param text:
        :return: unicode
        """
        words = self.word_tokenizer.tokenize(text)
        words = [word for word in words if word not in self.stopwords]
        words = map(lambda word: self.stemmer.stem(word), words)
        return u' '.join(words)


def ngram(text_unit, gram_length):
    """Compute set of ngrams of text_units (e.g characters or words)
    """
    return set([text_unit[i:i+gram_length] for i in xrange(len(text_unit)-(gram_length-1))])


def dice(s1, s2):
    """Compute Sorensen–Dice coefficient based on two sets.
    """
    if len(s1) == len(s2) == 0:
        return 0.0
    if s1 == s2:
        return 1.0
    return (2.0 * len(s1 & s2)) / (len(s1)+len(s2))


class IOfferExplanation(object):
    explanation = dict({'text': u'', 'facts': dict()})
    explain = False

    def get_description(self, language='en'):
        """Returns a brief description of this metric in the designated language.
        :param language: ISO language code
        :return: description
        :rtype: unicode
        """
        raise NotImplementedError('please implement the get_description method of class {}!'.format(self.__name__))

    def get_explanation(self, language='en'):
        """Returns the explanation of the last similarity measurement.
        :param language: ISO language code
        :return: explanation dict
        :rtype : dict
        """
        return self.explanation

    def _reset_explanation(self):
        """To be used internally.
        :return: None
        """
        if self.explain:
            self.explanation = dict({'text': u'', 'facts': dict()})

    def _add_fact(self, **fact):
        """To be used internally.
        :return: None
        """
        if self.explain:
            self.explanation['facts'].update(**fact)


class LocalSimilarityMetric(IOfferExplanation):
    """Local similarity metric class to compare attributes of problems. Optionally this allows
    """

    def __call__(self, attribute1, attribute2):
        """Calculates the similarity between two attributes. If explain is True, it will also offer the explanation
        of its process.

        :param attribute1: first attribute
        :param attribute2: second attribute
        :return: Similarity between two sets in interval [0,1]
        :rtype : float
        """
        raise NotImplementedError('please implement the __call__ method of class {}!'.format(self.__name__))


class DiceSimilarity(LocalSimilarityMetric):
    """Computes Sorensen–Dice coefficient based on two sets.
    """
    ABORT_REASONS_ONE_EMPTY, ABORT_REASON_BOTH_EQUAL = range(2)

    def __init__(self, explain=False):
        self.explain = explain

    def __call__(self, attribute1, attribute2):
        self._reset_explanation()

        def _dice_explained(s1, s2):
            if len(s1) == 0 or len(s2) == 0:
                self._add_fact(abort_reason=self.ABORT_REASONS_ONE_EMPTY)
                self._add_fact(similarity=0.0)
                return 0.0
            if s1 == s2:
                self._add_fact(abort_reason=self.ABORT_REASON_BOTH_EQUAL)
                self._add_fact(similarity=1.0)
                return 1.0

            matches = s1 & s2
            max_matches = len(s1)+len(s2)

            self._add_fact(matches=len(matches))
            self._add_fact(max_matches=max_matches)
            return (2.0 * len(matches)) / max_matches

        sim = _dice_explained(attribute1, attribute2)
        self._add_fact(similarity=sim)
        return sim

    def get_description(self, language='en'):
        if language == 'de':
            return u"""Berechnet die syntaktische Übereinstimmung von zwei Texten basierend auf den Sørensen–Dice-
            Koeffizienten."""
        else:
            return u"""Calculates the syntactical overlap of characters between two texts based on the Sørensen–Dice
            coefficient."""

    def get_explanation(self, language='en'):
        if not self.explain:
            raise Exception(u"In order to call this method, you have to set 'explain' to True and call this class at "
                            u"least once.")

        if language == 'de':
            if self.explanation['facts'].get('abort_reason') == self.ABORT_REASONS_ONE_EMPTY:
                self.explanation['text'] = u"Keine Ähnlichkeit, da einer der Texte leer sind."
            elif self.explanation['facts'].get('abort_reason') == self.ABORT_REASON_BOTH_EQUAL:
                self.explanation['text'] = u"Beide Texte sind identisch."
            else:
                self.explanation['text'] = u"Die Texte haben eine syntaktische Übereinstimmung mit einem "\
                                           u"Sørensen–Dice-Koeffizienten von {similarity:.3f} bei {matches:d} von " \
                                           u"{max_matches:d} möglichen Treffern."
        else:
            if self.explanation['facts'].get('abort_reason') == self.ABORT_REASONS_ONE_EMPTY:
                self.explanation['text'] = u"No similarity, because no one of the texts is empty."
            elif self.explanation['facts'].get('abort_reason') == self.ABORT_REASON_BOTH_EQUAL:
                self.explanation['text'] = u"Both texts are identical."
            else:
                self.explanation['text'] = u"Both texts have a syntactical overlap with a Sørensen–Dice coefficient of"\
                                           u" {similarity:.3f} with {matches:d} out of {max_matches:d} matches."
        self.explanation['text'] = self.explanation['text'].format(**self.explanation['facts'])

        return self.explanation


class AncestorDict(dict):
    """Dict, which returns the cached ancestors. On miss it will be computed."""

    def __missing__(self, tag):
        value = self[tag] = tag.get_ancestors()
        return value


class TagSetSimilarity(LocalSimilarityMetric):
    """Class to calculate the similarity between tags, which themselves are integrated within a taxonomy.
    Metric being used can be found in on Lin's paper "An Information-Theoretic Definition of Similarity"
    and works of Haiying Wang, Francisco Azuaje and Olivier Bodenreider.
    """

    STRATEGY_AVERAGE = 1
    STRATEGY_MAX_AVERAGE = 2

    ABORT_REASONS_BOTH_EMPTY,\
    ABORT_REASONS_ONE_EMPTY\
        = range(2)

    def __init__(self, strategy=STRATEGY_AVERAGE, explain=False):
        self.strategy = strategy
        self.ancestors = AncestorDict()
        self.explain = explain
        self.single_similarities = dict()

    # Helper for the explanation component
    def _tag_names(self, tags):
        return [tag.name for tag in tags]

    def _state_similarity(self, tag1, tag2, sim, lcs=None):
        if self.explain:
            lcs_name = None
            if lcs:
                lcs_name = lcs.name
            self.single_similarities.update({(tag1.name, tag2.name): (sim, lcs_name,)})

    def _have_common_ancestor(self, tag1, tag2):
        """Checks if two tags have the same ancestor (without counting the virtual root)
        :param tag1:
        :param tag2:
        :return:
        :rtype : bool
        """
        tag1_with_ancestors = [tag1] + self.ancestors[tag1]
        tag2_with_ancestors = [tag2] + self.ancestors[tag2]
        return tag1_with_ancestors[-1] is tag2_with_ancestors[-1] and not Tag.is_virtual_root(tag1_with_ancestors[-1])

    def similarity(self, tag1, tag2):
        """Calculates the similarity between two tags.
        :param tag1: first tag.
        :param tag2: second tag.
        :return: similarity between two tags in interval [0,1]
        :rtype : float"""

        # Easy decision to save cpu power (metric would still work even without this condition)
        if tag1 is tag2:
            self._state_similarity(tag1, tag2, sim=1.0, lcs=tag1)
            return 1.0

        tag1_with_ancestors = [tag1] + self.ancestors[tag1]
        tag2_plus_ancestors = [tag2] + self.ancestors[tag2]

        # No similarity when having not the same root:
        if not self._have_common_ancestor(tag1, tag2):
            self._state_similarity(tag1, tag2, sim=0.0, lcs=None)
            return 0.0

        # Now find the subsuming ancestor which shares the most similarity (most information) of both tags
        common_ancestors = set(tag1_with_ancestors).intersection(tag2_plus_ancestors)

        subsuming_ancestor = max(common_ancestors, key=lambda tag: tag.log_probability_of_instance)

        # Sanity check
        if subsuming_ancestor.log_probability_of_instance is None:
            raise Exception("Error when accessing attribute 'log_probability_of_instance' of this instance of Tag."
                            "All tags are required to have a probability in order to work. Consider using"
                            "compass.cbr.util.build_tag_trees_uniform_distribution")

        sim = 2.0 * subsuming_ancestor.log_probability_of_instance / (tag1.log_probability_of_instance +
                                                                      tag2.log_probability_of_instance)
        self._state_similarity(tag1, tag2, sim=sim, lcs=subsuming_ancestor)
        return sim

    def __call__(self, tags1, tags2):
        """Calculates the similarity based (on a strategy) of two tag sets.
        :param tags1: Tags of first problem.
        :param tags2: Tags of second problem.
        :return: Similarity between two sets in interval [0,1]
        :rtype : float
        """
        self._reset_explanation()
        self.single_similarities = dict()
        self._add_fact(strategy_used=self.strategy)

        # Pre-Condition: Both sets are not empty
        if len(tags1) + len(tags2) == 0:
            self._add_fact(abort_reason=self.ABORT_REASONS_BOTH_EMPTY)
            self._add_fact(similarity=1.0)
            return 1.0
        if len(tags1) == 0 or len(tags2) == 0:
            self._add_fact(abort_reason=self.ABORT_REASONS_ONE_EMPTY)
            self._add_fact(similarity=0.0)
            return 0.0

        # Average score
        if self.strategy == self.STRATEGY_AVERAGE:
            sim = 0.0
            for tag1, tag2 in itertools.product(tags1, tags2):
                sim += self.similarity(tag1, tag2)
            sim *= 1.0 / len(tags1) / len(tags2)
            self._add_fact(similarity=sim)
            self._add_fact(single_similarities=self.single_similarities)
            return sim
        # Take for each tag the most similar partner and then the average of all.
        elif self.strategy == self.STRATEGY_MAX_AVERAGE:
            sim = 0.0
            for tag1 in tags1:
                sim += max([self.similarity(tag1, tag2) for tag2 in tags2])
            for tag2 in tags2:
                sim += max([self.similarity(tag1, tag2) for tag1 in tags1])
            sim *= 1.0 / (len(tags1) + len(tags2))
            self._add_fact(similarity=sim)
            self._add_fact(single_similarities=self.single_similarities)
            return sim

        raise Exception("Could not compute similarity. No strategy set?")

    def get_explanation(self, language='en'):
        if not self.explain:
            raise Exception(u"In order to call this method, you have to set 'explain' to True and call this class at "
                            u"least once.")

        from itertools import groupby

        if self.explanation['facts'].get('abort_reason') == self.ABORT_REASONS_BOTH_EMPTY:
            if language == 'de':
                self.explanation['text'] = u"Beide Probleme haben keine Tags und damit identisch."
            else:
                self.explanation['text'] = u"No tags in both problems and thus identical."
        elif self.explanation['facts'].get('abort_reason') == self.ABORT_REASONS_ONE_EMPTY:
            if language == 'de':
                self.explanation['text'] = u"Eines der Probleme hat keine Tags. Damit haben sie keine Ähnlichkeit."
            else:
                self.explanation['text'] = u"One of the problems have no tags and thus no in common."
        else:
            self.explanation['facts']['single_similarities'] = self.single_similarities
            sentences = []
            if language == 'de':
                text = u"Die Tags der beiden Probleme haben eine durchschnittliche Ähnlichkeit von {similarity:.2f}."
            else:
                text = u"Both problem's tags have an average similarity of {similarity:.2f}."
            sentences.append(text.format(**self.explanation['facts']))
            # pick three most similar tags
            pairs = sorted(self.explanation['facts']['single_similarities'].items(), key=lambda ((_tag1, _tag2),
                                                                                                (sim, _lcs)): sim)
            pairs_full = filter(lambda ((_tag1, _tag2), (sim, _lcs)): sim == 1.0, pairs)
            pairs_null = filter(lambda ((_tag1, _tag2), (sim, _lcs)): sim == 0.0, pairs)
            pairs_sorted = pairs[:]
            for pair in pairs_full + pairs_null:
                pairs_sorted.remove(pair)
            # Detect tags which have no matching partner.
            left_handed_no_partner = set()
            right_handed_no_partner = set()
            get_first_tag = lambda ((_tag1, _tag2), (sim, _lcs)): _tag1
            get_second_tag = lambda ((_tag1, _tag2), (sim, _lcs)): _tag1
            for k, g in groupby(sorted(pairs, key=get_first_tag), key=get_first_tag):
                pairs_with_tag = list(g)
                if sum([sim for ((_tag1, _tag2), (sim, _lcs)) in pairs_with_tag]) == 0.0:
                    left_handed_no_partner.add(k)
            for k, g in groupby(sorted(pairs, key=get_second_tag), key=get_second_tag):
                pairs_with_tag = list(g)
                if sum([sim for ((_tag1, _tag2), (sim, _lcs)) in pairs_with_tag]) == 0.0:
                    right_handed_no_partner.add(k)
            no_matching_tags = left_handed_no_partner & right_handed_no_partner

            def _quote(text):
                return u'"{:s}"'.format(text)

            if language == 'de':
                if pairs_full:
                    sentences.append(u"Gemeinsame Tags: " + u", ".join([_quote(tag1) for ((tag1, _tag2), (_sim, _lcs)) in pairs_full]))
                else:
                    sentences.append(u"Es werden keine gemeinsamen Tags verwendet.")
                if no_matching_tags:
                    sentences.append(u"Keine Übereinstimmung gefunden für die Tags: " + u", ".join([_quote(tag) for tag in no_matching_tags]))
                if pairs_sorted:
                    sentences.append(u"Folgende Tags sind verwandt:")
                    for ((tag1, tag2), (sim, lcs)) in pairs_sorted:
                        sentences.append(u"{} und {} über {} (Ähnlichkeit: {:.2f})".format(_quote(tag1), _quote(tag2), _quote(lcs), sim))
            else:
                if pairs_full:
                    sentences.append(u"Common tags: " + u", ".join([_quote(tag1) for ((tag1, _tag2), (_sim, _lcs)) in pairs_full]))
                else:
                    sentences.append(u"There are no common tags.")
                if no_matching_tags:
                    sentences.append(u"No relation for the following tags found: " + u", ".join([_quote(tag) for tag in no_matching_tags]))
                if pairs_sorted:
                    sentences.append(u"Relationship between the following tags:")
                    for ((tag1, tag2), (sim, lcs)) in pairs_sorted:
                        sentences.append(u"- {} and {} over {} (similarity: {:.2f})".format(_quote(tag1), _quote(tag2), _quote(lcs), sim))
            self.explanation['text'] = u"\n".join(sentences)

        # Some post processing
        # Convert single_similarities into a list of dicts

        if 'single_similarities' in self.explanation['facts']:
            as_list = []
            for ((tag1, tag2), (sim,lcs)) in self.explanation['facts']['single_similarities'].items():
                as_list.append({'tag1': tag1, 'tag2': tag2, 'similarity': sim, 'least_common_subsumer': lcs})
            self.explanation['facts']['single_similarities'] = as_list

        return self.explanation


def build_tag_trees_uniform_distribution(unify=False):
    """Returns the current tag tree enriched with their distribution information (which is uniform) AND a dictionary to
    lookup the decorated tags (key: id).
    :param unify: determines whether all root tags should treated as children of a single virtual root node.
    :return: tuple of list of tags and dict
    """

    trees = Tag.get_trees()
    lookup = dict()

    def _set_probabilities(tag):
        num_children = len(tag.children)

        if not num_children:
            return

        probability_of_child = tag.log_probability_of_instance - math.log(num_children)

        for child in tag.children:
            lookup[child.id] = child
            child.log_probability_of_instance = probability_of_child
            _set_probabilities(child)

    # Initialize each probability

    # Unify? Build a single tree with a virtual root node
    if unify:
        # Create virtual root via special constructor
        virtual_root = Tag.get_virtual_root()
        # Chance of occurring this node is 100%
        virtual_root.log_probability_of_instance = math.log(1.0)
        lookup[virtual_root.id] = virtual_root
        # Add all previous root nodes to our singleton root node
        virtual_root.children.extend(trees)
        _set_probabilities(virtual_root)
        trees = [virtual_root]
        # Remove virtual root from session to prevent side effects (there are no reasons to store it in database)
        DBSession.expunge(virtual_root)
    else:
        # Treat each tag as separate tree root
        for root_tag in trees:
            lookup[root_tag.id] = root_tag
            root_tag.log_probability_of_instance = math.log(1.0)
            _set_probabilities(root_tag)
    return trees, lookup
