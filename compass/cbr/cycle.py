# coding=utf-8
from sqlalchemy import exists
from sqlalchemy.orm import aliased
from compass.models import DBSession
from compass.models import (
    Problem,
    ProblemSolutionProposal
)
from compass.cbr.util import (
    SortedLimitedDict,
    ngram,
    build_tag_trees_uniform_distribution,
    TagSetSimilarity,
    DiceSimilarity,
    IOfferExplanation,
    TextNormalizer)

GRAM_LENGTH = 4


class ProblemQuery(object):

    def __init__(self, instance, explain=False):
        self.instance = instance
        self.explanation_language = 'de'

        trees, lookup = build_tag_trees_uniform_distribution(unify=True)
        tagset_sim = TagSetSimilarity(strategy=TagSetSimilarity.STRATEGY_MAX_AVERAGE)
        dice = DiceSimilarity()
        normalizer = TextNormalizer(language='de')

        self.relevant_attributes = {
            'title': {
                'weight': 0.2,
                'convert_func': lambda text: ngram(normalizer(text), gram_length=GRAM_LENGTH),
                'local_sim_func': dice
            },
            'description': {
                'weight': 0.5,
                'convert_func': lambda text: ngram(normalizer(text), gram_length=GRAM_LENGTH),
                'local_sim_func': dice
            },
            'tags': {
                'weight': 0.3,
                'convert_func': lambda tags: [lookup[tag.id] for tag in tags],
                'local_sim_func': tagset_sim,
            },
        }
        self.normalize_weights()
        self.explain = explain

    def get_weights(self):
        return dict([(attribute, attribute_values['weight'],) for attribute, attribute_values in self.relevant_attributes])

    def update_weights(self, new_weight_dict):
        for attribute, weight in new_weight_dict.items():
            if self.relevant_attributes.get(attribute):
                self.relevant_attributes[attribute]['weight'] = weight
        self.normalize_weights()

    def normalize_weights(self):
        factor = 1.0 / sum([value['weight'] for value in self.relevant_attributes.values()])
        for attribute, attribute_values in self.relevant_attributes.items():
            self.relevant_attributes[attribute]['weight'] = attribute_values['weight'] * factor

    def __getitem__(self, item):
        if hasattr(self.instance, item):
            return getattr(self.instance, item)
        else:
            return self.instance[item]

    def __iter__(self):
        return self.instance.__iter__()

    @property
    def explain(self):
        """Returns explain flag. True, if all similarity class are set to explain.
        :rtype: bool
        """
        return all([attribute['local_sim_func'].explain for attribute in self.relevant_attributes.values()])

    @explain.setter
    def explain(self, value):
        for attribute in self.relevant_attributes.values():
            attribute['local_sim_func'].explain = value


class CBRCycle(IOfferExplanation):

    query_object = None

    def __init__(self):
        """Init a Case-Based Reasoning Cycle.

        :type explain: bool
        :param explain: Determines whether to document the retrieving process for the end user.
        """
        self.constraints = dict()
        self.explanation = dict()
        self.query_representation = dict()

    def set_constraints(self, constraints={}):
        """Set constraints
        :type constraints: dict
        :param constraints: additional constraints
        :return:
        """
        assert isinstance(constraints, dict)
        self.constraints = constraints

    def set_input(self, query_object):
        """Defines the input query.
        :type query_object: ProblemQuery
        :param query_object: problem query instance to be used to find most similar problems.
        """
        self.query_object = query_object
        if self.query_object.explain:
            self.explain = True

    def retrieve_cases(self, n):
        """Retrieve the n most similar problems and their similarities.
        :type n: int
        :param n: number of problems

        :rtype : list of (Problem, float)
        """
        if not self.query_object:
            raise ValueError("Please use input() to initialize the query_object.")

        # Clear previous explanatory history
        self._reset_explanation()

        relevant_attributes = self.query_object.relevant_attributes
        self._add_fact(relevant_attributes=relevant_attributes)

        best_results = SortedLimitedDict(n)

        # Building representation of query
        query_representation = dict()

        for attribute, config in relevant_attributes.items():
            query_representation[attribute] = config.get('convert_func')(self.query_object[attribute])
        #self._add_fact(query_representation=query_representation)

        # Define which problem set is going to looked:
        alias_problem = aliased(Problem)
        problem_query = DBSession.query(alias_problem)
        # Exclude own problem from the result set
        problem_query = problem_query.filter(alias_problem.id != self.query_object['id'])
        # If demanded, include only problems which are solved.
        if self.constraints.get('only_solved'):
            problem_query = problem_query.filter(alias_problem.solved_for_problem_creator.is_(True))

        # If demanded, include only problems which have a problem solution
        if self.constraints.get('with_solution_proposals'):
            proposals = exists().where(ProblemSolutionProposal.problem_id == alias_problem.id)
            problem_query = problem_query.filter(proposals)

        self._add_fact(only_solved=self.constraints.get('only_solved', False))
        problem_explanation = dict()

        for problem in problem_query:
            # Building a representation of the problem
            problem_representation = dict()

            if self.explain:
                problem_explanation[problem.id] = dict()
                problem_explanation[problem.id]['attributes'] = dict()

            sim = 0.0
            # For each relevant attribute
            for attribute, config in relevant_attributes.items():

                if self.explain:
                    problem_explanation[problem.id]['attributes'][attribute] = dict()

                if config.get('weight'):
                    # .. apply the conversion
                    problem_representation[attribute] = config.get('convert_func')(getattr(problem, attribute))
                    # .. perform the weighted similarity function
                    sim += config.get('weight') * config.get('local_sim_func')(problem_representation[attribute],
                                                                               query_representation[attribute])
                    # .. (optionally) get explanatory details
                    if self.explain:
                        problem_explanation[problem.id]['attributes'][attribute] = config.get('local_sim_func').\
                            get_explanation(self.query_object.explanation_language)

            dropped_problem = best_results.append(problem, sim)

            # When a problem drops out, also drop its explanatory details
            if self.explain and dropped_problem:
                del problem_explanation[dropped_problem.id]

        if self.explain:
            self._add_fact(problem_explanation=problem_explanation)
            self._add_fact(best_results=best_results.get_pairs())

        # Return n most similar results
        return best_results.get_pairs()

    def get_description(self, language='en'):
        if language == 'de':
            return u"Es werden ähnliche Probleme darüber gefunden, indem die Ähnlichkeit ihrer Kriterien " \
                   u"ermittelt wird. Anschließend werden diese Kriterien nach ihrer Bedeutung gewichtet und " \
                   u"zu einem einzelnen Ähnlichkeitswert errechnet. Zu den Kritieren gehören Titel, " \
                   u"Problembeschreibung und die Tags eines Problems."
        else:
            return u"Similar problems are found by checking the similarities of their criteria. Those values will " \
                   u"be weighted and yield a single similarity value. The criteria being examined are title, " \
                   u"description and tags of a problem."

    def get_explanation(self, language='en'):
        """Returns the explanation of the last retrieval.
        :param language: ignored here. Assuming the same language as query_object's.
        :return: explanation dict
        :rtype : dict
        """
        if not self.query_object or not self.explain:
            raise Exception(u"In order to call this method, you have to enable 'explain' in the query_object and "
                            u"call retrieve_cases.")

        language = self.query_object.explanation_language

        def _quote(text):
            return u'"{:s}"'.format(text)

        sentences = []
        sorted_attributes = sorted(self.explanation['facts']['relevant_attributes'].items(),
                                   key=lambda (attribute_name, attribute_properties): attribute_properties['weight'],
                                   reverse=True)

        if language == 'de':
            translation_strings = {'title': u'Titel',
                                   'description': u'Beschreibung',
                                   'tags': u'Tags'}
            parts = []
            for attribute_name, attribute_properties in sorted_attributes:
                parts.append(u"{} zu {:d}%".format(translation_strings[attribute_name],
                                                    int(attribute_properties['weight'] * 100.0)))
            text = u', '.join(parts[:-1]) + u' und {}'.format(parts[-1])
            sentences.append(u"Die einzelnen Ähnlichkeiten gehen wie folgt ein: {}.".format(text))
            sentences.append(u"")
            sentences.append(u"Es wurden {:d} Ergebnisse gefunden:".format(len(self.explanation['facts']['best_results'])))
            sentences.append(u"")
            for index, (problem, problem_similarity) in enumerate(self.explanation['facts']['best_results']):
                sentences_inner = []
                problem_explanation = self.explanation['facts']['problem_explanation'][problem.id]
                sentences_inner.append(u"{:d}. Treffer: {} mit einer Ähnlichkeit von {:.1f}%".format(int(index+1),
                                                                                              _quote(problem.title),
                                                                                              problem_similarity * 100.0))
                sentences_inner.append(u"")
                sentences_inner.append(u"Aufschlüsselung der einzelnen Kriterien:")
                for attribute_name, attribute_properties in sorted_attributes:
                    sentences_inner.append(u"")
                    sentences_inner.append(u"Ähnlichkeit für {}: {:.1f}%".format(
                        translation_strings[attribute_name],
                        problem_explanation['attributes'][attribute_name]['facts']['similarity'] * 100.0))
                    sentences_inner.append(u"Erklärung:")
                    sentences_inner.append(u"{}".format(problem_explanation['attributes'][attribute_name]['text']))
                self.explanation['facts']['problem_explanation'][problem.id]['text'] = u"\n".join(sentences_inner)
                sentences.extend(sentences_inner)
                sentences.append(u"")
        else:
            translation_strings = {'title': u'Title',
                                   'description': u'Description',
                                   'tags': u'Tags'}
            parts = []
            for attribute_name, attribute_properties in sorted_attributes:
                parts.append(u"{} with {:d}%".format(translation_strings[attribute_name],
                                                     int(attribute_properties['weight'] * 100.0)))
            text = u', '.join(parts[:-1]) + u' and {}'.format(parts[-1])
            sentences.append(u"Individual similarity contributions are: {}".format(text))
            sentences.append(u"")
            sentences.append(u"{:d} matches found:".format(len(self.explanation['facts']['best_results'])))
            sentences.append(u"")
            for index, (problem, problem_similarity) in enumerate(self.explanation['facts']['best_results']):
                sentences_inner = []
                problem_explanation = self.explanation['facts']['problem_explanation'][problem.id]
                sentences_inner.append(u"{:d}. match: {} with a similarity of {:.1f}%".format(int(index+1),
                                                                                              _quote(problem.title),
                                                                                              problem_similarity * 100.0))
                sentences_inner.append(u"")
                sentences_inner.append(u"Breakdown of the criteria:")
                for attribute_name, attribute_properties in sorted_attributes:
                    sentences_inner.append(u"")
                    sentences_inner.append(u"Similarity of {}: {:.1f}%".format(
                        translation_strings[attribute_name],
                        problem_explanation['attributes'][attribute_name]['facts']['similarity'] * 100.0))
                    sentences_inner.append(u"Explanation:")
                    sentences_inner.append(u"{}".format(problem_explanation['attributes'][attribute_name]['text']))
                self.explanation['facts']['problem_explanation'][problem.id]['text'] = u"\n".join(sentences_inner)
                sentences.extend(sentences_inner)
                sentences.append(u"")

        self.explanation['text'] = u'\n'.join(sentences)
        # Purge some unnecessary information from relevant_attributes
        for attribute_name, attribute_properties in self.explanation['facts']['relevant_attributes'].items():
            for key, value in attribute_properties.items():
                if key is not 'weight':
                    del self.explanation['facts']['relevant_attributes'][attribute_name][key]

        return self.explanation
