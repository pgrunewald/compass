# coding=utf-8
import unittest
import transaction
from compass.cbr.cycle import SortedLimitedDict
from compass.cbr.util import ngram, dice
from pyramid import testing
from compass.models import (
    DBSession,
    Base,
    User,
    Problem,
    Tag,
    Solution,
    ProblemSolutionProposal,
)
from compass.cbr.cycle import (
    ProblemQuery,
    CBRCycle
)


class TestDataStructuresAndUtils(unittest.TestCase):

    def test_sorted_limited_dict(self):
        best_values = SortedLimitedDict(n=5)
        best_values.append('A', 100)
        self.assertEqual(best_values.get_elements(), ['A'])
        best_values.append('B', 200)
        self.assertEqual(best_values.get_elements(), ['B', 'A'])
        best_values.append('C', 150)
        self.assertEqual(best_values.get_elements(), ['B', 'C', 'A'])
        best_values.append('D', 50)
        self.assertEqual(best_values.get_elements(), ['B', 'C', 'A', 'D'])
        best_values.append('E', 25)
        self.assertEqual(best_values.get_elements(), ['B', 'C', 'A', 'D', 'E'])
        best_values.append('F', 0)
        self.assertEqual(best_values.get_elements(), ['B', 'C', 'A', 'D', 'E'])
        best_values.append('F', 999)
        self.assertEqual(best_values.get_elements(), ['F', 'B', 'C', 'A', 'D'])
        best_values.append('G', -999)
        self.assertEqual(best_values.get_elements(), ['F', 'B', 'C', 'A', 'D'])
        best_values.append('H', 999)
        self.assertEqual(best_values.get_elements(), ['F', 'H', 'B', 'C', 'A'])
        self.assertEqual(best_values.get_pairs(), [('F', 999), ('H', 999), ('B', 200), ('C', 150), ('A', 100)])

    def test_ngrams(self):
        self.assertEqual(set(ngram("Hello World", 4)), set(["Hell", "ello", "llo ", "lo W", "o Wo", " Wor", "Worl",
                                                            "orld"]))
        self.assertEqual(set(ngram("Hel", 4)), set([]))
        self.assertEqual(set(ngram("Hell", 4)), set(["Hell"]))

    def test_dice(self):
        a = set(ngram("Hello World", 4))
        b = set(ngram("Hallo Welt", 4))  # 2 matches here
        self.assertEqual(dice(a, b), (2.0 * 2) / (8 + 7))

    def test_query_object_test_explanation(self):
        from compass.cbr.util import DiceSimilarity
        a = set(ngram("Hello World", 4))
        b = set(ngram("Hallo Welt", 4))  # 2 matches here

        metric = DiceSimilarity(explain=True)
        similarity = metric(a, b)
        explanation = metric.get_explanation()
        self.assertEqual(similarity, explanation['facts'].get('similarity'))

    def test_text_normalizer(self):
        from compass.cbr.util import TextNormalizer
        normalizer = TextNormalizer(language='de')
        text = u"""Auch gibt es niemanden, der den Schmerz an sich liebt, sucht oder wünscht, nur, weil er Schmerz
         ist, es sei denn, es kommt zu zufälligen Umständen, in denen Mühen und Schmerz ihm große Freude bereiten können
         . Um ein triviales Beispiel zu nehmen, wer von uns unterzieht sich je anstrengender körperlicher"""
        normalized_text = normalizer(text)
        self.assertTrue(len(normalized_text) < len(text), 'Normalized should be shorter than the original text.')


class TestCBRCycle(unittest.TestCase):

    name2id = None

    def setup_dummy_data(self):
        from compass.security import create_password_hash_with_salt
        password_hash, salt = create_password_hash_with_salt(password='dummy-password', salt='dummy-salt')
        # Setup dummy data
        bob = User(username=u'bob', email=u'bob@mail.com', password_hash=password_hash, password_salt=salt)
        mary = User(username=u'mary', email=u'mary@mail.com', password_hash=password_hash, password_salt=salt)

        # Building hierarchy:
        # 1st:
        # foo -> foo-1 -> foo-1-1
        #              -> foo-1-2 -> foo-1-2-1
        #                         -> foo-1-2-2
        #              -> foo-1-3
        #     -> foo-2 -> foo-2-1
        #              -> foo-2-2
        # 2nd:
        # bar -> bar-1
        #     -> bar-2

        foo = Tag(u'foo')
        # Left branch
        foo_1, foo_1_1, foo_1_2, foo_1_3, foo_1_2_1, foo_1_2_2 = Tag(u'foo-1'), Tag(u'foo-1-1'), Tag(u'foo-1-2'), \
                                                                 Tag(u'foo-1-3'), Tag(u'foo-1-2-1'), Tag(u'foo-1-2-2')
        foo_1.children.extend([foo_1_1, foo_1_2, foo_1_3])
        foo_1_2.children.extend([foo_1_2_1, foo_1_2_2])
        # Right branch
        foo_2, foo_2_1, foo_2_2 = Tag(u'foo-2'), Tag(u'foo-2-1'), Tag(u'foo-2-2')
        foo_2.children.extend([foo_2_1, foo_2_2])
        foo.children.extend([foo_1, foo_2])

        bar = Tag(u'bar')
        bar_1, bar_2 = Tag(u'bar-1'), Tag(u'bar-2')
        bar.children.extend([bar_1, bar_2])

        problem1 = Problem(title=u'Problemtitel fuer foo', description=u'Beschreibung fuer Problem foo',
                           tags=[foo], user=bob)

        solution1 = Solution(text=u"Lösung für Problem foo", user=mary)
        solution1_assoc = ProblemSolutionProposal(solution=solution1, user=mary)
        problem1.problem_solutions_association.append(solution1_assoc)
        DBSession.add_all([problem1, bob, mary, foo, bar, solution1])
        DBSession.flush()

    def setUp(self):
        self.config = testing.setUp()

        from sqlalchemy import create_engine
        # Memory database
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        self.setup_dummy_data()

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_query_object_weight_one(self):
        from random import randint
        problem = DBSession.query(Problem).get(1)
        query_object = ProblemQuery(problem)
        # Initial weights sum is 1
        self.assertAlmostEqual(sum([value['weight'] for value in query_object.relevant_attributes.values()]), 1.0)
        # Change weights randomly
        new_weights = dict()
        for attribute, values in query_object.relevant_attributes.items():
            new_weights[attribute] = values['weight'] + randint(0, 100)
        query_object.update_weights(new_weights)

        self.assertAlmostEqual(sum([value['weight'] for value in query_object.relevant_attributes.values()]), 1.0)

    def test_query_object_in_cycle_identical(self):
        # Convert a problem to dict representation and check if it is identical to itself.
        cycle = CBRCycle()
        problem = DBSession.query(Problem).filter(Problem.id == 1).one()
        query_dict = {'title': problem.title, 'description': problem.description, 'id': None, 'tags': problem.tags}
        query_object = ProblemQuery(query_dict)
        query_object.explain = True
        cycle.set_input(query_object)
        cases = cycle.retrieve_cases(n=5)

        first_problem, first_similarity = cases[0]
        for attribute in ['title', 'description']:
            self.assertEqual(query_dict[attribute], getattr(first_problem, attribute))
        self.assertEqual(first_similarity, 1.0)

    def get_tag(self, lookup, name):
        """Helper function to get a Tag by its name
        :param lookup: lookup dictionary
        :param name: name of tag
        :return: tag
        :rtype: Tag
        """
        if self.name2id is None:
            self.name2id = dict(DBSession.query(Tag.name, Tag.id).all())
        return lookup[self.name2id[name]]
        #return lookup[DBSession.query(Tag.id).filter(Tag.name == name).one()[0]]

    def test_tag_uniform_distribution(self):
        from compass.cbr.util import build_tag_trees_uniform_distribution
        from math import log
        trees, lookup = build_tag_trees_uniform_distribution(unify=False)

        # foo -> foo-1 -> foo-1-1
        #              -> foo-1-2 -> foo-1-2-1
        #                         -> foo-1-2-2
        #              -> foo-1-3
        #     -> foo-2 -> foo-2-1
        #              -> foo-2-2
        # bar -> bar-1
        #     -> bar-2

        expected_probabilities = {
            u'foo': log(1.0),
            u'foo-1': log(1.0/2.0),          # p(1/2)
            u'foo-1-1': log((1.0/2.0)/3.0),  # p(1/6)
            u'foo-1-2': log((1.0/2.0)/3.0),
            u'foo-1-2-1': log(((1.0/2.0)/3.0)/2.0),  # p(1/12)
            u'foo-1-2-2': log(((1.0/2.0)/3.0)/2.0),  # p(1/12)
            u'foo-1-3': log((1.0/2.0)/3.0),
            u'foo-2': log(1.0/2.0),          # p(1/2)
            u'foo-2-1': log((1.0/2.0)/2.0),  # p(1/4)
            u'foo-2-2': log((1.0/2.0)/2.0),
            u'bar': log(1.0),
            u'bar-1': log(1.0/2.0),  # p(1/2)
            u'bar-2': log(1.0/2.0),
        }
        for tag_name, prob in expected_probabilities.items():
            self.assertAlmostEqual(self.get_tag(lookup, tag_name).log_probability_of_instance, prob)

        # Now add a virtual root (unify=True)
        trees, lookup = build_tag_trees_uniform_distribution(unify=True)

        # virtual_root -> foo -> foo-1 -> foo-1-1
        #                              -> foo-1-2 -> foo-1-2-1
        #                              -> foo-1-2-2
        #                              -> foo-1-3
        #                     -> foo-2 -> foo-2-1
        #                              -> foo-2-2
        #              -> bar -> bar-1
        #                     -> bar-2

        # Each probability should be halved by now (more information)
        expected_probabilities = {
            u'foo': log(1.0/2.0),
            u'foo-1': log((1.0/2.0)/2.0),          # p(1/4)
            u'foo-1-1': log(((1.0/2.0)/2.0)/3.0),  # p(1/12)
            u'foo-1-2': log(((1.0/2.0)/2.0)/3.0),
            u'foo-1-2-1': log((((1.0/2.0)/2.0)/3.0)/2.0),  # p(1/24)
            u'foo-1-2-2': log((((1.0/2.0)/2.0)/3.0)/2.0),  # p(1/24)
            u'foo-1-3': log(((1.0/2.0)/2.0)/3.0),
            u'foo-2': log((1.0/2.0)/2.0),          # p(1/4)
            u'foo-2-1': log(((1.0/2.0)/2.0)/2.0),  # p(1/8)
            u'foo-2-2': log(((1.0/2.0)/2.0)/2.0),
            u'bar': log(1.0/2.0),
            u'bar-1': log((1.0/2.0)/2.0),  # p(1/4)
            u'bar-2': log((1.0/2.0)/2.0),
        }

        for tag_name, prob in expected_probabilities.iteritems():
            self.assertAlmostEqual(self.get_tag(lookup, tag_name).log_probability_of_instance, prob)

    def test_tag_similarity(self):
        """Test similarities between singular tag.
        """
        from compass.cbr.util import TagSetSimilarity, build_tag_trees_uniform_distribution

        trees, lookup = build_tag_trees_uniform_distribution()
        tagset_sim = TagSetSimilarity(explain=True)

        # Check some basic assertions
        foo = self.get_tag(lookup, u'foo')
        bar = self.get_tag(lookup, u'bar')
        self.assertEqual(tagset_sim.similarity(foo, bar), 0.0, 'Tag foo and bar are not in the same hierarchy and thus '
                                                               'should have no similarity whatsoever.')
        foo_1 = self.get_tag(lookup, u'foo-1')
        foo_1_2 = self.get_tag(lookup, u'foo-1-2')
        foo_1_2_1 = self.get_tag(lookup, u'foo-1-2-1')

        sim_foo1_foo12 = tagset_sim.similarity(foo_1, foo_1_2)
        sim_foo1_foo121 = tagset_sim.similarity(foo_1, foo_1_2_1)

        self.assertGreater(sim_foo1_foo12, sim_foo1_foo121, 'Similarity between foo-1 + foo-1-2 should be greater '
                                                            'than foo-1 + foo-1-2-1, since foo-1 and foo1-2 are closer'
                                                            ' within the hierarchy.')
        foo_1_1 = self.get_tag(lookup, u'foo-1-1')
        sim_foo1_foo11 = tagset_sim.similarity(foo_1, foo_1_1)

        self.assertEqual(sim_foo1_foo11, sim_foo1_foo12, 'Similarities foo-1 + foo-1-1 and foo-1 + foo-1-2 should be '
                                                         'equal, because there are siblings.')

    def test_tagset_similarity(self):
        from compass.cbr.util import TagSetSimilarity, build_tag_trees_uniform_distribution

        # Attention: Unifying here, because with that way, the presence of a formerly root-level tag will be credited
        #            as entropy.
        trees, lookup = build_tag_trees_uniform_distribution(unify=True)

        # Check each strategy
        for strategy in (TagSetSimilarity.STRATEGY_AVERAGE, TagSetSimilarity.STRATEGY_MAX_AVERAGE):
            tagset_sim_avg = TagSetSimilarity(strategy=strategy)

            # Check some basic assertions
            foo = self.get_tag(lookup, u'foo')
            foo_all_children = foo.get_all_sub_children()
            bar = self.get_tag(lookup, u'bar')
            bar_all_children = bar.get_all_sub_children()

            self.assertEqual(tagset_sim_avg([foo] + foo_all_children, [bar] + bar_all_children), 0.0,
                             'Tags foo and bar and their children are not in the same hierarchy and thus should have no '
                             'similarity whatsoever.')

            very_specialized_foo_problem = [self.get_tag(lookup, u'foo-1-2-1'), self.get_tag(lookup, u'foo-1-2-2')]
            general_foo_problem = [self.get_tag(lookup, u'foo')]
            sim_very_specialized_vs_general = tagset_sim_avg(very_specialized_foo_problem, general_foo_problem)

            self.assertGreater(sim_very_specialized_vs_general, 0.0,
                               'A very specialized problem and general one sharing the same hierarchy should have at least '
                               'some commonality.')

            some_specialized_foo_problem = [self.get_tag(lookup, u'foo-1-1'), self.get_tag(lookup, u'foo-1-2')]

            sim_some_vs_general = tagset_sim_avg(some_specialized_foo_problem, general_foo_problem)

            self.assertGreater(sim_some_vs_general, sim_very_specialized_vs_general,
                               'A not-so specialized problem should be more similar with a general problem than a very '
                               'strong specialized problem with the same general one.')

    def test_tagset_similarity_test_explanation(self):
        # Basically like test_tagset_similarity, but the purpose of testing this is explanatory information.
        from compass.cbr.util import TagSetSimilarity, build_tag_trees_uniform_distribution

        trees, lookup = build_tag_trees_uniform_distribution(unify=True)
        tagset_sim_avg = TagSetSimilarity(strategy=TagSetSimilarity.STRATEGY_AVERAGE, explain=True)

        def _check_if_all_present(tags1, tags2, explanation):
            from itertools import product
            for tag1, tag2 in product(tags1, tags2):
                self.assertTrue(any(element['tag1'] == tag1.name and element['tag2'] == tag2.name for element in
                                    explanation['facts']['single_similarities']),
                                u'No similarity info for tag "{}" and "{}" are not in the list.'.format(tag1.name,
                                                                                                        tag2.name))

        # Check some basic assertions
        foo = self.get_tag(lookup, u'foo')
        foo_all_children = foo.get_all_sub_children()
        bar = self.get_tag(lookup, u'bar')
        bar_all_children = bar.get_all_sub_children()
        foos = [foo] + foo_all_children
        bars = [bar] + bar_all_children
        sim = tagset_sim_avg(foos, bars)
        explanation = tagset_sim_avg.get_explanation()

        # Check if all tags have been compared.
        _check_if_all_present(foos, bars, explanation)
        self.assertEqual(sim, explanation['facts']['similarity'])

        # Do the same for the other example.
        very_specialized_foo_problem = [self.get_tag(lookup, u'foo-1-2-1'), self.get_tag(lookup, u'foo-1-2-2')]
        general_foo_problem = [self.get_tag(lookup, u'foo')]

        sim_very_specialized_vs_general = tagset_sim_avg(very_specialized_foo_problem, general_foo_problem)
        explanation = tagset_sim_avg.get_explanation(language='de')

        _check_if_all_present(very_specialized_foo_problem, general_foo_problem, explanation)
        self.assertEqual(sim_very_specialized_vs_general, explanation['facts']['similarity'])

    def test_cycle_explanation(self):

        # Insert very close problem and a completely unrelated one
        foos = DBSession.query(Tag).filter(Tag.name.in_([u"foo-1-2-1", u"foo-1-2-2"])).all()
        bars = DBSession.query(Tag).filter(Tag.name.in_([u"bar-2"])).all()
        mary = DBSession.query(User).filter(User.username == u'mary').one()
        problem1 = Problem(title=u'Spezialproblem in foo', description=u'Beschreibung fuer Problem foo!!',
                           tags=foos, user=mary)
        problem2 = Problem(title=u'Problem bei bar', description=u'Andere Beschreibung.',
                           tags=bars, user=mary)
        DBSession.add_all([problem1, problem2])

        cycle = CBRCycle()
        problem = DBSession.query(Problem).filter(Problem.id == 1).one()
        query_object = ProblemQuery(problem, explain=True)
        query_object.explanation_language = 'en'
        cycle.set_input(query_object)
        cases = cycle.retrieve_cases(n=5)
        explanation = cycle.get_explanation()
        #pprint.pprint(explanation)
        #print explanation['text']
        # Remark of the author: This "test" has been made manually, since no automatic test can be made to check those
        #                       generated sentences.

    def test_cycle_with_parameters(self):

        # Insert very close problem and a completely unrelated one
        foos = DBSession.query(Tag).filter(Tag.name.in_([u"foo-1-2-1", u"foo-1-2-2"])).all()
        bars = DBSession.query(Tag).filter(Tag.name.in_([u"bar-2"])).all()
        mary = DBSession.query(User).filter(User.username == u'mary').one()
        problem1 = Problem(title=u'Spezialproblem in foo', description=u'Beschreibung fuer Problem foo!!',
                           tags=foos, user=mary)
        problem2 = Problem(title=u'Problem bei bar', description=u'Andere Beschreibung.',
                           tags=bars, user=mary)
        DBSession.add_all([problem1, problem2])
        DBSession.flush()

        problem1_id, problem2_id = problem1.id, problem2.id

        # First retrieval, zero results expected
        cycle = CBRCycle()
        cycle.set_constraints(constraints={'with_solution_proposals': True})
        problem = DBSession.query(Problem).filter(Problem.id == 1).one()
        query_object = ProblemQuery(problem)
        cycle.set_input(query_object)
        cases = cycle.retrieve_cases(n=5)
        self.assertEqual(len(cases), 0, 'There should be no results, because no other problem has a solution proposal.')

        # Propose a solution
        problem1 = DBSession.query(Problem).filter(Problem.id == problem1_id).one()
        solution1 = Solution(text=u"Lösung für das Spezialproblem", user=mary)
        solution1_assoc = ProblemSolutionProposal(solution=solution1, user=mary)
        problem1.problem_solutions_association.append(solution1_assoc)
        DBSession.flush()
        solution1_id = solution1.id

        # Second retrieval, result with results expected
        cases = cycle.retrieve_cases(n=5)
        self.assertEqual(len(cases), 1, 'There should be the problem in the result set, which has a solution proposal.')
        self.assertEqual(cases[0][0].id, problem1_id)

        # Enforce additionally only solved solutions
        cycle.set_constraints(constraints={'with_solution_proposals': True, 'only_solved': True})
        cases = cycle.retrieve_cases(n=5)

        self.assertEqual(len(cases), 0, 'There are no problems with accepted solutions.')

        # Accept that solution
        problem_specialized = DBSession.query(Problem).filter(Problem.id == problem1_id).one()
        solution1 = DBSession.query(Solution).filter(Solution.id == solution1_id).one()
        problem_specialized.chosen_solution = solution1
        DBSession.flush()

        # Enforce additionally only solved solutions
        cycle.set_constraints(constraints={'with_solution_proposals': True, 'only_solved': True})
        cases = cycle.retrieve_cases(n=5)

        self.assertEqual(len(cases), 1, 'The solution was accepted and should appear in the result set.')







