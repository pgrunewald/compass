from . import JsonSerializer
from ..models import Tag


class TagJsonSerializer(JsonSerializer):
    __attributes__ = ['id', 'name']
    __required__ = ['id', 'name']
    __attribute_serializer__ = dict()
    __object_class__ = Tag