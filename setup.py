import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid>=1.5a4',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'docutils',
    'pytz',
    'python-dateutil',
    'webtest',
    'paste',
    'numpy',
    'scipy',
    'matplotlib',
    'simplejson',
    'nltk'
    ]

setup(name='compass',
      version='0.0',
      description='compass is a reasoning system to ease disabled users finding assistive technologies and appropriate'
                  'strategies based on a collaborative community.',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        ],
      author='Paul Grunewald',
      author_email='paul.grunewald@gmail.com',
      url='http://www.paul-grunewald.de/',
      keywords='case-based-reasoning help disabled wcga strategy-finding',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='compass',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = compass:main
      [console_scripts]
      initialize_compass_db = compass.scripts.initializedb:main
      """,
      )
